/* gb-view-plugin.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_VIEW_PLUGIN_H
#define GB_VIEW_PLUGIN_H

#include <glib-object.h>

G_BEGIN_DECLS

#define GB_TYPE_VIEW_PLUGIN             (gb_view_plugin_get_type())
#define GB_VIEW_PLUGIN(o)               (G_TYPE_CHECK_INSTANCE_CAST((o),    GB_TYPE_VIEW_PLUGIN, GbViewPlugin))
#define GB_IS_VIEW_PLUGIN(o)            (G_TYPE_CHECK_INSTANCE_TYPE((o),    GB_TYPE_VIEW_PLUGIN))
#define GB_VIEW_PLUGIN_GET_INTERFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE((o), GB_TYPE_VIEW_PLUGIN, GbViewPluginIface))

typedef struct _GbViewPlugin      GbViewPlugin;
typedef struct _GbViewPluginIface GbViewPluginIface;

struct _GbViewPluginIface
{
   GTypeInterface parent;

   /* interface methods */
   void (*activate)   (GbViewPlugin *plugin);
   void (*deactivate) (GbViewPlugin *plugin);
};

GType gb_view_plugin_get_type   (void) G_GNUC_CONST;
void  gb_view_plugin_activate   (GbViewPlugin *plugin);
void  gb_view_plugin_deactivate (GbViewPlugin *plugin);

G_END_DECLS

#endif /* GB_VIEW_PLUGIN_H */
