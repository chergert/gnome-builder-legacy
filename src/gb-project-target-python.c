/* gb-project-target-python.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-project-target-python.h"

G_DEFINE_TYPE(GbProjectTargetPython,
              gb_project_target_python,
              GB_TYPE_PROJECT_TARGET)

static void
gb_project_target_python_class_init (GbProjectTargetPythonClass *klass)
{
}

static void
gb_project_target_python_init (GbProjectTargetPython *python)
{
}
