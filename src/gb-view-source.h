/* gb-view-source.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_VIEW_SOURCE_H
#define GB_VIEW_SOURCE_H

#include "gb-view.h"

G_BEGIN_DECLS

#define GB_TYPE_VIEW_SOURCE            (gb_view_source_get_type())
#define GB_VIEW_SOURCE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_VIEW_SOURCE, GbViewSource))
#define GB_VIEW_SOURCE_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_VIEW_SOURCE, GbViewSource const))
#define GB_VIEW_SOURCE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_VIEW_SOURCE, GbViewSourceClass))
#define GB_IS_VIEW_SOURCE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_VIEW_SOURCE))
#define GB_IS_VIEW_SOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_VIEW_SOURCE))
#define GB_VIEW_SOURCE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_VIEW_SOURCE, GbViewSourceClass))

typedef struct _GbViewSource        GbViewSource;
typedef struct _GbViewSourceClass   GbViewSourceClass;
typedef struct _GbViewSourcePrivate GbViewSourcePrivate;

struct _GbViewSource
{
   GbView parent;

   /*< private >*/
   GbViewSourcePrivate *priv;
};

struct _GbViewSourceClass
{
   GbViewClass parent_class;
};

GType gb_view_source_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* GB_VIEW_SOURCE_H */
