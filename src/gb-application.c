/* gb-application.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <libpeas/peas.h>
#include <stdlib.h>

#include "gb-application.h"
#include "gb-application-plugin.h"
#include "gb-log.h"
#include "gb-project.h"
#include "gb-project-format.h"
#include "gb-util.h"
#include "gb-window.h"

G_DEFINE_TYPE(GbApplication, gb_application, GTK_TYPE_APPLICATION)

struct _GbApplicationPrivate
{
   PeasExtensionSet *plugins;
};

enum
{
   PROP_0,
   LAST_PROP
};

//static GParamSpec *gParamSpecs[LAST_PROP];

/**
 * gb_application_get_default:
 *
 * Fetches the singleton instance of #GbApplication.
 *
 * Returns: (transfer none): A #GbApplication.
 */
GbApplication *
gb_application_get_default (void)
{
   static GbApplication *instance;
   static gsize initialized = FALSE;

   ENTRY;

   if (g_once_init_enter(&initialized)) {
      instance = g_object_new(GB_TYPE_APPLICATION,
                              "application-id", "org.gnome.Builder",
                              "flags", G_APPLICATION_HANDLES_COMMAND_LINE,
                              NULL);
      g_once_init_leave(&initialized, TRUE);
   }

   RETURN(instance);
}

static gint
get_default_width (void)
{
   GdkScreen *screen;
   GdkRectangle geometry = { 0 };

   ENTRY;
   screen = gdk_screen_get_default();
   gdk_screen_get_monitor_geometry(screen,
                                   gdk_screen_get_primary_monitor(screen),
                                   &geometry);
   RETURN(geometry.width * 0.75);
}

static gint
get_default_height (void)
{
   GdkScreen *screen;
   GdkRectangle geometry = { 0 };

   ENTRY;
   screen = gdk_screen_get_default();
   gdk_screen_get_monitor_geometry(screen,
                                   gdk_screen_get_primary_monitor(screen),
                                   &geometry);
   RETURN(geometry.height * 0.75);
}

/**
 * gb_application_load_cb:
 * @object: (in): A #GbProjectFormat.
 * @result: (in): A #GAsyncResult.
 * @user_data: (in): User data provided for callback.
 *
 * Handle the completion of an attempt to load a project from disk.
 * If successful, the main window will be shown containing the project.
 * If unsuccessful, the application will be released and therefore exit.
 */
static void
gb_application_open_cb (GObject      *object,
                        GAsyncResult *result,
                        gpointer      user_data)
{
   GApplicationCommandLine *command_line = user_data;
   GbProjectFormat *format = (GbProjectFormat *)object;
   GbApplication *application;
   GbProject *project;
   GtkWindow *window;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(GB_IS_PROJECT_FORMAT(format));
   g_return_if_fail(G_IS_ASYNC_RESULT(result));
   g_return_if_fail(G_IS_APPLICATION_COMMAND_LINE(command_line));

   application = gb_application_get_default();

   if (!(project = gb_project_format_open_finish(format, result, &error))) {
      g_application_command_line_printerr(command_line, "%s\n", error->message);
      g_error_free(error);
      GOTO(failure);
   }

   window = g_object_new(GB_TYPE_WINDOW,
                         "default-height", get_default_height(),
                         "default-width", get_default_width(),
                         "project", project,
                         "title", _("Builder"),
                         "window-position", GTK_WIN_POS_CENTER,
                         NULL);
   gtk_application_add_window(GTK_APPLICATION(application), window);
   gtk_window_present(window);
   g_object_unref(project);

failure:
   g_application_release(G_APPLICATION(application));
   g_object_unref(command_line);
   EXIT;
}

/**
 * gb_application_command_line:
 * @application: (in): A #GbApplication.
 * @command_line: (in): A #GApplicationCommandLine.
 *
 * Handles the command line for the main #GbApplication instance. This will
 * handle the arguements and possibly run a command, open a project, or
 * show the welcome window.
 *
 * Returns: An exit code.
 */
static int
gb_application_command_line (GApplication            *application,
                             GApplicationCommandLine *command_line)
{
   GOptionContext *context;
   GbProjectFormat *format;
   GInputStream *stream;
   GError *error = NULL;
   gchar **args;
   gchar **argv;
   gchar *path;
   GFile *file;
   gint argc;
   gint i;
   int ret = EXIT_SUCCESS;

   ENTRY;

   g_return_val_if_fail(GB_IS_APPLICATION(application), 0);
   g_return_val_if_fail(G_IS_APPLICATION_COMMAND_LINE(command_line), 0);

   /*
    * GOptionContext will remove items from the array without freeing them.
    * so we must handle freeing them ourselves.
    */
   args = g_application_command_line_get_arguments(command_line, &argc);
   argv = g_new0(gchar *, argc + 1);
   for (i = 0; i < argc; i++) {
      argv[i] = args[i];
   }

   /*
    * Parse the command line arguments for the application.
    */
   context = g_option_context_new(_("- Build applications for Gnome."));
   g_option_context_add_group(context, gtk_get_option_group(TRUE));
   g_option_context_add_group(context, g_irepository_get_option_group());
   if (!g_option_context_parse(context, &argc, &argv, &error)) {
      g_application_command_line_printerr(command_line, "%s\n",
                                          error->message);
      g_error_free(error);
      ret = EXIT_FAILURE;
      GOTO(failure);
   }
   g_option_context_free(context);

   /*
    * Initialize icon themes.
    */
   gb_theme_init();

   /*
    * If there is an argument left and it is a path to a directory,
    * we will attempt to open it as a project.
    */
   if ((argc == 2) && g_file_test(argv[1], G_FILE_TEST_IS_DIR)) {
      path = g_build_filename(argv[1], ".gbproject", NULL);
      format = gb_project_format_new();
      file = g_file_new_for_path(path);
      if ((stream = (GInputStream *)g_file_read(file, NULL, &error))) {
         gb_project_format_open_async(format, argv[1], stream, NULL,
                                      gb_application_open_cb,
                                      g_object_ref(command_line));
         g_application_hold(application);
         g_object_unref(stream);
      } else {
         g_application_command_line_printerr(command_line, "%s\n",
                                             error->message);
         g_error_free(error);
         ret = EXIT_FAILURE;
      }
      g_free(path);
      g_object_unref(file);
      g_object_unref(format);
   }

failure:
   g_strfreev(args);
   g_free(argv);
   RETURN(ret);
}

/**
 * gb_application_finalize:
 * @object: (in): A #GbApplication.
 *
 * Finalizer for a #GbApplication instance.  Frees any resources held by
 * the instance.
 */
static void
gb_application_finalize (GObject *object)
{
   ENTRY;
   G_OBJECT_CLASS(gb_application_parent_class)->finalize(object);
   EXIT;
}

/**
 * gb_application_class_init:
 * @klass: (in): A #GbApplicationClass.
 *
 * Initializes the #GbApplicationClass and prepares the vtable.
 */
static void
gb_application_class_init (GbApplicationClass *klass)
{
   GObjectClass *object_class;
   GApplicationClass *app_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_application_finalize;
   g_type_class_add_private(object_class, sizeof(GbApplicationPrivate));

   app_class = G_APPLICATION_CLASS(klass);
   app_class->command_line = gb_application_command_line;
}

static void
gb_application_init_plugins (GbApplication *application)
{
   static const gchar *default_plugins[] = {
      "cautoindent",
      "cfuncstyler",
      "cgobject",
      "cinclude",
      "superopen",
      "whitespace",
      "pyjunk",
      "cjunk",
      "android",
      NULL
   };
   GbApplicationPrivate *priv;
   PeasPluginInfo *plugin_info;
   const gchar *name;
   PeasEngine *engine;
   gchar *path;
   guint i;
   GDir *dir;

   ENTRY;

   g_return_if_fail(GB_IS_APPLICATION(application));

   priv = application->priv;

   /*
    * If we are running from the project tree, we will have the typelib
    * right beneath us.
    */
   if (g_file_test("GnomeBuilder-1.0.typelib", G_FILE_TEST_IS_REGULAR)) {
      g_irepository_prepend_search_path(".");
   }

   engine = peas_engine_get_default();
   peas_engine_enable_loader(engine, "c");
   peas_engine_enable_loader(engine, "python");

   /*
    * If we are running the project from within the source tree, lets try
    * to add the plugin directories to our search path.
    */
   if (g_getenv("GB_IN_TREE_PLUGINS")) {
      if ((dir = g_dir_open("plugins", 0, NULL))) {
         while ((name = g_dir_read_name(dir))) {
            path = g_build_filename("plugins", name, NULL);
            if (g_file_test(path, G_FILE_TEST_IS_DIR)) {
               peas_engine_add_search_path(engine, path, path);
            }
            g_free(path);
         }
         g_dir_close(dir);
      }
   }

   /*
    * Add installed plugins to the search path.
    */
   peas_engine_add_search_path(engine,
                               PACKAGE_LIB_DIR "/gnome-builder/plugins",
                               PACKAGE_DATA_DIR);

   peas_engine_rescan_plugins(engine);

   /*
    * Load our builtin plugins.
    */
   for (i = 0; default_plugins[i]; i++) {
      if ((plugin_info = peas_engine_get_plugin_info(engine,
                                                     default_plugins[i]))) {
         peas_engine_load_plugin(engine, plugin_info);
      }
   }

   /*
    * Retrieve our plugins for hooking into application features.
    */
   priv->plugins = peas_extension_set_new(engine, GB_TYPE_APPLICATION_PLUGIN,
                                          "application", application,
                                          NULL);

   EXIT;
}

/**
 * gb_application_init:
 * @application: (in): A #GbApplication.
 *
 * Initializes the newly created #GbApplication instance.
 */
static void
gb_application_init (GbApplication *application)
{
   ENTRY;
   application->priv =
      G_TYPE_INSTANCE_GET_PRIVATE(application,
                                  GB_TYPE_APPLICATION,
                                  GbApplicationPrivate);
   gb_application_init_plugins(application);
   EXIT;
}
