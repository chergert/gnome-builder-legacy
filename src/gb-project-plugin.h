/* gb-project-plugin.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_PROJECT_PLUGIN_H
#define GB_PROJECT_PLUGIN_H

#include <glib-object.h>

#include "gb-project-item.h"

G_BEGIN_DECLS

#define GB_TYPE_PROJECT_PLUGIN             (gb_project_plugin_get_type())
#define GB_PROJECT_PLUGIN(o)               (G_TYPE_CHECK_INSTANCE_CAST((o),    GB_TYPE_PROJECT_PLUGIN, GbProjectPlugin))
#define GB_IS_PROJECT_PLUGIN(o)            (G_TYPE_CHECK_INSTANCE_TYPE((o),    GB_TYPE_PROJECT_PLUGIN))
#define GB_PROJECT_PLUGIN_GET_INTERFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE((o), GB_TYPE_PROJECT_PLUGIN, GbProjectPluginIface))

typedef struct _GbProjectPlugin      GbProjectPlugin;
typedef struct _GbProjectPluginIface GbProjectPluginIface;

struct _GbProjectPluginIface
{
   GTypeInterface parent;

   void (*activate)   (GbProjectPlugin  *plugin);
   void (*deactivate) (GbProjectPlugin  *plugin);
};

GType gb_project_plugin_get_type   (void) G_GNUC_CONST;
void  gb_project_plugin_activate   (GbProjectPlugin *plugin);
void  gb_project_plugin_deactivate (GbProjectPlugin *plugin);

G_END_DECLS

#endif /* GB_PROJECT_PLUGIN_H */
