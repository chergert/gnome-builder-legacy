/* gb-window.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_WINDOW_H
#define GB_WINDOW_H

#include <gtk/gtk.h>

#include "gb-project.h"
#include "gb-view.h"
#include "gb-view-layout.h"

G_BEGIN_DECLS

#define GB_TYPE_WINDOW            (gb_window_get_type())
#define GB_WINDOW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_WINDOW, GbWindow))
#define GB_WINDOW_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_WINDOW, GbWindow const))
#define GB_WINDOW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_WINDOW, GbWindowClass))
#define GB_IS_WINDOW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_WINDOW))
#define GB_IS_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_WINDOW))
#define GB_WINDOW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_WINDOW, GbWindowClass))

typedef struct _GbWindow        GbWindow;
typedef struct _GbWindowClass   GbWindowClass;
typedef struct _GbWindowPrivate GbWindowPrivate;

struct _GbWindow
{
   GtkWindow parent;

   /*< private >*/
   GbWindowPrivate *priv;
};

struct _GbWindowClass
{
   GtkWindowClass parent_class;

   void (*view_changed) (GbWindow *window,
                         GbView   *view);
};

void          gb_window_action_set        (GbWindow    *window,
                                           const gchar *action_name,
                                           const gchar *first_property,
                                           ...);
guint         gb_window_add_ui            (GbWindow    *window,
                                           const gchar *data,
                                           gssize       length);
void          gb_window_add_view          (GbWindow    *window,
                                           GbView      *view);
GtkAction    *gb_window_get_action        (GbWindow    *window,
                                           const gchar *action_name);
GbViewLayout *gb_window_get_layout        (GbWindow    *window);
GbProject    *gb_window_get_project       (GbWindow    *window);
GType         gb_window_get_type          (void) G_GNUC_CONST;
GtkUIManager *gb_window_get_ui_manager    (GbWindow    *window);
GbView       *gb_window_get_view_for_file (GbWindow    *window,
                                           GFile       *file);

G_END_DECLS

#endif /* GB_WINDOW_H */
