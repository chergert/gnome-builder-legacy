/* gb-progress-bar.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_PROGRESS_BAR_H
#define GB_PROGRESS_BAR_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GB_TYPE_PROGRESS_BAR            (gb_progress_bar_get_type())
#define GB_PROGRESS_BAR(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_PROGRESS_BAR, GbProgressBar))
#define GB_PROGRESS_BAR_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_PROGRESS_BAR, GbProgressBar const))
#define GB_PROGRESS_BAR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_PROGRESS_BAR, GbProgressBarClass))
#define GB_IS_PROGRESS_BAR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_PROGRESS_BAR))
#define GB_IS_PROGRESS_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_PROGRESS_BAR))
#define GB_PROGRESS_BAR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_PROGRESS_BAR, GbProgressBarClass))

typedef struct _GbProgressBar        GbProgressBar;
typedef struct _GbProgressBarClass   GbProgressBarClass;
typedef struct _GbProgressBarPrivate GbProgressBarPrivate;

struct _GbProgressBar
{
   GtkDrawingArea parent;

   /*< private >*/
   GbProgressBarPrivate *priv;
};

struct _GbProgressBarClass
{
   GtkDrawingAreaClass parent_class;
};

GType gb_progress_bar_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* GB_PROGRESS_BAR_H */
