/* gb-window-plugin.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gb-window.h"
#include "gb-window-plugin.h"

void
gb_window_plugin_activate (GbWindowPlugin *plugin)
{
   GbWindowPluginIface *iface;

   g_return_if_fail(GB_IS_WINDOW_PLUGIN(plugin));

   iface = GB_WINDOW_PLUGIN_GET_INTERFACE(plugin);
   if (iface->activate) {
      iface->activate(plugin);
   }
}

void
gb_window_plugin_deactivate (GbWindowPlugin *plugin)
{
   GbWindowPluginIface *iface;

   g_return_if_fail(GB_IS_WINDOW_PLUGIN(plugin));

   iface = GB_WINDOW_PLUGIN_GET_INTERFACE(plugin);
   if (iface->deactivate) {
      iface->deactivate(plugin);
   }
}

void
gb_window_plugin_update_state (GbWindowPlugin *plugin)
{
   GbWindowPluginIface *iface;

   g_return_if_fail(GB_IS_WINDOW_PLUGIN(plugin));

   iface = GB_WINDOW_PLUGIN_GET_INTERFACE(plugin);
   if (iface->update_state) {
      iface->update_state(plugin);
   }
}

static void
gb_window_plugin_default_init (gpointer data)
{
   static gsize initialized = FALSE;
   GbWindowPluginIface *iface = data;

   if (g_once_init_enter(&initialized)) {
      g_object_interface_install_property(
         iface,
         g_param_spec_object("window",
                              "Window",
                              "The window the plugin is attached to.",
                              GB_TYPE_WINDOW,
                              G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
      g_once_init_leave(&initialized, TRUE);
   }
}

GType
gb_window_plugin_get_type (void)
{
   static GType type_id = 0;

   if (g_once_init_enter((gsize *)&type_id)) {
      GType _type_id;
      const GTypeInfo g_type_info = {
         sizeof(GbWindowPluginIface),
         gb_window_plugin_default_init, /* base_init */
         NULL,                          /* base_finalize */
         NULL,                          /* class_init */
         NULL,                          /* class_finalize */
         NULL,                          /* class_data */
         0,                             /* instance_size */
         0,                             /* n_preallocs */
         NULL,                          /* instance_init */
         NULL                           /* value_vtable */
      };

      _type_id = g_type_register_static(G_TYPE_INTERFACE,
                                        "GbWindowPlugin",
                                        &g_type_info,
                                        0);
      g_type_interface_add_prerequisite(_type_id, G_TYPE_OBJECT);
      g_once_init_leave((gsize *)&type_id, _type_id);
   }

   return type_id;
}
