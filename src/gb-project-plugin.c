/* gb-project-plugin.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gb-log.h"
#include "gb-project.h"
#include "gb-project-plugin.h"

void
gb_project_plugin_activate (GbProjectPlugin *plugin)
{
   GbProjectPluginIface *iface;

   ENTRY;
   g_return_if_fail(GB_IS_PROJECT_PLUGIN(plugin));
   if ((iface = GB_PROJECT_PLUGIN_GET_INTERFACE(plugin))->activate) {
      iface->activate(plugin);
   }
   EXIT;
}

void
gb_project_plugin_deactivate (GbProjectPlugin *plugin)
{
   GbProjectPluginIface *iface;

   ENTRY;
   g_return_if_fail(GB_IS_PROJECT_PLUGIN(plugin));
   if ((iface = GB_PROJECT_PLUGIN_GET_INTERFACE(plugin))->deactivate) {
      iface->deactivate(plugin);
   }
   EXIT;
}

static void
gb_project_plugin_init (gpointer iface)
{
   static gsize initialized = FALSE;

   if (g_once_init_enter(&initialized)) {
      g_object_interface_install_property(
            iface,
            g_param_spec_object("project",
                                "Project",
                                "The target project.",
                                GB_TYPE_PROJECT,
                                G_PARAM_WRITABLE));
      g_once_init_leave(&initialized, TRUE);
   }
}

GType
gb_project_plugin_get_type (void)
{
   static GType type_id = 0;

   if (g_once_init_enter((gsize *)&type_id)) {
      GType _type_id;
      const GTypeInfo g_type_info = {
         sizeof(GbProjectPluginIface),
         gb_project_plugin_init, /* base_init */
         NULL,                   /* base_finalize */
         NULL,                   /* class_init */
         NULL,                   /* class_finalize */
         NULL,                   /* class_data */
         0,                      /* instance_size */
         0,                      /* n_preallocs */
         NULL,                   /* instance_init */
         NULL                    /* value_vtable */
      };

      _type_id = g_type_register_static(G_TYPE_INTERFACE,
                                        "GbProjectPlugin",
                                        &g_type_info,
                                        0);
      g_type_interface_add_prerequisite(_type_id, G_TYPE_OBJECT);
      g_once_init_leave((gsize *)&type_id, _type_id);
   }

   return type_id;
}
