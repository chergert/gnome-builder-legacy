/* gb-view-source.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <gtksourceview/gtksource.h>

#include "gb-anim-bin.h"
#include "gb-animation.h"
#include "gb-log.h"
#include "gb-menu-button.h"
#include "gb-progress-bar.h"
#include "gb-project.h"
#include "gb-scrolled-window.h"
#include "gb-util.h"
#include "gb-view-source.h"
#include "gb-window.h"

G_DEFINE_TYPE(GbViewSource, gb_view_source, GB_TYPE_VIEW)

struct _GbViewSourcePrivate
{
   GtkWidget *vpaned;
   GtkWidget *scroller1;
   GtkWidget *scroller2;

   GtkSourceBuffer *buffer;
   GtkWidget *source1;
   GtkWidget *source2;

   GtkWidget *lang_combo;
   GtkWidget *size_combo;
   GtkWidget *bottom_notebook;
   GtkWidget *bottom_combo;

   GtkWidget *pos_label;

   GtkWidget *progress;

   GFile *file;

   guint theme_handler;
};

enum
{
   PROP_0,
   PROP_BUFFER,
   PROP_FILE,
   PROP_PATH,
   PROP_SOURCE_VIEW,
   LAST_PROP
};

enum
{
   PRE_SAVE,
   LAST_SIGNAL
};

static GParamSpec *gParamSpecs[LAST_PROP];
static guint       gSignals[LAST_SIGNAL];

/**
 * gb_view_source_buffer_modified_changed:
 * @source: (in): A #GbViewSource.
 * @buffer: (in): A #GtkTextBuffer.
 *
 * Handle the "modified-changed" signal from @buffer. Notify the view that
 * it can be saved based on the modification status of @buffer.
 */
static void
gb_view_source_buffer_modified_changed (GbViewSource  *source,
                                        GtkTextBuffer *buffer)
{
   g_return_if_fail(GB_IS_VIEW_SOURCE(source));
   g_return_if_fail(GTK_IS_TEXT_BUFFER(buffer));
   gb_view_set_can_save(GB_VIEW(source),
                        gtk_text_buffer_get_modified(buffer));
}

/**
 * gb_view_source_goto_start:
 * @buffer: A #GtkTextBuffer.
 *
 * Moves the cursor to the beginning of @buffer.
 */
static void
gb_view_source_goto_start (GtkTextBuffer *buffer)
{
   GtkTextIter iter;

   g_return_if_fail(GTK_IS_TEXT_BUFFER(buffer));

   gtk_text_buffer_get_start_iter(buffer, &iter);
   gtk_text_buffer_place_cursor(buffer, &iter);
}

/**
 * gb_view_source_get_scheme_id:
 *
 * Helper function to determine what GtkSourceStyleScheme we should be using.
 * Currently, this checks for the user preferring a dark theme for the
 * application for which style to load. In the future, we will likely allow
 * an override in the preferences.
 */
static const gchar *
gb_view_source_get_scheme_id (void)
{
   gboolean prefer_dark;

   /*
    * TODO: Support custom schemes using GSettings.
    */
   g_object_get(gtk_settings_get_default(),
                "gtk-application-prefer-dark-theme", &prefer_dark,
                NULL);

   return prefer_dark ? "gnome-builder-dark" : "gnome-builder";
}

/**
 * gb_view_source_get_scheme:
 * @scheme_id: (in) (allow-none): The id of the scheme.
 *
 * Fetches a given scheme from the schemes manager. If @scheme_id is %NULL,
 * the default scheme will be retrieved.
 */
static GtkSourceStyleScheme *
gb_view_source_get_scheme (const gchar *scheme_id)
{
   GtkSourceStyleSchemeManager *manager;
   GtkSourceStyleScheme *scheme;

   if (!scheme_id) {
      scheme_id = gb_view_source_get_scheme_id();
   }
   manager = gtk_source_style_scheme_manager_get_default();
   scheme = gtk_source_style_scheme_manager_get_scheme(manager, scheme_id);
   return scheme;
}

/**
 * gb_view_source_get_file:
 * @source: (in): A #GbViewSource.
 *
 * Gets a #GFile that represents the file displayed.
 *
 * Returns: (transfer none): A #GFile.
 */
GFile *
gb_view_source_get_file (GbViewSource *source)
{
   g_return_val_if_fail(GB_IS_VIEW_SOURCE(source), NULL);
   return source->priv->file;
}

static void
gb_view_source_notify_cursor (GbViewSource  *source,
                              GParamSpec    *pspec,
                              GtkTextBuffer *buffer)
{
   GbViewSourcePrivate *priv;
   GtkTextIter iter;
   guint col;
   guint line;
   guint position;
   gchar *s;

   g_return_if_fail(GB_IS_VIEW_SOURCE(source));
   g_return_if_fail(pspec != NULL);
   g_return_if_fail(GTK_SOURCE_IS_BUFFER(buffer));

   priv = source->priv;

   g_object_get(buffer, "cursor-position", &position, NULL);
   gtk_text_buffer_get_iter_at_offset(buffer, &iter, position);

   line = gtk_text_iter_get_line(&iter);
   col = gtk_source_view_get_visual_column(GTK_SOURCE_VIEW(priv->source1),
                                           &iter);
   s = g_strdup_printf(_("Line %d, Column %d"), line + 1, col + 1);
   g_object_set(priv->pos_label, "label", s, NULL);
   g_free(s);
}

static void
gb_view_source_set_file_attribs (GbViewSource *source,
                                 GFile        *file)
{
   GtkSourceLanguageManager *lm;
   GbViewSourcePrivate *priv;
   GtkSourceLanguage *lang;
   const gchar *icon_name;
   GFileInfo *info;
   gchar *path;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW_SOURCE(source));
   g_return_if_fail(G_IS_FILE(file));

   priv = source->priv;

   if (!(path = g_file_get_path(file))) {
      path = g_file_get_uri(file);
   }

   if ((icon_name = gb_path_get_icon_name(path))) {
      gb_view_set_icon_name(GB_VIEW(source), icon_name);
   }

   info = g_file_query_info(file, "standard::*", 0, NULL, NULL);

   /*
    * Get the relative path to the file from the project.
    */
   {
      const gchar *directory;
      GtkWidget *window;
      GbProject *project;

      gb_view_set_name(GB_VIEW(source), path);

      window = gtk_widget_get_toplevel(GTK_WIDGET(source));
      if (GB_IS_WINDOW(window)) {
         project = gb_window_get_project(GB_WINDOW(window));
         directory = gb_project_get_directory(project);
         if (g_str_has_prefix(path, directory)) {
            gb_view_set_name(GB_VIEW(source), path + strlen(directory));
         }
      }
   }

   lm = gtk_source_language_manager_get_default();
   lang = gtk_source_language_manager_guess_language(
      lm, path, g_file_info_get_content_type(info));
   if (lang) {
      gb_menu_button_set_label(GB_MENU_BUTTON(priv->lang_combo),
                               gtk_source_language_get_name(lang));
   } else {
      gb_menu_button_set_label(GB_MENU_BUTTON(priv->lang_combo), "");
   }
   gtk_source_buffer_set_language(priv->buffer, lang);

   g_object_unref(info);
   g_free(path);

   EXIT;
}

static void
gb_view_source_set_file (GbViewSource *source,
                         GFile        *file)
{
   GbViewSourcePrivate *priv;
   gchar *contents = NULL;
   gsize size;

   g_return_if_fail(GB_IS_VIEW_SOURCE(source));
   g_return_if_fail(G_IS_FILE(file));

   priv = source->priv;

   /*
    * TODO: Do this all aysnc.
    */


   priv->file = g_object_ref(file);

   gb_view_source_set_file_attribs(source, file);

   if (g_file_get_contents(g_file_get_path(file), &contents, &size, NULL)) {
      gtk_source_buffer_begin_not_undoable_action(priv->buffer);
      gtk_text_buffer_set_text(GTK_TEXT_BUFFER(priv->buffer), contents, size);
      gtk_source_buffer_end_not_undoable_action(priv->buffer);
      gtk_text_buffer_set_modified(GTK_TEXT_BUFFER(priv->buffer), FALSE);
      gb_view_source_goto_start(GTK_TEXT_BUFFER(priv->buffer));
      g_free(contents);
   }
}

static void
gb_view_source_grab_focus (GtkWidget *widget)
{
   GbViewSource *source = (GbViewSource *)widget;
   g_return_if_fail(GB_IS_VIEW_SOURCE(source));
   gtk_widget_grab_focus(source->priv->source1);
}

static void
save_animation_done (gpointer progress)
{
   gtk_widget_hide(progress);
   g_object_unref(progress);
}

static void
gb_view_source_save (GbView *view)
{
   GbViewSourcePrivate *priv;
   GFileOutputStream *stream;
   GtkTextBuffer *buffer;
   GbViewSource *source = (GbViewSource *)view;
   GCancellable *cancellable = NULL;
   GtkTextIter iter1;
   GtkTextIter iter2;
   const gchar *etag = NULL;
   GtkWidget *widget;
   gboolean make_backup = FALSE;
   GError *error = NULL;
   gchar *text;
   gsize n_written;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW_SOURCE(source));

   priv = source->priv;

   if (!priv->file) {
      GtkDialog *dialog;

      widget = gtk_widget_get_toplevel(GTK_WIDGET(view));
      dialog = g_object_new(GTK_TYPE_FILE_CHOOSER_DIALOG,
                            "action", GTK_FILE_CHOOSER_ACTION_SAVE,
                            "title", _("Save File"),
                            "transient-for", widget,
                            NULL);
      gtk_dialog_add_buttons(dialog,
                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                             GTK_STOCK_SAVE, GTK_RESPONSE_OK,
                             NULL);
      if (gtk_dialog_run(dialog) != GTK_RESPONSE_OK) {
         gtk_widget_destroy(GTK_WIDGET(dialog));
         EXIT;
      }
      priv->file = gtk_file_chooser_get_file(GTK_FILE_CHOOSER(dialog));
      gtk_widget_destroy(GTK_WIDGET(dialog));
   }

   g_signal_emit(source, gSignals[PRE_SAVE], 0);

   g_object_set(priv->progress,
                "fraction", 0.0,
                "visible", TRUE,
                NULL);
   gb_object_animate_full(priv->progress,
                          GB_ANIMATION_EASE_IN_OUT_QUAD,
                          500,
                          60,
                          save_animation_done,
                          g_object_ref(priv->progress),
                          "fraction", 1.0,
                          NULL);

   buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(priv->source1));

   /*
    * TODO: Do this all async.
    */

   if (!(stream = g_file_replace(priv->file, etag, make_backup,
                                 G_FILE_CREATE_NONE,
                                 cancellable, &error))) {
      g_printerr("%s\n", error->message);
      g_error_free(error);
      EXIT;
   }

   gtk_text_buffer_get_start_iter(buffer, &iter1);
   gtk_text_buffer_get_end_iter(buffer, &iter2);
   text = gtk_text_buffer_get_text(buffer, &iter1, &iter2, TRUE);

   if (!g_output_stream_write_all(G_OUTPUT_STREAM(stream), text, strlen(text),
                                  &n_written, cancellable, &error)) {
      g_printerr("%s\n", error->message);
      g_error_free(error);
   } else {
      g_output_stream_flush(G_OUTPUT_STREAM(stream), NULL, NULL);
      if (!g_output_stream_close(G_OUTPUT_STREAM(stream), cancellable,
                                 &error)) {
         g_printerr("%s\n", error->message);
         g_error_free(error);
      } else {
         gb_view_set_can_save(view, FALSE);
      }
   }

   gtk_text_buffer_set_modified(buffer, FALSE);

   g_object_unref(stream);
   g_free(text);

   gb_view_source_set_file_attribs(source, priv->file);

   EXIT;
}

static void
gb_view_source_update_style (GbViewSource *source)
{
   g_return_if_fail(GB_IS_VIEW_SOURCE(source));
   gtk_source_buffer_set_style_scheme(source->priv->buffer,
                                      gb_view_source_get_scheme(NULL));
}

static void
gb_view_source_style_set (GtkWidget *widget,
                          GtkStyle  *previous_style)
{
   GTK_WIDGET_CLASS(gb_view_source_parent_class)->style_set(widget, previous_style);
   gb_view_source_update_style(GB_VIEW_SOURCE(widget));
}

static gboolean
gb_view_source_is_file (GbView *view,
                        GFile  *file)
{
   return g_file_equal(file, GB_VIEW_SOURCE(view)->priv->file);
}

/**
 * gb_view_source_finalize:
 * @object: (in): A #GbViewSource.
 *
 * Finalizer for a #GbViewSource instance.
 */
static void
gb_view_source_finalize (GObject *object)
{
   GbViewSourcePrivate *priv;

   ENTRY;
   priv = GB_VIEW_SOURCE(object)->priv;
   g_clear_object(&priv->buffer);
   g_clear_object(&priv->file);
   g_signal_handler_disconnect(gtk_settings_get_default(),
                               priv->theme_handler);
   G_OBJECT_CLASS(gb_view_source_parent_class)->finalize(object);
   EXIT;
}

/**
 * gb_view_source_get_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (out): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Get a given #GObject property.
 */
static void
gb_view_source_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
   GbViewSource *source = GB_VIEW_SOURCE(object);

   switch (prop_id) {
   case PROP_BUFFER:
      g_value_set_object(value, source->priv->buffer);
      break;
   case PROP_FILE:
      g_value_set_object(value, gb_view_source_get_file(source));
      break;
   case PROP_PATH:
      if (source->priv->file) {
         g_value_take_string(value, g_file_get_path(source->priv->file));
      }
      break;
   case PROP_SOURCE_VIEW:
      g_value_set_object(value, source->priv->source1);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * gb_view_source_set_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (in): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Set a given #GObject property.
 */
static void
gb_view_source_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
   GbViewSource *source = GB_VIEW_SOURCE(object);

   switch (prop_id) {
   case PROP_FILE:
      gb_view_source_set_file(source, g_value_get_object(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * gb_view_source_class_init:
 * @klass: (in): A #GbViewSourceClass.
 *
 * Initializes the #GbViewSourceClass and prepares the vtable.
 */
static void
gb_view_source_class_init (GbViewSourceClass *klass)
{
   GbViewClass *view_class;
   GObjectClass *object_class;
   GtkWidgetClass *widget_class;
   GtkSourceStyleSchemeManager *schemes;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_view_source_finalize;
   object_class->get_property = gb_view_source_get_property;
   object_class->set_property = gb_view_source_set_property;
   g_type_class_add_private(object_class, sizeof(GbViewSourcePrivate));

   widget_class = GTK_WIDGET_CLASS(klass);
   widget_class->grab_focus = gb_view_source_grab_focus;
   widget_class->style_set = gb_view_source_style_set;

   view_class = GB_VIEW_CLASS(klass);
   view_class->is_file = gb_view_source_is_file;
   view_class->save = gb_view_source_save;

   /**
    * GbViewSource:buffer:
    *
    * The "buffer" property is the #GtkSourceBuffer being visualized by
    * the editor.
    */
   gParamSpecs[PROP_BUFFER] =
      g_param_spec_object("buffer",
                          _("Buffer"),
                          _("The text buffer."),
                          GTK_SOURCE_TYPE_BUFFER,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_BUFFER,
                                   gParamSpecs[PROP_BUFFER]);

   /**
    * GbViewSource:file:
    *
    * The "file" property is a #GFile that represents the target path.
    */
   gParamSpecs[PROP_FILE] =
      g_param_spec_object("file",
                          _("File"),
                          _("A GFile to display."),
                          G_TYPE_FILE,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_FILE,
                                   gParamSpecs[PROP_FILE]);

   /**
    * GbViewSource:path:
    *
    * The "path" property is the path to the destination file.
    */
   gParamSpecs[PROP_PATH] =
      g_param_spec_string("path",
                          _("Path"),
                          _("The path of the file."),
                          NULL,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_PATH,
                                   gParamSpecs[PROP_PATH]);

   /**
    * GbViewSource:source-view:
    *
    * The "source-view" property is the #GtkSourceView widget.
    */
   gParamSpecs[PROP_SOURCE_VIEW] =
      g_param_spec_object("source-view",
                          _("Source View"),
                          _("The GtkSourceView widget."),
                          GTK_SOURCE_TYPE_VIEW,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_SOURCE_VIEW,
                                   gParamSpecs[PROP_SOURCE_VIEW]);

   /**
    * GbViewSource::pre-save:
    *
    * The "pre-save" signal is emitted before saving a buffer.
    */
   gSignals[PRE_SAVE] = g_signal_new("pre-save",
                                     GB_TYPE_VIEW_SOURCE,
                                     G_SIGNAL_RUN_LAST,
                                     0,
                                     NULL,
                                     NULL,
                                     g_cclosure_marshal_VOID__VOID,
                                     G_TYPE_NONE,
                                     0);

   /*
    * Add search paths for our custom style schemes.
    */
   schemes = gtk_source_style_scheme_manager_get_default();
   gtk_source_style_scheme_manager_prepend_search_path(
      schemes, PACKAGE_DATA_DIR"/data/schemes");
   gtk_source_style_scheme_manager_prepend_search_path(schemes, "data/schemes");
}

static gboolean
gb_view_source_position_progress (GtkOverlay   *overlay,
                                  GtkWidget    *progress,
                                  GdkRectangle *allocation,
                                  gpointer      user_data)
{
   GtkAllocation a;

   g_return_val_if_fail(GTK_IS_WIDGET(overlay), FALSE);
   g_return_val_if_fail(allocation != NULL, FALSE);

   gtk_widget_get_allocation(GTK_WIDGET(overlay), &a);

   allocation->x = 0;
   allocation->y = 0;
   allocation->height = 2;
   allocation->width = a.width;

   return TRUE;
}

static GtkWidget *
gb_view_source_create_source_view (GbViewSource *source)
{
   GbViewSourcePrivate *priv;
   GtkWidget *ret;
   gboolean use_spaces = TRUE;

   ENTRY;

   g_return_val_if_fail(GB_IS_VIEW_SOURCE(source), NULL);

   priv = source->priv;

   if (priv->source1) {
      g_object_get(priv->source1,
                   "insert-spaces-instead-of-tabs", &use_spaces,
                   NULL);
   }

   ret = g_object_new(GTK_SOURCE_TYPE_VIEW,
                      "auto-indent", FALSE,
                      "buffer", priv->buffer,
                      "indent-width", 3,
                      "insert-spaces-instead-of-tabs", use_spaces,
                      "right-margin-position", 80,
                      "show-line-numbers", TRUE,
                      "show-right-margin", TRUE,
                      "smart-home-end", TRUE,
                      "tab-width", 3,
                      "visible", TRUE,
                      NULL);

   gb_widget_monospace_font(ret);
   gb_widget_shrink_font(ret);

   /*
    * TODO: Bind above properties to GSettings first, and then
    *       overrides in the controls.
    */

   RETURN(ret);
}

static void
split_toggled (GtkWidget    *button,
               GbViewSource *source)
{
   GbViewSourcePrivate *priv;

   ENTRY;

   g_return_if_fail(GTK_IS_BUTTON(button));
   g_return_if_fail(GB_IS_VIEW_SOURCE(source));

   priv = source->priv;

   if (priv->scroller2) {
      gtk_container_remove(GTK_CONTAINER(priv->vpaned), priv->scroller2);
      priv->scroller2 = NULL;
      priv->source2 = NULL;
   } else {
      GtkAllocation alloc;

      priv->scroller2 = g_object_new(gb_get_scrolled_window_gtype(),
                                     "visible", TRUE,
                                     NULL);
      gtk_container_add_with_properties(GTK_CONTAINER(priv->vpaned), priv->scroller2,
                                        "shrink", FALSE,
                                        "resize", TRUE,
                                        NULL);

      priv->source2 = gb_view_source_create_source_view(source);
      gtk_container_add(GTK_CONTAINER(priv->scroller2), priv->source2);

      gtk_widget_get_allocation(priv->vpaned, &alloc);
      g_object_set(priv->vpaned, "position", alloc.height / 2, NULL);
   }

   EXIT;
}

static gint
lang_sort_func (gconstpointer a,
                gconstpointer b)
{
   GtkSourceLanguage **l1 = (gpointer)a;
   GtkSourceLanguage **l2 = (gpointer)b;
   const gchar *i1 = gtk_source_language_get_name(*l1);
   const gchar *i2 = gtk_source_language_get_name(*l2);
   return g_strcmp0(i1, i2);
}

static void
gb_view_source_lang_activate (GbViewSource *source,
                              GtkMenuItem  *menu_item)
{
   GbViewSourcePrivate *priv;
   GtkSourceLanguageManager *lm;
   GtkSourceLanguage *lang;
   const gchar *lang_id;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW_SOURCE(source));
   g_return_if_fail(GTK_IS_MENU_ITEM(menu_item));

   priv = source->priv;

   if ((lang_id = g_object_get_data(G_OBJECT(menu_item), "language-id"))) {
      lm = gtk_source_language_manager_get_default();
      lang = gtk_source_language_manager_get_language(lm, lang_id);
      gtk_source_buffer_set_language(priv->buffer, lang);
      gb_menu_button_set_label(GB_MENU_BUTTON(priv->lang_combo),
                               gtk_source_language_get_name(lang));
   }

   EXIT;
}

static void
gb_view_source_tab_size_activate (GtkMenuItem  *menu_item,
                                  GbViewSource *source)
{
   GbViewSourcePrivate *priv;
   const gchar *label;
   gint size;

   ENTRY;

   g_return_if_fail(GTK_IS_MENU_ITEM(menu_item));
   g_return_if_fail(GB_IS_VIEW_SOURCE(source));

   priv = source->priv;

   label = gtk_menu_item_get_label(menu_item);
   size = g_ascii_strtoll(label, NULL, 10);

   if (size > 0) {
      gtk_source_view_set_tab_width(GTK_SOURCE_VIEW(priv->source1), size);
      if (priv->source2) {
         gtk_source_view_set_tab_width(GTK_SOURCE_VIEW(priv->source2), size);
      }
      g_object_set(priv->size_combo,
                   "label", label,
                   NULL);
   }

   EXIT;
}

static void
gb_view_source_spaces_toggled (GtkMenuItem  *menu_item,
                               GbViewSource *source)
{
   GbViewSourcePrivate *priv;
   gboolean active;

   g_return_if_fail(GB_IS_VIEW_SOURCE(source));
   g_return_if_fail(GTK_IS_MENU_ITEM(menu_item));

   priv = source->priv;

   g_object_get(menu_item,
                "active", &active,
                NULL);
   g_object_set(priv->source1,
                "insert-spaces-instead-of-tabs", active,
                NULL);
   if (priv->source2) {
      g_object_set(priv->source2,
                   "insert-spaces-instead-of-tabs", active,
                   NULL);
   }
}

/**
 * gb_view_source_init:
 * @: (in): A #GbViewSource.
 *
 * Initializes the newly created #GbViewSource instance.
 */
static void
gb_view_source_init (GbViewSource *source)
{
   GbViewSourcePrivate *priv;
   GtkWidget *overlay;

   ENTRY;

   source->priv = priv =
      G_TYPE_INSTANCE_GET_PRIVATE(source,
                                  GB_TYPE_VIEW_SOURCE,
                                  GbViewSourcePrivate);

   g_object_set(source,
                "icon-name", GTK_STOCK_FILE,
                "name", _("Unsaved Document"),
                NULL);

   priv->buffer = gtk_source_buffer_new(NULL);
   g_signal_connect_swapped(priv->buffer,
                            "notify::cursor-position",
                            G_CALLBACK(gb_view_source_notify_cursor),
                            source);
   g_signal_connect_swapped(priv->buffer, "modified-changed",
                            G_CALLBACK(gb_view_source_buffer_modified_changed),
                            source);

   overlay = g_object_new(GTK_TYPE_OVERLAY,
                          "visible", TRUE,
                          NULL);
   g_signal_connect(overlay, "get-child-position",
                    G_CALLBACK(gb_view_source_position_progress),
                    NULL);
   gtk_container_add_with_properties(GTK_CONTAINER(source), overlay,
                                     "left-attach", 0,
                                     "width", 1,
                                     "top-attach", 0,
                                     "height", 1,
                                     NULL);

   priv->vpaned = g_object_new(GTK_TYPE_VPANED,
                               "visible", TRUE,
                               NULL);
   gtk_container_add(GTK_CONTAINER(overlay), priv->vpaned);

   priv->progress = g_object_new(GB_TYPE_PROGRESS_BAR,
                                 "visible", FALSE,
                                 NULL);
   gtk_overlay_add_overlay(GTK_OVERLAY(overlay), priv->progress);

   priv->scroller1 = g_object_new(gb_get_scrolled_window_gtype(),
                                  "expand", TRUE,
                                  "visible", TRUE,
                                  NULL);
   gtk_container_add_with_properties(GTK_CONTAINER(priv->vpaned), priv->scroller1,
                                     "shrink", FALSE,
                                     "resize", TRUE,
                                     NULL);

   priv->source1 = gb_view_source_create_source_view(source);
   gtk_container_add(GTK_CONTAINER(priv->scroller1), priv->source1);

   {
      GtkSourceLanguageManager *langs = gtk_source_language_manager_get_default();
      const gchar * const * ids = gtk_source_language_manager_get_language_ids(langs);
      GtkSourceLanguage *lang;
      const gchar *section;
      GHashTable *section_menu;
      GPtrArray *sorted;
      GtkWidget *controls = gb_view_get_controls(GB_VIEW(source));
      GtkWidget *menu;
      GtkWidget *mitem;
      GtkWidget *submenu;
      guint i;

      section_menu = g_hash_table_new(g_str_hash, g_str_equal);
      menu = gtk_menu_new();

      sorted = g_ptr_array_new();
      for (i = 0; ids[i]; i++) {
         lang = gtk_source_language_manager_get_language(langs, ids[i]);
         g_ptr_array_add(sorted, lang);
      }
      g_ptr_array_sort(sorted, lang_sort_func);
      for (i = 0; i < sorted->len; i++) {
         lang = g_ptr_array_index(sorted, i);
         section = gtk_source_language_get_section(lang);
         if (!(submenu = g_hash_table_lookup(section_menu, section))) {
            submenu = gtk_menu_new();
            mitem = g_object_new(GTK_TYPE_MENU_ITEM,
                                 "label", section,
                                 "submenu", submenu,
                                 "visible", TRUE,
                                 NULL);
            gtk_container_add(GTK_CONTAINER(menu), mitem);
            g_hash_table_insert(section_menu, (gchar*)section, submenu);
         }
         mitem = g_object_new(GTK_TYPE_MENU_ITEM,
                              "label", gtk_source_language_get_name(lang),
                              "visible", TRUE,
                              NULL);
         g_signal_connect_swapped(mitem, "activate",
                                  G_CALLBACK(gb_view_source_lang_activate),
                                  source);
         g_object_set_data(G_OBJECT(mitem), "language-id",
                           (gchar *)gtk_source_language_get_id(lang));
         gtk_container_add(GTK_CONTAINER(submenu), mitem);
      }
      g_ptr_array_free(sorted, TRUE);

      priv->lang_combo = g_object_new(GB_TYPE_MENU_BUTTON,
                                      "hexpand", FALSE,
                                      "label", "",
                                      "menu", menu,
                                      "tooltip-text", _("Set source language."),
                                      "visible", TRUE,
                                      NULL);
      gb_widget_shrink_font(
         gb_menu_button_get_label_widget(
            GB_MENU_BUTTON(priv->lang_combo)));

      gtk_style_context_add_class(gtk_widget_get_style_context(priv->lang_combo),
                                  GB_STYLE_CLASS_TOP_BAR);
      gtk_container_add_with_properties(GTK_CONTAINER(controls), priv->lang_combo,
                                        "expand", FALSE,
                                        NULL);

      g_hash_table_unref(section_menu);
   }

   {
      GtkWidget *controls = gb_view_get_controls(GB_VIEW(source));
      GtkWidget *b;
      GtkWidget *item;
      GtkWidget *menu;
      GtkWidget *group = NULL;
      gboolean active;
      gchar *str;
      int tabs[] = { 2, 3, 4, 5, 8, 0 };
      int i;

      b = g_object_new(GB_TYPE_MENU_BUTTON,
                       "hexpand", FALSE,
                       "label", "4",
                       "tooltip-text", _("Set tab-character width."),
                       "visible", TRUE,
                       "width-request", 45,
                       NULL);

      menu = gtk_menu_new();
      for (i = 0; tabs[i]; i++) {
         str = g_strdup_printf("%d", tabs[i]);
         item = g_object_new(GTK_TYPE_RADIO_MENU_ITEM,
                             "group", group,
                             "label", str,
                             "visible", TRUE,
                             NULL);
         if (!group) {
            group = item;
         }
         active = (tabs[i] == gtk_source_view_get_tab_width(
                   GTK_SOURCE_VIEW(priv->source1)));
         g_object_set(item, "active", active, NULL);
         if (active) {
            g_object_set(b, "label", str, NULL);
         }
         g_signal_connect(item, "activate",
                          G_CALLBACK(gb_view_source_tab_size_activate),
                          source);
         gtk_container_add(GTK_CONTAINER(menu), item);
         g_free(str);
      }
      g_object_set(b, "menu", menu, NULL);

      item = g_object_new(GTK_TYPE_SEPARATOR_MENU_ITEM,
                          "visible", TRUE,
                          NULL);
      gtk_container_add(GTK_CONTAINER(menu), item);

      item = g_object_new(GTK_TYPE_CHECK_MENU_ITEM,
                          "active", TRUE,
                          "label", _("Use Spaces"),
                          "visible", TRUE,
                          NULL);
      g_signal_connect(item, "toggled",
                       G_CALLBACK(gb_view_source_spaces_toggled),
                       source);
      gtk_container_add(GTK_CONTAINER(menu), item);

      gb_widget_shrink_font(gb_menu_button_get_label_widget(GB_MENU_BUTTON(b)));
      gtk_style_context_add_class(gtk_widget_get_style_context(b),
                                  GB_STYLE_CLASS_TOP_BAR);
      gtk_container_add_with_properties(GTK_CONTAINER(controls), b,
                                        "expand", FALSE,
                                        NULL);
      priv->size_combo = b;
   }

   {
      GtkWidget *controls = gb_view_get_controls(GB_VIEW(source));
      GtkWidget *img;
      GtkWidget *b;

      img = g_object_new(GTK_TYPE_IMAGE,
                         "icon-name", "gb-split",
                         "icon-size", GTK_ICON_SIZE_MENU,
                         "visible", TRUE,
                         NULL);
      b = g_object_new(GTK_TYPE_TOGGLE_BUTTON,
                       "child", img,
                       "tooltip-text", _("Split the editor into two views."),
                       "visible", TRUE,
                       NULL);
      gtk_style_context_add_class(gtk_widget_get_style_context(b),
                                  GB_STYLE_CLASS_TOP_BAR);
      gtk_container_add_with_properties(GTK_CONTAINER(controls), b,
                                        "expand", FALSE,
                                        NULL);
      g_signal_connect(b, "toggled",
                       G_CALLBACK(split_toggled),
                       source);
   }

   {
      GtkWidget *hbox;
      GtkWidget *vp;
      GtkWidget *c;

      hbox = g_object_new(GTK_TYPE_HBOX,
                          "height-request", 30,
                          "visible", TRUE,
                          NULL);
      gtk_container_add_with_properties(GTK_CONTAINER(source), hbox,
                                        "height", 1,
                                        "left-attach", 0,
                                        "top-attach", 1,
                                        "width", 1,
                                        NULL);

      gtk_container_add(GTK_CONTAINER(hbox),
                        (c = g_object_new(GTK_TYPE_COMBO_BOX,
                                          "visible", TRUE,
                                          NULL)));
      gtk_style_context_add_class(gtk_widget_get_style_context(c),
                                  GB_STYLE_CLASS_BOTTOM_BAR);

      vp = g_object_new(GTK_TYPE_VIEWPORT,
                        "visible", TRUE,
                        NULL);
      gtk_style_context_add_class(gtk_widget_get_style_context(vp),
                                  GB_STYLE_CLASS_BOTTOM_BAR);
      gtk_container_add_with_properties(GTK_CONTAINER(hbox), vp,
                                        "expand", FALSE,
                                        "fill", TRUE,
                                        NULL);

      priv->pos_label = g_object_new(GTK_TYPE_LABEL,
                                     "label", _("Line 1, Column 1"),
                                     "visible", TRUE,
                                     "xalign", 1.0f,
                                     "xpad", 6,
                                     NULL);
      gb_widget_shrink_font(priv->pos_label);
      gtk_container_add(GTK_CONTAINER(vp), priv->pos_label);
   }

   priv->theme_handler =
      g_signal_connect_swapped(gtk_settings_get_default(),
                               "notify::gtk-application-prefer-dark-theme",
                               G_CALLBACK(gb_view_source_update_style),
                               source);

   EXIT;
}
