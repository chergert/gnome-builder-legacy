/* gb-progress-bar.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-progress-bar.h"

G_DEFINE_TYPE(GbProgressBar, gb_progress_bar, GTK_TYPE_DRAWING_AREA)

struct _GbProgressBarPrivate
{
   gdouble fraction;
};

enum
{
   PROP_0,
   PROP_FRACTION,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

gdouble
gb_progress_bar_get_fraction (GbProgressBar *bar)
{
   g_return_val_if_fail(GB_IS_PROGRESS_BAR(bar), 0.0);
   return bar->priv->fraction;
}

void
gb_progress_bar_set_fraction (GbProgressBar *bar,
                              gdouble        fraction)
{
   g_return_if_fail(GB_IS_PROGRESS_BAR(bar));
   g_return_if_fail(fraction >= 0.0);
   g_return_if_fail(fraction <= 1.0);

   bar->priv->fraction = fraction;
   g_object_notify_by_pspec(G_OBJECT(bar), gParamSpecs[PROP_FRACTION]);
   gtk_widget_queue_draw(GTK_WIDGET(bar));
}

static gboolean
gb_progress_bar_draw (GtkWidget *widget,
                      cairo_t   *cr)
{
   GbProgressBarPrivate *priv;
   GtkStyleContext *context;
   GbProgressBar *bar = (GbProgressBar *)widget;
   GtkAllocation allocation;
   GtkStateType state;
   GdkRGBA color = { 0 };

   g_return_val_if_fail(GB_IS_PROGRESS_BAR(bar), FALSE);

   priv = bar->priv;

   if (gtk_widget_is_drawable(widget)) {
      gtk_widget_get_allocation(widget, &allocation);

      state = gtk_widget_get_state(widget);
      context = gtk_widget_get_style_context(widget);
      gtk_style_context_get_color(context, state, &color);

      cairo_save(cr);
      cairo_rectangle(cr,
                      0,
                      0,
                      priv->fraction * allocation.width,
                      allocation.height);
      gdk_cairo_set_source_rgba(cr, &color);
      cairo_fill(cr);
      cairo_restore(cr);
   }

   return FALSE;
}

static void
gb_progress_bar_finalize (GObject *object)
{
   G_OBJECT_CLASS(gb_progress_bar_parent_class)->finalize(object);
}

static void
gb_progress_bar_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
   GbProgressBar *bar = GB_PROGRESS_BAR(object);

   switch (prop_id) {
   case PROP_FRACTION:
      g_value_set_double(value, gb_progress_bar_get_fraction(bar));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_progress_bar_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
   GbProgressBar *bar = GB_PROGRESS_BAR(object);

   switch (prop_id) {
   case PROP_FRACTION:
      gb_progress_bar_set_fraction(bar, g_value_get_double(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_progress_bar_class_init (GbProgressBarClass *klass)
{
   GtkWidgetClass *widget_class;
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_progress_bar_finalize;
   object_class->get_property = gb_progress_bar_get_property;
   object_class->set_property = gb_progress_bar_set_property;
   g_type_class_add_private(object_class, sizeof(GbProgressBarPrivate));

   widget_class = GTK_WIDGET_CLASS(klass);
   widget_class->draw = gb_progress_bar_draw;

   /**
    * GbProgressBar:fraction:
    *
    * The "fraction" property contains the percentage of the progress bar,
    * from 0.0 to 1.0.
    */
   gParamSpecs[PROP_FRACTION] =
      g_param_spec_double("fraction",
                          _("Fraction"),
                          _("The fraction of the progress."),
                          0.0,
                          1.0,
                          0.0,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_FRACTION,
                                   gParamSpecs[PROP_FRACTION]);
}

static void
gb_progress_bar_init (GbProgressBar *bar)
{
   bar->priv =
      G_TYPE_INSTANCE_GET_PRIVATE(bar,
                                  GB_TYPE_PROGRESS_BAR,
                                  GbProgressBarPrivate);

   g_object_set(bar, "height-request", 2, NULL);

   bar->priv->fraction = 0.5;
}
