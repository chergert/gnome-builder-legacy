/* gb-view-stack.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_VIEW_STACK_H
#define GB_VIEW_STACK_H

#include <gtk/gtk.h>

#include "gb-view.h"

G_BEGIN_DECLS

#define GB_TYPE_VIEW_STACK            (gb_view_stack_get_type())
#define GB_VIEW_STACK(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_VIEW_STACK, GbViewStack))
#define GB_VIEW_STACK_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_VIEW_STACK, GbViewStack const))
#define GB_VIEW_STACK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_VIEW_STACK, GbViewStackClass))
#define GB_IS_VIEW_STACK(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_VIEW_STACK))
#define GB_IS_VIEW_STACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_VIEW_STACK))
#define GB_VIEW_STACK_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_VIEW_STACK, GbViewStackClass))

typedef struct _GbViewStack        GbViewStack;
typedef struct _GbViewStackClass   GbViewStackClass;
typedef struct _GbViewStackPrivate GbViewStackPrivate;

struct _GbViewStack
{
   GtkVBox parent;

   /*< private >*/
   GbViewStackPrivate *priv;
};

struct _GbViewStackClass
{
   GtkVBoxClass parent_class;
};

GType      gb_view_stack_get_type       (void) G_GNUC_CONST;
GtkWidget *gb_view_stack_get_active     (GbViewStack *stack);
gboolean   gb_view_stack_contains_view  (GbViewStack *stack,
                                         GbView      *view);
void       gb_view_stack_remove_view    (GbViewStack *stack,
                                         GbView      *view);
guint      gb_view_stack_get_n_views    (GbViewStack *stack);
gboolean   gb_view_stack_focus_first    (GbViewStack *stack);
gboolean   gb_view_stack_focus_next     (GbViewStack *stack);
gboolean   gb_view_stack_focus_previous (GbViewStack *stack);
gboolean   gb_view_stack_focus_view     (GbViewStack *stack,
                                         GbView      *view);
GList     *gb_view_stack_get_views      (GbViewStack *stack);

G_END_DECLS

#endif /* GB_VIEW_STACK_H */
