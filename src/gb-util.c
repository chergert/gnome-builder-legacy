/* gb-util.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gb-log.h"
#include "gb-scrolled-window.h"
#include "gb-util.h"

#include "build_16x.h"
#include "project_16x.h"
#include "run_16x.h"
#include "split_16x.h"
#include "text_x_chdr_16x.h"
#include "text_x_csrc_16x.h"

static GHashTable *gSuffixToIcon;

void
gb_widget_shrink_font (GtkWidget *widget)
{
   PangoFontDescription *f;
   GtkStyleContext *s;

   g_return_if_fail(GTK_IS_WIDGET(widget));

   s = gtk_widget_get_style_context(widget);
   f = pango_font_description_copy(gtk_style_context_get_font(s, GTK_STATE_NORMAL));
   pango_font_description_set_size(f, PANGO_SCALE_SMALL * pango_font_description_get_size(f));
   gtk_widget_override_font(widget, f);
   pango_font_description_free(f);
}

void
gb_widget_monospace_font (GtkWidget *widget)
{
   PangoFontDescription *f;
   GtkStyleContext *s;

   g_return_if_fail(GTK_IS_WIDGET(widget));

   s = gtk_widget_get_style_context(widget);
   f = pango_font_description_copy(gtk_style_context_get_font(s, GTK_STATE_NORMAL));
   pango_font_description_set_family(f, "Monospace");
   gtk_widget_override_font(widget, f);
   pango_font_description_free(f);
}

static void
gb_theme_load_css (const gchar *name)
{
   GtkCssProvider *css_provider;
   gchar *path;

   ENTRY;

   g_return_if_fail(name != NULL);

   /*
    * Try to load from the build directory.
    */
   path = g_build_filename("data", "css", name, NULL);
   if (g_file_test(path, G_FILE_TEST_IS_REGULAR)) {
      GOTO(load_css);
   }
   g_free(path);

   /*
    * Try to load from installation.
    */
   path = g_build_filename(PACKAGE_DATA_DIR,
                           "gnome-builder",
                           "css",
                           name,
                           NULL);
   if (g_file_test(path, G_FILE_TEST_IS_REGULAR)) {
      GOTO(load_css);
   }
   g_free(path);

   EXIT;

load_css:
   css_provider = gtk_css_provider_new();
   gtk_css_provider_load_from_path(css_provider, path, NULL);
   g_free(path);
   gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
                                             GTK_STYLE_PROVIDER(css_provider),
                                             GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

   EXIT;
}

static gboolean
grab_focus (gpointer data)
{
   gtk_widget_grab_focus(data);
   g_object_unref(data);
   return FALSE;
}

void
gb_widget_grab_focus_in_idle (GtkWidget *widget)
{
   g_idle_add(grab_focus, g_object_ref(widget));
}

void
gb_widget_add_style_class (GtkWidget   *widget,
                           const gchar *style_class)
{
   gtk_style_context_add_class(gtk_widget_get_style_context(widget),
                               style_class);
}

void
gb_theme_init (void)
{
   /*
    * Load builtin icons.
    */
#define REGISTER_ICON(_name, _size, _data)                               \
   gtk_icon_theme_add_builtin_icon(_name, _size,                         \
                                   gdk_pixbuf_new_from_inline(-1, _data, \
                                                              FALSE, NULL))

   REGISTER_ICON("gb-build", 16, build_16x);
   REGISTER_ICON("gb-project", 16, project_16x);
   REGISTER_ICON("gb-run", 16, run_16x);
   REGISTER_ICON("gb-split", 16, split_16x);
   REGISTER_ICON("text-x-chdr", 16, text_x_chdr_16x);
   REGISTER_ICON("text-x-csrc", 16, text_x_csrc_16x);

#undef REGISTER_ICON

   /*
    * Set optional dark theme.
    */
#if 0
   g_object_set(gtk_settings_get_default(),
                "gtk-application-prefer-dark-theme", TRUE,
                NULL);
#endif

   /*
    * Load CSS overrides.
    */
   gb_theme_load_css("gtk-overrides.css");
}

const gchar *
gb_path_get_icon_name (const gchar *path)
{
   static gsize initialized = FALSE;
   const gchar *icon_name = NULL;
   const gchar *suffix;
   gchar **parts;
   gchar *name;
   gint length;

   if (g_once_init_enter(&initialized)) {
      gSuffixToIcon = g_hash_table_new_full(g_str_hash, g_str_equal,
                                            g_free, g_free);

#define ADD_ICON_FOR_SUFFIX(icon, suffix) \
         g_hash_table_insert(gSuffixToIcon, (gchar *)suffix, (gchar *)icon)

      ADD_ICON_FOR_SUFFIX("text-x-csrc", "c");
      ADD_ICON_FOR_SUFFIX("text-x-csrc", "cc");
      ADD_ICON_FOR_SUFFIX("text-x-csrc", "cpp");
      ADD_ICON_FOR_SUFFIX("text-x-chdr", "h");
      ADD_ICON_FOR_SUFFIX("text-x-chdr", "hh");
      ADD_ICON_FOR_SUFFIX("text-x-chdr", "hpp");
      ADD_ICON_FOR_SUFFIX("gnome-mime-text-x-python", "py");
      ADD_ICON_FOR_SUFFIX("gnome-mime-application-x-python-bytecode", "pyc");

#undef ADD_ICON_FOR_SUFFIX

      g_once_init_leave(&initialized, TRUE);
   }

   name = g_path_get_basename(path);
   parts = g_strsplit(name, ".", 0);
   if ((length = g_strv_length(parts))) {
      suffix = parts[length - 1];
      icon_name = g_hash_table_lookup(gSuffixToIcon, suffix);
   }

   g_free(name);
   g_strfreev(parts);

   return icon_name;
}

gboolean
gb_container_has_children (GtkContainer *container)
{
   gboolean ret;
   GList *children;

   children = gtk_container_get_children(container);
   ret = !!children;
   g_list_free(children);
   return ret;
}

GType
gb_get_scrolled_window_gtype (void)
{
   if (!gtk_check_version(3, 3, 14)) {
      return GTK_TYPE_SCROLLED_WINDOW;
   }
   return GB_TYPE_SCROLLED_WINDOW;
}
