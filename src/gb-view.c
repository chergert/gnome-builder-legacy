/* gb-view.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <libpeas/peas.h>

#include "gb-log.h"
#include "gb-util.h"
#include "gb-view.h"
#include "gb-view-plugin.h"
#include "gb-window.h"

G_DEFINE_TYPE(GbView, gb_view, GTK_TYPE_GRID)

struct _GbViewPrivate
{
   PeasExtensionSet *plugins;
   GtkWidget        *controls;
   gchar            *icon_name;
   gchar            *name;
   gboolean          can_save;
   gboolean          can_search;
};

enum
{
   PROP_0,
   PROP_CAN_SAVE,
   PROP_CAN_SEARCH,
   PROP_ICON_NAME,
   PROP_NAME,
   LAST_PROP
};

enum
{
   CLOSED,
   LAST_SIGNAL
};

static GParamSpec *gParamSpecs[LAST_PROP];
static guint       gSignals[LAST_SIGNAL];

gboolean
gb_view_is_file (GbView *view,
                 GFile  *file)
{
   GbViewClass *klass;
   gboolean ret = FALSE;

   g_return_val_if_fail(GB_IS_VIEW(view), FALSE);

   if ((klass = GB_VIEW_GET_CLASS(view))->is_file) {
      ret = klass->is_file(view, file);
   }

   return ret;
}

void
gb_view_emit_closed (GbView *view)
{
   ENTRY;
   g_return_if_fail(GB_IS_VIEW(view));
   g_signal_emit(view, gSignals[CLOSED], 0);
   EXIT;
}

/**
 * gb_view_close:
 * @view: (in): A #GbView.
 *
 * Attempt to close the view. The view may keep itself open while asking
 * the user for more information.
 */
void
gb_view_close (GbView *view)
{
   ENTRY;

   g_return_if_fail(GB_IS_VIEW(view));

   if (GB_VIEW_GET_CLASS(view)->close) {
      GB_VIEW_GET_CLASS(view)->close(view);
   } else {
      gb_view_emit_closed(view);
   }

   EXIT;
}

/**
 * gb_view_save:
 * @view: (in): A #GbView.
 *
 * Requests that the view save the contents of the view.
 */
void
gb_view_save (GbView *view)
{
   ENTRY;

   g_return_if_fail(GB_IS_VIEW(view));

   if (GB_VIEW_GET_CLASS(view)->save) {
      GB_VIEW_GET_CLASS(view)->save(view);
   }

   EXIT;
}

/**
 * gb_view_get_can_save:
 * @view: (in): A #GbView.
 *
 * Checks to see if the view supports saving in its current state.
 *
 * Returns: %TRUE if the #GbView can save.
 */
gboolean
gb_view_get_can_save (GbView *view)
{
   g_return_val_if_fail(GB_IS_VIEW(view), FALSE);
   return view->priv->can_save;
}

/**
 * gb_view_set_can_save:
 * @view: (in): A #GbView.
 * @can_save: (in): If the view can be saved.
 *
 * Marks the view as having the ability to be saved in its current state.
 */
void
gb_view_set_can_save (GbView   *view,
                      gboolean  can_save)
{
   g_return_if_fail(GB_IS_VIEW(view));
   view->priv->can_save = can_save;
   g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_CAN_SAVE]);
}

/**
 * gb_view_get_can_search:
 * @view: (in): A #GbView.
 *
 * Gets the "can-search" property. This idicates that the view supports the
 * search command.
 *
 * Returns: %TRUE if @view can perform search.
 */
gboolean
gb_view_get_can_search (GbView *view)
{
   ENTRY;
   g_return_val_if_fail(GB_IS_VIEW(view), FALSE);
   RETURN(view->priv->can_search);
}

/**
 * gb_view_set_can_search:
 * @view: (in): A #GbView.
 * @can_search: (in): A boolean.
 *
 * Sets if @view can perform a search given its current state.
 */
void
gb_view_set_can_search (GbView   *view,
                        gboolean  can_search)
{
   ENTRY;
   g_return_if_fail(GB_IS_VIEW(view));
   view->priv->can_search = can_search;
   g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_CAN_SEARCH]);
   EXIT;
}

/**
 * gb_view_get_controls:
 * @view: (in): A #GbView.
 *
 * Gets an associated controls for the view that should be packed into a
 * toolbar. This is packed next to the combo box in #GbViewStack.
 *
 * Returns: (transfer none): A #GtkWidget or %NULL.
 */
GtkWidget *
gb_view_get_controls (GbView *view)
{
   ENTRY;
   g_return_val_if_fail(GB_IS_VIEW(view), NULL);
   RETURN(view->priv->controls);
}

/**
 * gb_view_get_icon_name:
 * @view: (in): A #GbView.
 *
 * Gets the "icon-name" property which will be used to show an icon next
 * to the views name.
 *
 * Returns: A string containing a stock icon name.
 */
const gchar *
gb_view_get_icon_name (GbView *view)
{
   g_return_val_if_fail(GB_IS_VIEW(view), NULL);
   return view->priv->icon_name;
}

/**
 * gb_view_set_icon_name:
 * @view: (in): A #GbView.
 * @icon_name: (in) (allow-none): A string or %NULL.
 *
 * Sets the "icon-name" property. The "icon-name" property is the name
 * of a stock icon that can be displayed next to the views name.
 */
void
gb_view_set_icon_name (GbView      *view,
                       const gchar *icon_name)
{
   ENTRY;

   g_return_if_fail(GB_IS_VIEW(view));

   g_free(view->priv->icon_name);
   view->priv->icon_name = g_strdup(icon_name);
   g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_ICON_NAME]);

   EXIT;
}

/**
 * gb_view_get_name:
 * @view: (in): A #GbView.
 *
 * Gets the "name" property of the view. The name should be a description of
 * the view. For an editor view, this would typically be the filename.
 *
 * Returns: A string or %NULL.
 */
const gchar *
gb_view_get_name (GbView *view)
{
   g_return_val_if_fail(GB_IS_VIEW(view), NULL);
   return view->priv->name;
}

/**
 * gb_view_set_name:
 * @view: (in): A #GbView.
 * @name: (in): A String.
 *
 * Sets the "name" property for the view.
 */
void
gb_view_set_name (GbView      *view,
                  const gchar *name)
{
   ENTRY;

   g_return_if_fail(GB_IS_VIEW(view));

   g_free(view->priv->name);
   view->priv->name = g_strdup(name);
   g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_NAME]);
}

static void
gb_view_plugin_added (GbView           *view,
                      PeasPluginInfo   *plugin_info,
                      GObject          *exten,
                      PeasExtensionSet *plugins)
{
   gb_view_plugin_activate(GB_VIEW_PLUGIN(exten));
}

static void
gb_view_plugin_removed (GbView           *view,
                        PeasPluginInfo   *plugin_info,
                        GObject          *exten,
                        PeasExtensionSet *plugins)
{
   gb_view_plugin_deactivate(GB_VIEW_PLUGIN(exten));
}

static void
gb_view_load_plugin (PeasExtensionSet *plugins,
                     PeasPluginInfo   *plugin_info,
                     PeasExtension    *exten,
                     gpointer          data)
{
   gb_view_plugin_added(data, plugin_info, G_OBJECT(exten), plugins);
}

static void
gb_view_load_plugins (GbView   *view,
                      GbWindow *window)
{
   GbViewPrivate *priv;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW(view));
   g_return_if_fail(GB_IS_WINDOW(window));

   priv = view->priv;

   priv->plugins = peas_extension_set_new(peas_engine_get_default(),
                                          GB_TYPE_VIEW_PLUGIN,
                                          "view", view,
                                          "window", window,
                                          NULL);
   g_signal_connect_swapped(priv->plugins, "extension-added",
                            G_CALLBACK(gb_view_plugin_added),
                            view);
   g_signal_connect_swapped(priv->plugins, "extension-removed",
                            G_CALLBACK(gb_view_plugin_removed),
                            view);
   peas_extension_set_foreach(priv->plugins,
                              gb_view_load_plugin,
                              view);

   EXIT;
}

static void
gb_view_parent_set (GtkWidget *widget,
                    GtkWidget *previous_parent)
{
   GbWindow *window;
   GbView *view = (GbView *)widget;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW(view));

   if (!view->priv->plugins) {
      if (GB_IS_WINDOW(gtk_widget_get_toplevel(widget))) {
         window = GB_WINDOW(gtk_widget_get_toplevel(widget));
         gb_view_load_plugins(view, window);
      }
   }

   EXIT;
}

/**
 * gb_view_destroy:
 * @widget: (in): A #GbView.
 *
 * Handles the "destroy" signal for the widget. Non-direct children such
 * as the controls are released.
 */
static void
gb_view_destroy (GtkWidget *widget)
{
   GbView *view = (GbView *)widget;

   ENTRY;
   g_return_if_fail(GB_IS_VIEW(view));
   g_clear_object(&view->priv->controls);
   g_clear_object(&view->priv->plugins);
   GTK_WIDGET_CLASS(gb_view_parent_class)->destroy(widget);
   EXIT;
}

/**
 * gb_view_finalize:
 * @object: (in): A #GbView.
 *
 * Finalizer for a #GbView instance.
 */
static void
gb_view_finalize (GObject *object)
{
   GbViewPrivate *priv;

   ENTRY;

   priv = GB_VIEW(object)->priv;

   g_free(priv->icon_name);
   g_free(priv->name);

   G_OBJECT_CLASS(gb_view_parent_class)->finalize(object);

   EXIT;
}

/**
 * gb_view_get_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (out): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Get a given #GObject property.
 */
static void
gb_view_get_property (GObject    *object,
                      guint       prop_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
   GbView *view = GB_VIEW(object);

   switch (prop_id) {
   case PROP_CAN_SAVE:
      g_value_set_boolean(value, gb_view_get_can_save(view));
      break;
   case PROP_CAN_SEARCH:
      g_value_set_boolean(value, gb_view_get_can_search(view));
      break;
   case PROP_ICON_NAME:
      g_value_set_string(value, gb_view_get_icon_name(view));
      break;
   case PROP_NAME:
      g_value_set_string(value, gb_view_get_name(view));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * gb_view_set_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (in): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Set a given #GObject property.
 */
static void
gb_view_set_property (GObject      *object,
                      guint         prop_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
   GbView *view = GB_VIEW(object);

   switch (prop_id) {
   case PROP_CAN_SAVE:
      gb_view_set_can_save(view, g_value_get_boolean(value));
      break;
   case PROP_CAN_SEARCH:
      gb_view_set_can_search(view, g_value_get_boolean(value));
      break;
   case PROP_ICON_NAME:
      gb_view_set_icon_name(view, g_value_get_string(value));
      break;
   case PROP_NAME:
      gb_view_set_name(view, g_value_get_string(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * gb_view_class_init:
 * @klass: (in): A #GbViewClass.
 *
 * Initializes the #GbViewClass and prepares the vtable.
 */
static void
gb_view_class_init (GbViewClass *klass)
{
   GObjectClass *object_class;
   GtkWidgetClass *widget_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_view_finalize;
   object_class->get_property = gb_view_get_property;
   object_class->set_property = gb_view_set_property;
   g_type_class_add_private(object_class, sizeof(GbViewPrivate));

   widget_class = GTK_WIDGET_CLASS(klass);
   widget_class->destroy = gb_view_destroy;
   widget_class->parent_set = gb_view_parent_set;

   /**
    * GbView:can-save:
    *
    * The "can-save" property indicates that the view can currently be saved.
    */
   gParamSpecs[PROP_CAN_SAVE] =
      g_param_spec_boolean("can-save",
                           _("Can Save"),
                           _("If the view can be saved."),
                           FALSE,
                           G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_CAN_SAVE,
                                   gParamSpecs[PROP_CAN_SAVE]);

   /**
    * GbView:can-search:
    *
    * The "can-search" property indicates that the view provides search
    * support and can be focused for a search when the search toolitem
    * is pressed.
    */
   gParamSpecs[PROP_CAN_SEARCH] =
      g_param_spec_boolean("can-search",
                           _("Can Search"),
                           _("If the view can perform searches."),
                           FALSE,
                           G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_CAN_SEARCH,
                                   gParamSpecs[PROP_CAN_SEARCH]);

   /**
    * GbView:icon-name:
    *
    * The "icon-name" of the image to display next to the file name.
    */
   gParamSpecs[PROP_ICON_NAME] =
      g_param_spec_string("icon-name",
                          _("Icon Name"),
                          _("The name of the icon to display."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_ICON_NAME,
                                   gParamSpecs[PROP_ICON_NAME]);

   /**
    * GbView:name:
    *
    * The name of the document or view.
    */
   gParamSpecs[PROP_NAME] =
      g_param_spec_string("name",
                          _("Name"),
                          _("The name of the document or view."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_NAME,
                                   gParamSpecs[PROP_NAME]);

   /**
    * GbView::closed:
    *
    * The closed signal is emitted when a close request has been completed.
    * Aternatively, if the view has had a request to close internally, this
    * signal will tell the container to remove the child. An example of this
    * would be a terminal whose child has exted.
    */
   gSignals[CLOSED] = g_signal_new("closed",
                                   GB_TYPE_VIEW,
                                   G_SIGNAL_RUN_LAST,
                                   0,
                                   NULL,
                                   NULL,
                                   g_cclosure_marshal_VOID__VOID,
                                   G_TYPE_NONE,
                                   0);
}

/**
 * gb_view_init:
 * @view: (in): A #GbView.
 *
 * Initializes the newly created #GbView instance.
 */
static void
gb_view_init (GbView *view)
{
   GbViewPrivate *priv;

   view->priv = priv =
      G_TYPE_INSTANCE_GET_PRIVATE(view,
                                  GB_TYPE_VIEW,
                                  GbViewPrivate);

   priv->controls = g_object_new(GTK_TYPE_HBOX,
                                 "visible", TRUE,
                                 NULL);
   g_object_ref(priv->controls);

   gtk_style_context_add_class(gtk_widget_get_style_context(priv->controls),
                               GB_STYLE_CLASS_TOP_CONTROLS);
}
