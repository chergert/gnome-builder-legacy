/* gb-window.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <vte/vte.h>

#include "gb-anim-bin.h"
#include "gb-animation.h"
#include "gb-application.h"
#include "gb-log.h"
#include "gb-menu-button.h"
#include "gb-progress-bar.h"
#include "gb-project.h"
#include "gb-scrolled-window.h"
#include "gb-tree.h"
#include "gb-tree-builder-project.h"
#include "gb-tree-node-project.h"
#include "gb-util.h"
#include "gb-view.h"
#include "gb-view-docs.h"
#include "gb-view-layout.h"
#include "gb-view-layout-edit.h"
#include "gb-view-source.h"
#include "gb-view-stack.h"
#include "gb-view-terminal.h"
#include "gb-window.h"
#include "gb-window-plugin.h"
#include "gb-window-ui.h"

G_DEFINE_TYPE(GbWindow, gb_window, GTK_TYPE_WINDOW)

struct _GbWindowPrivate
{
   GbProject *project;

   GtkUIManager   *ui_manager;
   GtkActionGroup *actions;

   GtkWidget *mb_tb_anim;
   GtkWidget *menubar;
   GtkWidget *toolbar;
   GtkWidget *layout;

   GtkWidget *sidebar;
   GtkWidget *progress;
   GtkWidget *project_button;
   GtkWidget *project_popup;
   GtkWidget *browser;
   GtkWidget *devhelp;

   PeasExtensionSet *plugins;

   GbView *view;
   guint view_notify;
};

enum
{
   PROP_0,
   PROP_BROWSER,
   PROP_PROJECT,
   PROP_SIDEBAR,
   PROP_VIEW,
   LAST_PROP
};

enum
{
   VIEW_CHANGED,
   LAST_SIGNAL
};

static GParamSpec *gParamSpecs[LAST_PROP];
static guint       gSignals[LAST_SIGNAL];

/**
 * gb_window_get_project:
 * @window: (in): A #GbWindow.
 *
 * Gets the #GbProject for @window.
 *
 * Returns: (transfer none): A #GbProject.
 */
GbProject *
gb_window_get_project (GbWindow *window)
{
   g_return_val_if_fail(GB_IS_WINDOW(window), NULL);
   return window->priv->project;
}

/**
 * gb_window_set_project:
 * @window: (in): A #GbWindow.
 * @project: (in): A #GbProject.
 *
 * Sets the project for a window. The project information will be loaded
 * into the project treeview and such.
 *
 * This can currently only be called once.
 */
static void
gb_window_set_project (GbWindow  *window,
                       GbProject *project)
{
   GbWindowPrivate *priv;
   GbTreeNode *root;

   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));
   g_return_if_fail(GB_IS_PROJECT(project));

   priv = window->priv;

   g_clear_object(&priv->project);
   priv->project = g_object_ref_sink(project);

   root = g_object_new(GB_TYPE_TREE_NODE_PROJECT,
                       "project", project,
                       NULL);
   g_object_set(priv->browser,
                "root", root,
                NULL);

   g_object_bind_property(project, "name", priv->project_button, "label",
                          G_BINDING_SYNC_CREATE);

   g_object_notify_by_pspec(G_OBJECT(window), gParamSpecs[PROP_PROJECT]);

   EXIT;
}

/**
 * gb_window_get_view_for_file:
 * @window: (in): A #GbWindow.
 * @file: (in): A #GFile.
 *
 * Locates a #GbView that is currently viewing @file. If no view could be
 * found then %NULL is returned.
 *
 * Returns: (transfer none): A #GbView or %NULL.
 */
GbView *
gb_window_get_view_for_file (GbWindow *window,
                             GFile    *file)
{
   GbWindowPrivate *priv;
   const GList *iter;
   GbView *ret = NULL;
   GList *views;

   g_return_val_if_fail(GB_IS_WINDOW(window), NULL);
   g_return_val_if_fail(G_IS_FILE(file), NULL);

   priv = window->priv;

   views = gb_view_layout_get_views(GB_VIEW_LAYOUT(priv->layout));

   for (iter = views; iter; iter = iter->next) {
      if (gb_view_is_file(iter->data, file)) {
         ret = iter->data;
         break;
      }
   }

   g_list_free(views);

   return ret;
}

static GbView *
gb_window_get_current_view (GbWindow *window)
{
   GtkWidget *focused;

   ENTRY;

   g_return_val_if_fail(GB_IS_WINDOW(window), NULL);

   focused = gtk_window_get_focus(GTK_WINDOW(window));
   for (; focused; focused = gtk_widget_get_parent(focused)) {
      if (GB_IS_VIEW(focused)) {
         RETURN(GB_VIEW(focused));
      }
   }

   RETURN(NULL);
}

/**
 * gb_window_build_clicked:
 * @window: (in): A #GbWindow.
 *
 * Handle the "clicked" signal from the build button. This should send a
 * build request if one has not yet been started.
 */
static void
gb_window_build_clicked (GbWindow  *window,
                         GtkWidget *button)
{
   GbWindowPrivate *priv;

   g_return_if_fail(GB_IS_WINDOW(window));

   priv = window->priv;

   /*
    * TODO: Send build request.
    */

   g_object_set(priv->progress, "fraction", 0.0, NULL);
   gb_object_animate(priv->progress, GB_ANIMATION_EASE_IN_OUT_QUAD, 5000,
                     "fraction", 1.0, NULL);
}

/**
 * gb_window_add_view:
 * @window: (in): A #GbWindow.
 * @view: (transfer full) (in): A #GbView.
 *
 * Adds a view to the window. The view will be placed according to
 * the current view layout.
 */
void
gb_window_add_view (GbWindow *window,
                    GbView   *view)
{
   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));
   g_return_if_fail(GB_IS_VIEW(view));

   /*
    * TODO: Use GbViewLayout.
    */

   gtk_container_add(GTK_CONTAINER(window->priv->layout),
                     GTK_WIDGET(view));
   gtk_widget_grab_focus(GTK_WIDGET(view));

   EXIT;
}

static void
gb_window_close_activate (GtkAction *action,
                          GbWindow  *window)
{
   GtkWidget *focused;

   ENTRY;

   focused = gtk_window_get_focus(GTK_WINDOW(window));
   for (; focused; focused = gtk_widget_get_parent(focused)) {
      if (GB_IS_VIEW(focused)) {
         gb_view_close(GB_VIEW(focused));
         break;
      }
   }

   EXIT;
}

static void
gb_window_close_all_activate (GtkAction *action,
                              GbWindow  *window)
{
   GbWindowPrivate *priv;
   GList *iter;
   GList *views;

   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));

   priv = window->priv;

   views = gb_view_layout_get_views(GB_VIEW_LAYOUT(priv->layout));
   for (iter = views; iter; iter = iter->next) {
      gb_view_close(GB_VIEW(iter->data));
   }
   g_list_free(views);

   EXIT;
}

static void
gb_window_quit_activate (GtkAction *action,
                         GbWindow  *window)
{
   ENTRY;
   /*
    * TODO: Check for unsaved documents.
    */
   g_application_quit(G_APPLICATION(GB_APPLICATION_DEFAULT));
   EXIT;
}

static void
gb_window_cut_activate (GtkAction *action,
                        GbWindow  *window)
{
   GtkWidget *focused;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   if ((focused = gtk_window_get_focus(GTK_WINDOW(window)))) {
      if (GTK_IS_EDITABLE(focused)) {
         gtk_editable_cut_clipboard(GTK_EDITABLE(focused));
      } else if (GTK_IS_TEXT_VIEW(focused)) {
         g_signal_emit_by_name(focused, "cut-clipboard", NULL);
      }
   }

   EXIT;
}

static void
gb_window_copy_activate (GtkAction *action,
                         GbWindow  *window)
{
   GtkWidget *focused;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   if ((focused = gtk_window_get_focus(GTK_WINDOW(window)))) {
      if (GTK_IS_EDITABLE(focused)) {
         gtk_editable_copy_clipboard(GTK_EDITABLE(focused));
      } else if (GTK_IS_TEXT_VIEW(focused)) {
         g_signal_emit_by_name(focused, "copy-clipboard", NULL);
      } else if (VTE_IS_TERMINAL(focused)) {
         vte_terminal_copy_clipboard(VTE_TERMINAL(focused));
      }
   }

   EXIT;
}

static void
gb_window_paste_activate (GtkAction *action,
                          GbWindow  *window)
{
   GtkWidget *focused;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   if ((focused = gtk_window_get_focus(GTK_WINDOW(window)))) {
      if (GTK_IS_EDITABLE(focused)) {
         gtk_editable_paste_clipboard(GTK_EDITABLE(focused));
      } else if (GTK_IS_TEXT_VIEW(focused)) {
         g_signal_emit_by_name(focused, "paste-clipboard", NULL);
      } else if (VTE_IS_TERMINAL(focused)) {
         vte_terminal_paste_clipboard(VTE_TERMINAL(focused));
      }
   }

   EXIT;
}

static void
gb_window_delete_activate (GtkAction *action,
                           GbWindow  *window)
{
   GtkWidget *focused;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   if ((focused = gtk_window_get_focus(GTK_WINDOW(window)))) {
      if (GTK_IS_EDITABLE(focused)) {
         gtk_editable_delete_selection(GTK_EDITABLE(focused));
      } else if (GTK_IS_TEXT_VIEW(focused)) {
         g_signal_emit_by_name(focused, "delete-from-cursor",
                               GTK_DELETE_CHARS, 1, NULL);
      }
   }

   EXIT;
}

static void
gb_window_move_right_activate (GtkAction *action,
                               GbWindow  *window)
{
   GbWindowPrivate *priv;
   GtkWidget *focused;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   priv = window->priv;

   focused = gtk_window_get_focus(GTK_WINDOW(window));
   for (; focused; focused = gtk_widget_get_parent(focused)) {
      if (GB_IS_VIEW(focused)) {
         gb_view_layout_move_right(GB_VIEW_LAYOUT(priv->layout),
                                   GB_VIEW(focused));
         gtk_widget_grab_focus(GTK_WIDGET(focused));
         break;
      }
   }

   EXIT;
}

static void
gb_window_new_terminal_activate (GtkAction *action,
                                 GbWindow  *window)
{
   GbView *view;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   view = g_object_new(GB_TYPE_VIEW_TERMINAL,
                       "visible", TRUE,
                       NULL);
   gb_window_add_view(window, view);

   EXIT;
}

static void
gb_window_new_file_activate (GtkAction *action,
                             GbWindow  *window)
{
   GbView *view;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   view = g_object_new(GB_TYPE_VIEW_SOURCE,
                       "visible", TRUE,
                       NULL);
   gb_window_add_view(window, view);

   EXIT;
}

static void
gb_window_save_activate (GtkAction *action,
                         GbWindow  *window)
{
   GbView *view;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   view = gb_window_get_current_view(window);
   if (gb_view_get_can_save(view)) {
      gb_view_save(view);
   }

   EXIT;
}

static void
gb_window_next_view_activate (GtkAction *action,
                              GbWindow  *window)
{
   GbWindowPrivate *priv;
   GbView *view;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   priv = window->priv;

   if ((view = gb_window_get_current_view(window))) {
      gb_view_layout_next_view(GB_VIEW_LAYOUT(priv->layout), view);
   }

   EXIT;
}

static void
gb_window_prev_view_activate (GtkAction *action,
                              GbWindow  *window)
{
   GbWindowPrivate *priv;
   GbView *view;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   priv = window->priv;

   if ((view = gb_window_get_current_view(window))) {
      gb_view_layout_previous_view(GB_VIEW_LAYOUT(priv->layout), view);
   }

   EXIT;
}

static void
gb_window_minimal_activate (GtkAction *action,
                            GbWindow  *window)
{
   gboolean visible;

   ENTRY;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   visible = gtk_widget_get_visible(window->priv->mb_tb_anim);
   gtk_widget_set_visible(window->priv->mb_tb_anim, !visible);

   EXIT;
}

static void
gb_window_devhelp_activate (GtkAction *action,
                            GbWindow  *window)
{
   GbWindowPrivate *priv;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   priv = window->priv;

   if (priv->devhelp) {
      /*
       * TODO: Ask layout to show view.
       */
   } else {
      priv->devhelp = g_object_new(GB_TYPE_VIEW_DOCS,
                                   "visible", TRUE,
                                   NULL);
      g_object_add_weak_pointer(G_OBJECT(priv->devhelp),
                                (gpointer *)&priv->devhelp);
      gb_window_add_view(window, GB_VIEW(priv->devhelp));
   }
}

static void
gb_window_fullscreen_activate (GtkAction *action,
                               GbWindow  *window)
{
   gboolean active;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   g_object_get(action, "active", &active, NULL);

#if 0
   g_object_set(gtk_settings_get_default(),
                "gtk-application-prefer-dark-theme", active,
                NULL);
#endif

   if (active) {
      gtk_window_fullscreen(GTK_WINDOW(window));
   } else {
      gtk_window_unfullscreen(GTK_WINDOW(window));
   }
}

static void
gb_window_sidebar_activate (GtkAction *action,
                            GbWindow  *window)
{
   gboolean active;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   g_object_get(action, "active", &active, NULL);
   gtk_widget_set_visible(window->priv->sidebar, active);
}

static void
gb_window_toolbar_activate (GtkAction *action,
                            GbWindow  *window)
{
   gboolean active;

   g_return_if_fail(GTK_IS_ACTION(action));
   g_return_if_fail(GB_IS_WINDOW(window));

   g_object_get(action, "active", &active, NULL);
   gtk_widget_set_visible(window->priv->toolbar, active);
}

static void
gb_window_open_activate (GtkAction *action,
                         GbWindow  *window)
{
   GtkWidget *dialog;
   GbView *view;
   GFile *file;

   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));
   g_return_if_fail(GTK_IS_ACTION(action));

   dialog = gtk_file_chooser_dialog_new(_("Open File"),
                                        GTK_WINDOW(window),
                                        GTK_FILE_CHOOSER_ACTION_OPEN,
                                        GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                        GTK_STOCK_OPEN, GTK_RESPONSE_OK,
                                        NULL);
   if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK) {
      file = gtk_file_chooser_get_file(GTK_FILE_CHOOSER(dialog));
      if (file) {
         view = g_object_new(GB_TYPE_VIEW_SOURCE,
                             "file", file,
                             "visible", TRUE,
                             NULL);
         gb_window_add_view(window, view);
         g_object_unref(file);
      }
   }
   gtk_widget_destroy(dialog);

   EXIT;
}

static void
gb_window_dark_activate (GtkToggleAction *action,
                         GbWindow        *window)
{
   gboolean toggled = gtk_toggle_action_get_active(action);
   g_object_set(gtk_settings_get_default(),
                "gtk-application-prefer-dark-theme", toggled,
                NULL);
}

static void
gb_window_init_actions (GbWindow *window)
{
   static const GtkActionEntry actions[] = {
      { "file", NULL, N_("_Builder") },
      { "file-open", GTK_STOCK_OPEN, NULL, NULL, NULL,
        G_CALLBACK(gb_window_open_activate) },
      { "new-terminal", "terminal", N_("New _Terminal"),
        N_("<primary><shift>t"), NULL,
        G_CALLBACK(gb_window_new_terminal_activate) },
      { "new-file", NULL, N_("New _File"), NULL, NULL,
        G_CALLBACK(gb_window_new_file_activate) },
      { "save", GTK_STOCK_SAVE, NULL, NULL, NULL,
        G_CALLBACK(gb_window_save_activate) },
      { "close", GTK_STOCK_CLOSE, NULL, N_("<primary>w"), NULL,
        G_CALLBACK(gb_window_close_activate) },
      { "close-all", GTK_STOCK_CLOSE, N_("Close All"),
        N_("<primary><shift>w"), N_("Close all views."),
        G_CALLBACK(gb_window_close_all_activate) },
      { "quit", GTK_STOCK_QUIT, NULL, N_("<primary>q"), NULL,
        G_CALLBACK(gb_window_quit_activate) },

      { "edit", GTK_STOCK_EDIT },
      { "cut", GTK_STOCK_CUT, NULL, "<primary><shift>x", NULL,
        G_CALLBACK(gb_window_cut_activate) },
      { "copy", GTK_STOCK_COPY, NULL, "<primary><shift>c", NULL,
        G_CALLBACK(gb_window_copy_activate) },
      { "paste", GTK_STOCK_PASTE, NULL, "<primary><shift>v", NULL,
        G_CALLBACK(gb_window_paste_activate) },
      { "delete", GTK_STOCK_DELETE, NULL, NULL, NULL,
        G_CALLBACK(gb_window_delete_activate) },

      { "view", NULL, N_("_View") },
      { "prev-view", NULL, N_("Previous document"), N_("<primary>Page_Up"),
        N_("Move to the previous document in the window."),
        G_CALLBACK(gb_window_prev_view_activate) },
      { "next-view", NULL, N_("Next document"), N_("<primary>Page_Down"),
        N_("Move to the next document in the window."),
        G_CALLBACK(gb_window_next_view_activate) },
      { "move-right", NULL, N_("Move to right"), "<alt><shift>Right", NULL,
        G_CALLBACK(gb_window_move_right_activate) },

      { "help", GTK_STOCK_HELP },
      { "devhelp", GTK_STOCK_HELP, N_("API References"), "<primary><shift>h", NULL,
        G_CALLBACK(gb_window_devhelp_activate) },
   };
   static const GtkToggleActionEntry toggle_actions[] = {
      { "fullscreen", GTK_STOCK_FULLSCREEN, NULL, N_("F11"), NULL,
        G_CALLBACK(gb_window_fullscreen_activate) },
      { "minimal", NULL, N_("Minimal mode"), N_("<primary><shift>m"), NULL,
        G_CALLBACK(gb_window_minimal_activate) },
      { "sidebar", NULL, N_("Show sidebar"), N_("F9"), NULL,
        G_CALLBACK(gb_window_sidebar_activate), TRUE },
      { "toolbar", NULL, N_("Show toolbar"), NULL, NULL,
        G_CALLBACK(gb_window_toolbar_activate), TRUE },
      { "dark", NULL, N_("Night mode"), "<alt><shift>m", NULL,
        G_CALLBACK(gb_window_dark_activate) },
   };

   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));

   window->priv->actions = g_object_new(GTK_TYPE_ACTION_GROUP,
                                        "name", "GbWindow::Actions",
                                        NULL);
   gtk_action_group_add_actions(window->priv->actions, actions,
                                G_N_ELEMENTS(actions), window);
   gtk_action_group_add_toggle_actions(window->priv->actions, toggle_actions,
                                       G_N_ELEMENTS(toggle_actions), window);

   EXIT;
}

static void
gb_window_init_ui_manager (GbWindow *window)
{
   GbWindowPrivate *priv;
   GtkAccelGroup *accel_group;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));

   priv = window->priv;

   priv->ui_manager = gtk_ui_manager_new();
   gtk_ui_manager_insert_action_group(priv->ui_manager, priv->actions, 0);
   if (!gtk_ui_manager_add_ui_from_string(priv->ui_manager,
                                          gb_window_ui, -1, &error)) {
      g_error("%s", error->message);
      g_error_free(error);
      EXIT;
   }

   accel_group = gtk_ui_manager_get_accel_group(priv->ui_manager);
   gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);

   priv->menubar = gtk_ui_manager_get_widget(priv->ui_manager, "/menubar");
   priv->toolbar = gtk_ui_manager_get_widget(priv->ui_manager, "/toolbar");
   priv->project_popup = gtk_ui_manager_get_widget(priv->ui_manager,
                                                   "/project-popup");

   g_assert(priv->menubar);
   g_assert(priv->toolbar);
   g_assert(priv->project_popup);

   EXIT;
}

/**
 * gb_window_get_action:
 * @window: (in): A #GbWindow.
 * @action_name: (in): A string containing the actions name.
 *
 * Gets the action associated with the window named @action_name.
 *
 * Returns: (transfer none): A #GtkAction.
 */
GtkAction *
gb_window_get_action (GbWindow    *window,
                      const gchar *action_name)
{
   g_return_val_if_fail(GB_IS_WINDOW(window), NULL);
   g_return_val_if_fail(action_name != NULL, NULL);
   return gtk_action_group_get_action(window->priv->actions, action_name);
}

void
gb_window_action_set (GbWindow    *window,
                      const gchar *action_name,
                      const gchar *first_property,
                      ...)
{
   GtkAction *action;
   va_list args;

   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));
   g_return_if_fail(action_name != NULL);
   g_return_if_fail(first_property != NULL);

   if (!(action = gb_window_get_action(window, action_name))) {
      g_warning("gb_window_action_set(): No such action %s", action_name);
      EXIT;
   }

   va_start(args, first_property);
   g_object_set_valist(G_OBJECT(action), first_property, args);
   va_end(args);

   EXIT;
}

static void
gb_window_update_actions (GbWindow *window)
{
   GbWindowPrivate *priv;
   gboolean can_save = FALSE;
   gboolean can_close = FALSE;
   gboolean can_close_all;
   GList *views;

   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));

   priv = window->priv;

   if (priv->view) {
      can_close = TRUE;
      can_save = gb_view_get_can_save(priv->view);
   }

   views = gb_view_layout_get_views(GB_VIEW_LAYOUT(priv->layout));
   can_close_all = !!views;
   g_list_free(views);

   gb_window_action_set(window, "save",
                        "sensitive", can_save,
                        NULL);
   gb_window_action_set(window, "close",
                        "sensitive", can_close,
                        NULL);
   gb_window_action_set(window, "close-all",
                        "sensitive", can_close_all,
                        NULL);

   EXIT;
}

static void
gb_window_view_notify (GbWindow   *window,
                       GParamSpec *pspec,
                       GbView     *view)
{
   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));
   g_return_if_fail(pspec != NULL);
   g_return_if_fail(GB_IS_VIEW(view));

   gb_window_update_actions(window);

   EXIT;
}

static void
gb_window_view_weak_ref_cb (GbWindow *window,
                            GbView   *where_view_was)
{
   ENTRY;
   g_return_if_fail(GB_IS_WINDOW(window));
   window->priv->view = NULL;
   window->priv->view_notify = 0;
   gb_window_update_actions(window);
   EXIT;
}

static void
gb_window_update_state_cb (PeasExtensionSet *set,
                           PeasPluginInfo   *plugin_info,
                           PeasExtension    *extension,
                           gpointer          user_data)
{
   GbWindow *window = user_data;

   ENTRY;
   g_return_if_fail(GB_IS_WINDOW_PLUGIN(extension));
   g_return_if_fail(GB_IS_WINDOW(window));
   gb_window_plugin_update_state(GB_WINDOW_PLUGIN(extension));
   EXIT;
}

static void
gb_window_view_changed (GbWindow *window,
                        GbView   *view)
{
   GbWindowPrivate *priv;

   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));
   g_return_if_fail(!view || GB_IS_VIEW(view));

   priv = window->priv;

   if (priv->view) {
      g_signal_handler_disconnect(priv->view, priv->view_notify);
      priv->view_notify = 0;
      g_object_weak_unref(G_OBJECT(priv->view),
                          (GWeakNotify)gb_window_view_weak_ref_cb,
                          window);
      priv->view = NULL;
   }

   if (view) {
      priv->view = view;
      g_object_weak_ref(G_OBJECT(priv->view),
                        (GWeakNotify)gb_window_view_weak_ref_cb,
                        window);
      priv->view_notify =
         g_signal_connect_swapped(priv->view, "notify",
                                  G_CALLBACK(gb_window_view_notify),
                                  window);
   }

   gb_window_update_actions(window);

   peas_extension_set_foreach(window->priv->plugins,
                              gb_window_update_state_cb,
                              window);

   EXIT;
}

static void
gb_window_emit_view_changed (GbWindow *window,
                             GbView   *view)
{
   ENTRY;
   g_signal_emit(window, gSignals[VIEW_CHANGED], 0, view);
   EXIT;
}

static void
gb_window_set_focus (GtkWindow *window,
                     GtkWidget *widget)
{
   GbView *view;

   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));

   GTK_WINDOW_CLASS(gb_window_parent_class)->set_focus(window, widget);

   view = gb_window_get_current_view(GB_WINDOW(window));
   gb_window_emit_view_changed(GB_WINDOW(window), view);

   EXIT;
}

guint
gb_window_add_ui (GbWindow    *window,
                  const gchar *data,
                  gssize       length)
{
   g_return_val_if_fail(GB_IS_WINDOW(window), 0);
   g_return_val_if_fail(data != NULL, 0);

   return gtk_ui_manager_add_ui_from_string(window->priv->ui_manager,
                                            data, length, NULL);
}

/**
 * gb_window_get_ui_manager:
 * @window: (in): A #GbWindow.
 *
 * Gets the #GtkUIManager for @window.
 *
 * Returns: (transfer none): A #GtkUIManager.
 */
GtkUIManager *
gb_window_get_ui_manager (GbWindow *window)
{
   g_return_val_if_fail(GB_IS_WINDOW(window), NULL);
   return window->priv->ui_manager;
}

static void
gb_window_plugin_added (GbWindow         *window,
                        PeasPluginInfo   *plugin_info,
                        PeasExtension    *extension,
                        PeasExtensionSet *extensions)
{
   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));

   if (gtk_widget_get_realized(GTK_WIDGET(window))) {
      gb_window_plugin_activate(GB_WINDOW_PLUGIN(extension));
   }

   EXIT;
}

static void
gb_window_plugin_removed (GbWindow         *window,
                          PeasPluginInfo   *plugin_info,
                          PeasExtension    *extension,
                          PeasExtensionSet *extensions)
{
   ENTRY;

   g_return_if_fail(GB_IS_WINDOW(window));

   if (gtk_widget_get_realized(GTK_WIDGET(window))) {
      gb_window_plugin_deactivate(GB_WINDOW_PLUGIN(extension));
   }

   EXIT;
}

static void
gb_window_realize_plugins_cb (PeasExtensionSet *set,
                              PeasPluginInfo   *plugin_info,
                              PeasExtension    *extension,
                              gpointer          user_data)
{
   GbWindow *window = user_data;

   ENTRY;
   g_return_if_fail(GB_IS_WINDOW_PLUGIN(extension));
   g_return_if_fail(GB_IS_WINDOW(window));
   gb_window_plugin_activate(GB_WINDOW_PLUGIN(extension));
   EXIT;
}

static void
gb_window_realize (GtkWidget *widget)
{
   GbWindow *window = (GbWindow *)widget;

   ENTRY;
   g_return_if_fail(GB_IS_WINDOW(window));
   GTK_WIDGET_CLASS(gb_window_parent_class)->realize(widget);
   peas_extension_set_foreach(window->priv->plugins,
                              gb_window_realize_plugins_cb,
                              window);
   EXIT;
}

/**
 * gb_window_get_layout:
 * @window: (in): A #GbWindow.
 *
 * Gets the current layout for the views.
 *
 * Returns: (transfer none): A #GbViewLayout.
 */
GbViewLayout *
gb_window_get_layout (GbWindow *window)
{
   g_return_val_if_fail(GB_IS_WINDOW(window), NULL);
   return GB_VIEW_LAYOUT(window->priv->layout);
}

/**
 * gb_window_finalize:
 * @object: (in): A #GbWindow.
 *
 * Finalizer for a #GbWindow instance.
 */
static void
gb_window_finalize (GObject *object)
{
   GbWindowPrivate *priv = GB_WINDOW(object)->priv;

   ENTRY;
   g_clear_object(&priv->project);
   G_OBJECT_CLASS(gb_window_parent_class)->finalize(object);
   EXIT;
}

/**
 * gb_window_get_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (out): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Get a given #GObject property.
 */
static void
gb_window_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
   GbWindow *window = GB_WINDOW(object);

   switch (prop_id) {
   case PROP_BROWSER:
      g_value_set_object(value, window->priv->browser);
      break;
   case PROP_PROJECT:
      g_value_set_object(value, gb_window_get_project(window));
      break;
   case PROP_SIDEBAR:
      g_value_set_object(value, window->priv->sidebar);
      break;
   case PROP_VIEW:
      g_value_set_object(value, window->priv->view);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * gb_window_set_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (in): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Set a given #GObject property.
 */
static void
gb_window_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
   GbWindow *window = GB_WINDOW(object);

   switch (prop_id) {
   case PROP_PROJECT:
      gb_window_set_project(window, g_value_get_object(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * gb_window_class_init:
 * @klass: (in): A #GbWindowClass.
 *
 * Initializes the #GbWindowClass and prepares the vtable.
 */
static void
gb_window_class_init (GbWindowClass *klass)
{
   GObjectClass *object_class;
   GtkWidgetClass *widget_class;
   GtkWindowClass *window_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_window_finalize;
   object_class->get_property = gb_window_get_property;
   object_class->set_property = gb_window_set_property;
   g_type_class_add_private(object_class, sizeof(GbWindowPrivate));

   widget_class = GTK_WIDGET_CLASS(klass);
   widget_class->realize = gb_window_realize; // TODO: unrealize+plugins

   window_class = GTK_WINDOW_CLASS(klass);
   window_class->set_focus = gb_window_set_focus;

   klass->view_changed = gb_window_view_changed;

   gParamSpecs[PROP_BROWSER] =
      g_param_spec_object("browser",
                          _("Browser"),
                          _("The project browser."),
                          GB_TYPE_TREE,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_BROWSER,
                                   gParamSpecs[PROP_BROWSER]);

   /**
    * GbWindow:project:
    *
    * The project property contains the project currently being edited.
    */
   gParamSpecs[PROP_PROJECT] =
      g_param_spec_object("project",
                          _("Project"),
                          _("The project for the window."),
                          GB_TYPE_PROJECT,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_PROJECT,
                                   gParamSpecs[PROP_PROJECT]);

   gParamSpecs[PROP_SIDEBAR] =
      g_param_spec_object("sidebar",
                          _("Sidebar"),
                          _("The sidebar widget."),
                          GTK_TYPE_WIDGET,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_SIDEBAR,
                                   gParamSpecs[PROP_SIDEBAR]);

   /**
    * GbWindow:view:
    *
    * The view property contains the currently focused view.
    */
   gParamSpecs[PROP_VIEW] =
      g_param_spec_object("view",
                          _("View"),
                          _("The current view."),
                          GB_TYPE_VIEW,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_VIEW,
                                   gParamSpecs[PROP_VIEW]);

   /**
    * GbWindow::view-changed:
    *
    * The "view-changed" signal is emitted when the currently focused view
    * has changed.
    */
   gSignals[VIEW_CHANGED] = g_signal_new("view-changed",
                                         GB_TYPE_WINDOW,
                                         G_SIGNAL_RUN_LAST,
                                         G_STRUCT_OFFSET(GbWindowClass, view_changed),
                                         NULL,
                                         NULL,
                                         g_cclosure_marshal_VOID__OBJECT,
                                         G_TYPE_NONE,
                                         1,
                                         GB_TYPE_VIEW);
}

/**
 * gb_window_init:
 * @window: (in): A #GbWindow.
 *
 * Initializes the newly created #GbWindow instance.
 */
static void
gb_window_init (GbWindow *window)
{
   GbWindowPrivate *priv;
   GtkWidget *b;
   GtkWidget *bbox;
   GtkWidget *img;
   GtkWidget *scroller;
   GtkWidget *vbox;
   GtkWidget *vbox2;

   ENTRY;

   window->priv = priv =
      G_TYPE_INSTANCE_GET_PRIVATE(window,
                                  GB_TYPE_WINDOW,
                                  GbWindowPrivate);

   gb_window_init_actions(window);
   gb_window_init_ui_manager(window);

   vbox = g_object_new(GTK_TYPE_VBOX,
                       "visible", TRUE,
                       NULL);
   gtk_container_add(GTK_CONTAINER(window), vbox);

   priv->mb_tb_anim = g_object_new(GB_TYPE_ANIM_BIN,
                                   "visible", TRUE,
                                   NULL);
   gtk_container_add_with_properties(GTK_CONTAINER(vbox), priv->mb_tb_anim,
                                     "expand", FALSE,
                                     NULL);

   vbox2 = g_object_new(GTK_TYPE_VBOX,
                        "visible", TRUE,
                        NULL);
   gtk_container_add(GTK_CONTAINER(priv->mb_tb_anim), vbox2);

   gtk_container_add_with_properties(GTK_CONTAINER(vbox2), priv->menubar,
                                     "expand", FALSE,
                                     NULL);
   gtk_style_context_add_class(gtk_widget_get_style_context(priv->toolbar),
                               GTK_STYLE_CLASS_PRIMARY_TOOLBAR);
   gtk_container_add_with_properties(GTK_CONTAINER(vbox2), priv->toolbar,
                                     "expand", FALSE,
                                     NULL);

   priv->layout = g_object_new(GB_TYPE_VIEW_LAYOUT_EDIT,
                               "visible", TRUE,
                               NULL);
   gtk_container_add(GTK_CONTAINER(vbox), priv->layout);

   priv->sidebar = g_object_new(GTK_TYPE_VBOX,
                                "expand", FALSE,
                                "name", "sidebar",
                                "visible", TRUE,
                                NULL);
   gtk_container_add_with_properties(GTK_CONTAINER(priv->layout), priv->sidebar,
                                     "resize", FALSE,
                                     "shrink", FALSE,
                                     NULL);

   bbox = g_object_new(GTK_TYPE_HBOX,
                       "height-request", 30,
                       "visible", TRUE,
                       NULL);
   gtk_container_add_with_properties(GTK_CONTAINER(priv->sidebar), bbox,
                                     "expand", FALSE,
                                     NULL);

   priv->project_button = g_object_new(GB_TYPE_MENU_BUTTON,
                                       "label", NULL,
                                       "menu", priv->project_popup,
                                       "name", "gb-project-button",
                                       "visible", TRUE,
                                       NULL);
   gtk_style_context_add_class(gtk_widget_get_style_context(priv->project_button),
                               GB_STYLE_CLASS_TOP_BAR);
   gtk_container_add(GTK_CONTAINER(bbox), priv->project_button);

   img = g_object_new(GTK_TYPE_IMAGE,
                      "icon-name", "gb-build",
                      "icon-size", GTK_ICON_SIZE_MENU,
                      "visible", TRUE,
                      NULL);
   b = g_object_new(GTK_TYPE_BUTTON,
                    "child", img,
                    "visible", TRUE,
                    NULL);
   gtk_style_context_add_class(gtk_widget_get_style_context(b),
                               GB_STYLE_CLASS_TOP_BAR);
   gtk_container_add_with_properties(GTK_CONTAINER(bbox), b,
                                     "expand", FALSE,
                                     NULL);
   g_signal_connect_swapped(b, "clicked",
                            G_CALLBACK(gb_window_build_clicked),
                            window);

   img = g_object_new(GTK_TYPE_IMAGE,
                      "icon-name", "gb-run",
                      "icon-size", GTK_ICON_SIZE_MENU,
                      "visible", TRUE,
                      NULL);
   b = g_object_new(GTK_TYPE_BUTTON,
                    "child", img,
                    "visible", TRUE,
                    NULL);
   gtk_style_context_add_class(gtk_widget_get_style_context(b),
                               GB_STYLE_CLASS_TOP_BAR);
   gtk_container_add_with_properties(GTK_CONTAINER(bbox), b,
                                     "expand", FALSE,
                                     NULL);

   priv->progress = g_object_new(GB_TYPE_PROGRESS_BAR,
                                 "visible", TRUE,
                                 "fraction", 0.0,
                                 NULL);
   gtk_container_add_with_properties(GTK_CONTAINER(priv->sidebar), priv->progress,
                                     "expand", FALSE,
                                     NULL);

   scroller = g_object_new(gb_get_scrolled_window_gtype(),
                           "visible", TRUE,
                           "width-request", 225,
                           NULL);
   gtk_container_add(GTK_CONTAINER(priv->sidebar), scroller);

   priv->browser = g_object_new(GB_TYPE_TREE,
                                "root", NULL,
                                "visible", TRUE,
                                NULL);
   gb_tree_add_builder(GB_TREE(priv->browser),
                       gb_tree_builder_project_new());
   gtk_container_add(GTK_CONTAINER(scroller), priv->browser);

   priv->plugins = peas_extension_set_new(peas_engine_get_default(),
                                          GB_TYPE_WINDOW_PLUGIN,
                                          "window", window,
                                          NULL);
   g_signal_connect_swapped(priv->plugins, "extension-added",
                            G_CALLBACK(gb_window_plugin_added),
                            window);
   g_signal_connect_swapped(priv->plugins, "extension-removed",
                            G_CALLBACK(gb_window_plugin_removed),
                            window);

   EXIT;
}
