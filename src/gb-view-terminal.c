/* gb-view-terminal.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <vte/vte.h>

#include "gb-log.h"
#include "gb-scrolled-window.h"
#include "gb-view-terminal.h"

G_DEFINE_TYPE(GbViewTerminal, gb_view_terminal, GB_TYPE_VIEW)

struct _GbViewTerminalPrivate
{
   GtkWidget *scroller;
   GtkWidget *vte;
};

enum
{
   PROP_0,
   LAST_PROP
};

//static GParamSpec *gParamSpecs[LAST_PROP];

static void
gb_view_terminal_window_title_changed (GbViewTerminal *terminal,
                                       VteTerminal    *vte)
{
   const gchar *title;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW_TERMINAL(terminal));
   g_return_if_fail(VTE_IS_TERMINAL(vte));

   title = vte_terminal_get_window_title(vte);
   gb_view_set_name(GB_VIEW(terminal),
                    title ? title : _("Terminal"));

   EXIT;
}

static void
gb_view_terminal_grab_focus (GtkWidget *widget)
{
   GbViewTerminal *terminal = (GbViewTerminal *)widget;
   g_return_if_fail(GB_IS_VIEW_TERMINAL(terminal));
   gtk_widget_grab_focus(terminal->priv->vte);
}


/**
 * gb_terminal_view_key_press:
 * @terminal: (in): A #GbViewTerminal.
 * @key: (in): A #GdkEventKey.
 * @vte: (in): A #VteTerminal.
 *
 * Handle the "key-press-event" for the VteTerminal. Key combinations like
 * <Control><Shift>C are handled for features such as copy/paste.
 *
 * Returns: %TRUE if the event was handled.
 */
static gboolean
gb_view_terminal_key_press (GbViewTerminal *terminal,
                            GdkEventKey    *key,
                            VteTerminal    *vte)
{
   GdkModifierType mask;

   g_return_val_if_fail(key != NULL, FALSE);

   switch (key->keyval) {
   case GDK_KEY_C:
      mask = GDK_SHIFT_MASK | GDK_CONTROL_MASK;
      if ((key->state & mask) == mask) {
         vte_terminal_copy_clipboard(vte);
         return TRUE;
      }
      break;
   case GDK_KEY_V:
      mask = GDK_SHIFT_MASK | GDK_CONTROL_MASK;
      if ((key->state & mask) == mask) {
         vte_terminal_paste_clipboard(vte);
         return TRUE;
      }
   default:
      break;
   }

   return FALSE;
}

static void
gb_view_terminal_child_exited (GbViewTerminal *terminal,
                               VteTerminal    *vte)
{
   ENTRY;

   g_return_if_fail(GB_IS_VIEW_TERMINAL(terminal));
   g_return_if_fail(VTE_IS_TERMINAL(vte));

   gb_view_emit_closed(GB_VIEW(terminal));

   EXIT;
}

static void
gb_view_terminal_finalize (GObject *object)
{
   G_OBJECT_CLASS(gb_view_terminal_parent_class)->finalize(object);
}

static void
gb_view_terminal_class_init (GbViewTerminalClass *klass)
{
   GObjectClass *object_class;
   GtkWidgetClass *widget_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_view_terminal_finalize;
   g_type_class_add_private(object_class, sizeof(GbViewTerminalPrivate));

   widget_class = GTK_WIDGET_CLASS(klass);
   widget_class->grab_focus = gb_view_terminal_grab_focus;
}

static void
gb_view_terminal_init (GbViewTerminal *terminal)
{
   GbViewTerminalPrivate *priv;
   static const gchar *argv[] = { "/bin/bash", NULL };

   terminal->priv = priv =
      G_TYPE_INSTANCE_GET_PRIVATE(terminal,
                                  GB_TYPE_VIEW_TERMINAL,
                                  GbViewTerminalPrivate);

   g_object_set(terminal,
                "icon-name", "terminal",
                "name", _("Terminal"),
                NULL);

   priv->scroller = g_object_new(GTK_TYPE_SCROLLED_WINDOW,
                                 "visible", TRUE,
                                 NULL);
   gtk_container_add_with_properties(GTK_CONTAINER(terminal), priv->scroller,
                                     "left-attach", 0,
                                     "top-attach", 0,
                                     "width", 1,
                                     "height", 1,
                                     NULL);

   priv->vte = g_object_new(VTE_TYPE_TERMINAL,
                            "hexpand", TRUE,
                            "vexpand", TRUE,
                            "visible", TRUE,
                            NULL);
   g_signal_connect_swapped(priv->vte, "child-exited",
                            G_CALLBACK(gb_view_terminal_child_exited),
                            terminal);
   g_signal_connect_swapped(priv->vte, "key-press-event",
                            G_CALLBACK(gb_view_terminal_key_press),
                            terminal);
   g_signal_connect_swapped(priv->vte, "window-title-changed",
                            G_CALLBACK(gb_view_terminal_window_title_changed),
                            terminal);
   gtk_container_add(GTK_CONTAINER(priv->scroller), priv->vte);

   {
      PangoFontDescription *fd;

      /*
       * TODO: Global font settings.
       */

      fd = pango_font_description_new();
      pango_font_description_set_size(fd, PANGO_SCALE * 9);
      vte_terminal_set_font(VTE_TERMINAL(priv->vte), fd);
      pango_font_description_free(fd);
   }

   vte_terminal_fork_command_full(VTE_TERMINAL(priv->vte),
                                  VTE_PTY_DEFAULT,
                                  ".",
                                  (gchar **)argv,
                                  NULL,
                                  0,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL);

   gb_view_terminal_window_title_changed(terminal, VTE_TERMINAL(priv->vte));
}
