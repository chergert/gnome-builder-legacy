/* gb-view-docs.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_VIEW_DOCS_H
#define GB_VIEW_DOCS_H

#include "gb-view.h"

G_BEGIN_DECLS

#define GB_TYPE_VIEW_DOCS            (gb_view_docs_get_type())
#define GB_VIEW_DOCS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_VIEW_DOCS, GbViewDocs))
#define GB_VIEW_DOCS_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_VIEW_DOCS, GbViewDocs const))
#define GB_VIEW_DOCS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_VIEW_DOCS, GbViewDocsClass))
#define GB_IS_VIEW_DOCS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_VIEW_DOCS))
#define GB_IS_VIEW_DOCS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_VIEW_DOCS))
#define GB_VIEW_DOCS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_VIEW_DOCS, GbViewDocsClass))

typedef struct _GbViewDocs        GbViewDocs;
typedef struct _GbViewDocsClass   GbViewDocsClass;
typedef struct _GbViewDocsPrivate GbViewDocsPrivate;

struct _GbViewDocs
{
   GbView parent;

   /*< private >*/
   GbViewDocsPrivate *priv;
};

struct _GbViewDocsClass
{
   GbViewClass parent_class;
};

GType gb_view_docs_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* GB_VIEW_DOCS_H */
