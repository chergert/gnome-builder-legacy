/* gb-menu-button.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_MENU_BUTTON_H
#define GB_MENU_BUTTON_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GB_TYPE_MENU_BUTTON            (gb_menu_button_get_type())
#define GB_MENU_BUTTON(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_MENU_BUTTON, GbMenuButton))
#define GB_MENU_BUTTON_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_MENU_BUTTON, GbMenuButton const))
#define GB_MENU_BUTTON_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_MENU_BUTTON, GbMenuButtonClass))
#define GB_IS_MENU_BUTTON(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_MENU_BUTTON))
#define GB_IS_MENU_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_MENU_BUTTON))
#define GB_MENU_BUTTON_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_MENU_BUTTON, GbMenuButtonClass))

typedef struct _GbMenuButton        GbMenuButton;
typedef struct _GbMenuButtonClass   GbMenuButtonClass;
typedef struct _GbMenuButtonPrivate GbMenuButtonPrivate;

struct _GbMenuButton
{
   GtkButton parent;

   /*< private >*/
   GbMenuButtonPrivate *priv;
};

struct _GbMenuButtonClass
{
   GtkButtonClass parent_class;
};

const gchar *gb_menu_button_get_label        (GbMenuButton *button);
GtkWidget   *gb_menu_button_get_menu         (GbMenuButton *button);
GtkWidget   *gb_menu_button_get_label_widget (GbMenuButton *button);
GType        gb_menu_button_get_type         (void) G_GNUC_CONST;
GtkWidget   *gb_menu_button_new              (const gchar  *label,
                                              GtkMenu      *menu);
void         gb_menu_button_set_label        (GbMenuButton *button,
                                              const gchar  *label);
void         gb_menu_button_set_menu         (GbMenuButton *button,
                                              GtkMenu      *menu);

G_END_DECLS

#endif /* GB_MENU_BUTTON_H */
