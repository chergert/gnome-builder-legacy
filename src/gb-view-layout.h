/* gb-view-layout.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_VIEW_LAYOUT_H
#define GB_VIEW_LAYOUT_H

#include <glib-object.h>

#include "gb-view.h"

G_BEGIN_DECLS

#define GB_TYPE_VIEW_LAYOUT             (gb_view_layout_get_type())
#define GB_VIEW_LAYOUT(o)               (G_TYPE_CHECK_INSTANCE_CAST((o),    GB_TYPE_VIEW_LAYOUT, GbViewLayout))
#define GB_IS_VIEW_LAYOUT(o)            (G_TYPE_CHECK_INSTANCE_TYPE((o),    GB_TYPE_VIEW_LAYOUT))
#define GB_VIEW_LAYOUT_GET_INTERFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE((o), GB_TYPE_VIEW_LAYOUT, GbViewLayoutIface))

typedef struct _GbViewLayout      GbViewLayout;
typedef struct _GbViewLayoutIface GbViewLayoutIface;

struct _GbViewLayoutIface
{
   GTypeInterface parent;

   GList *(*get_views)     (GbViewLayout *layout);
   void   (*move_right)    (GbViewLayout *layout,
                            GbView       *view);
   void   (*next_view)     (GbViewLayout *layout,
                            GbView       *view);
   void   (*previous_view) (GbViewLayout *layout,
                            GbView       *view);
};

GType  gb_view_layout_get_type      (void) G_GNUC_CONST;
GList *gb_view_layout_get_views     (GbViewLayout *layout);
void   gb_view_layout_move_right    (GbViewLayout *layout,
                                     GbView       *view);
void   gb_view_layout_next_view     (GbViewLayout *layout,
                                     GbView       *view);
void   gb_view_layout_previous_view (GbViewLayout *layout,
                                     GbView       *view);

G_END_DECLS

#endif /* GB_VIEW_LAYOUT_H */
