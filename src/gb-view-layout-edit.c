/* gb-view-layout-edit.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-log.h"
#include "gb-view.h"
#include "gb-view-layout.h"
#include "gb-view-layout-edit.h"
#include "gb-view-stack.h"

struct _GbViewLayoutEditPrivate
{
   GtkWidget *top_hpaned;
};

static void gb_view_layout_init (GbViewLayoutIface *iface);

G_DEFINE_TYPE_EXTENDED(GbViewLayoutEdit,
                       gb_view_layout_edit,
                       GTK_TYPE_ALIGNMENT,
                       0,
                       G_IMPLEMENT_INTERFACE(GB_TYPE_VIEW_LAYOUT,
                                             gb_view_layout_init))

static GtkWidget *gb_view_layout_edit_get_first_stack (GbViewLayoutEdit*);

static void
gb_view_layout_edit_remove_empty (GbViewLayoutEdit *edit)
{
   GbViewLayoutEditPrivate *priv;
   GtkWidget *paned;
   GtkWidget *stack;
   GtkWidget *parent;
   GtkWidget *child;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit));

   priv = edit->priv;

   paned = gtk_paned_get_child2(GTK_PANED(priv->top_hpaned));
   g_assert(GTK_IS_PANED(paned));

   while (paned) {
      stack = gtk_paned_get_child1(GTK_PANED(paned));
      g_assert(GB_IS_VIEW_STACK(stack));

      if (!gb_view_stack_get_n_views(GB_VIEW_STACK(stack))) {
         child = gtk_paned_get_child2(GTK_PANED(paned));
         g_object_ref(child);
         parent = gtk_widget_get_parent(paned);
         gtk_container_remove(GTK_CONTAINER(paned), child);
         gtk_container_remove(GTK_CONTAINER(parent), paned);
         gtk_paned_add2(GTK_PANED(parent), child);
         g_object_unref(child);
         paned = parent;
      }

      paned = gtk_paned_get_child2(GTK_PANED(paned));
   }

   /*
    * If everything got removed, re-add a default stack.
    */
   if (!gtk_paned_get_child2(GTK_PANED(priv->top_hpaned))) {
      gb_view_layout_edit_get_first_stack(edit);
   }

   EXIT;
}

static GtkWidget *
gb_view_layout_edit_get_first_stack (GbViewLayoutEdit *edit)
{
   GbViewLayoutEditPrivate *priv;
   GtkWidget *child;
   GtkWidget *paned;

   ENTRY;

   g_return_val_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit), NULL);

   priv = edit->priv;

   if (!(paned = gtk_paned_get_child2(GTK_PANED(priv->top_hpaned)))) {
      paned = g_object_new(GTK_TYPE_HPANED,
                           "visible", TRUE,
                           NULL);
      gtk_paned_add2(GTK_PANED(priv->top_hpaned), paned);
      gtk_container_child_set(GTK_CONTAINER(priv->top_hpaned), paned,
                              "resize", TRUE,
                              "shrink", FALSE,
                              NULL);
      child = g_object_new(GB_TYPE_VIEW_STACK,
                           "visible", TRUE,
                           NULL);
      g_signal_connect_swapped(child, "changed",
                               G_CALLBACK(gb_view_layout_edit_remove_empty),
                               edit);
      gtk_paned_add1(GTK_PANED(paned), child);
   }

   child = gtk_paned_get_child1(GTK_PANED(paned));

   RETURN(child);
}

static void
gb_view_layout_edit_add (GtkContainer *container,
                         GtkWidget    *child)
{
   GbViewLayoutEditPrivate *priv;
   GbViewLayoutEdit *edit = (GbViewLayoutEdit *)container;
   GtkWidget *stack = NULL;
   GtkWidget *toplevel;

   g_return_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit));
   g_return_if_fail(GTK_IS_WIDGET(child));

   priv = edit->priv;

   if (GB_IS_VIEW(child)) {
      /*
       * Try to find the currently focused view.
       */
      toplevel = gtk_widget_get_toplevel(GTK_WIDGET(edit));
      if (toplevel && GTK_IS_WINDOW(toplevel)) {
         if ((stack = gtk_window_get_focus(GTK_WINDOW(toplevel)))) {
            while (stack && !GB_IS_VIEW_STACK(stack)) {
               stack = gtk_widget_get_parent(stack);
            }
         }
      }

      if (!stack) {
         stack = gb_view_layout_edit_get_first_stack(edit);
      }

      gtk_container_add(GTK_CONTAINER(stack), child);
   } else {
      gtk_paned_add1(GTK_PANED(priv->top_hpaned), child);
   }
}

static GList *
gb_view_layout_edit_get_stacks (GbViewLayoutEdit *edit)
{
   GbViewLayoutEditPrivate *priv;
   GtkWidget *child;
   GtkWidget *paned;
   GList *list = NULL;

   ENTRY;

   g_return_val_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit), NULL);

   priv = edit->priv;

   paned = priv->top_hpaned;

   for (; paned; paned = gtk_paned_get_child2(GTK_PANED(paned))) {
      child = gtk_paned_get_child1(GTK_PANED(paned));
      if (GB_IS_VIEW_STACK(child)) {
         list = g_list_append(list, child);
      }
   }

   RETURN(list);
}

static GList *
gb_view_layout_edit_get_views (GbViewLayout *layout)
{
   GbViewLayoutEdit *edit = (GbViewLayoutEdit *)layout;
   GList *stacks;
   GList *iter;
   GList *ret = NULL;

   ENTRY;

   g_return_val_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit), NULL);

   stacks = gb_view_layout_edit_get_stacks(edit);
   for (iter = stacks; iter; iter = iter->next) {
      ret = g_list_concat(ret, gb_view_stack_get_views(iter->data));
   }
   g_list_free(stacks);

   RETURN(ret);
}

static void
gb_view_layout_edit_realign (GbViewLayoutEdit *edit)
{
   GbViewLayoutEditPrivate *priv;
   GtkAllocation alloc;
   GtkWidget *paned;
   guint n_paneds = 0;
   guint width;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit));

   priv = edit->priv;

   paned = gtk_paned_get_child2(GTK_PANED(priv->top_hpaned));
   do {
      n_paneds++;
   } while ((paned = gtk_paned_get_child2(GTK_PANED(paned))));
   g_assert_cmpint(n_paneds, >, 0);

   paned = gtk_paned_get_child2(GTK_PANED(priv->top_hpaned));
   gtk_widget_get_allocation(paned, &alloc);
   width = alloc.width / n_paneds;

   do {
      gtk_paned_set_position(GTK_PANED(paned), width);
   } while ((paned = gtk_paned_get_child2(GTK_PANED(paned))));

   EXIT;
}

static GbViewStack *
gb_view_layout_edit_add_stack (GbViewLayoutEdit *edit)
{
   GbViewLayoutEditPrivate *priv;
   GtkWidget *stack_paned;
   GtkWidget *stack = NULL;
   GtkWidget *paned;

   ENTRY;

   g_return_val_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit), NULL);

   priv = edit->priv;

   stack = g_object_new(GB_TYPE_VIEW_STACK,
                        "visible", TRUE,
                        NULL);
   g_signal_connect_swapped(stack, "changed",
                            G_CALLBACK(gb_view_layout_edit_remove_empty),
                            edit);

   paned = priv->top_hpaned;
   while (gtk_paned_get_child2(GTK_PANED(paned))) {
      paned = gtk_paned_get_child2(GTK_PANED(paned));
   }

   stack_paned = g_object_new(GTK_TYPE_HPANED,
                              "visible", TRUE,
                              NULL);
   gtk_paned_add1(GTK_PANED(stack_paned), stack);
   gtk_container_child_set(GTK_CONTAINER(stack_paned), stack,
                           "resize", TRUE,
                           "shrink", FALSE,
                           NULL);

   gtk_paned_add2(GTK_PANED(paned), stack_paned);
   gtk_container_child_set(GTK_CONTAINER(paned), stack_paned,
                           "resize", TRUE,
                           "shrink", FALSE,
                           NULL);

   gb_view_layout_edit_realign(edit);

   RETURN(GB_VIEW_STACK(stack));
}

static void
gb_view_layout_edit_move_right (GbViewLayout *layout,
                                GbView       *view)
{
   GbViewLayoutEdit *edit = (GbViewLayoutEdit *)layout;
   GbViewStack *stack;
   GList *iter;
   GList *stacks;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit));
   g_return_if_fail(GB_IS_VIEW(view));

   stacks = gb_view_layout_edit_get_stacks(edit);

   for (iter = stacks; iter; iter = iter->next) {
      if (gb_view_stack_contains_view(iter->data, view)) {
         g_object_ref(view);
         gb_view_stack_remove_view(iter->data, view);
         if (!iter->next) {
            stack = gb_view_layout_edit_add_stack(edit);
         } else {
            stack = iter->next->data;
         }
         //gb_view_stack_add_view(stack, view);
         gtk_container_add(GTK_CONTAINER(stack), GTK_WIDGET(view));
         g_object_unref(view);
         break;
      }
   }

   g_list_free(stacks);

   gb_view_layout_edit_remove_empty(edit);

   EXIT;
}

static void
gb_view_layout_edit_next_view (GbViewLayout *layout,
                               GbView       *view)
{
   GbViewLayoutEdit *edit = (GbViewLayoutEdit *)layout;
   GList *iter;
   GList *stacks;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit));
   g_return_if_fail(GB_IS_VIEW(view));

   stacks = gb_view_layout_edit_get_stacks(edit);
   for (iter = stacks; iter; iter = iter->next) {
      if (gb_view_stack_contains_view(iter->data, view)) {
         if (!gb_view_stack_focus_next(iter->data)) {
            if (iter->next) {
               gb_view_stack_focus_first(iter->next->data);
            }
         }
         break;
      }
   }
   g_list_free(stacks);

   EXIT;
}

static void
gb_view_layout_edit_previous_view (GbViewLayout *layout,
                                   GbView       *view)
{
   GbViewLayoutEdit *edit = (GbViewLayoutEdit *)layout;
   GList *iter;
   GList *stacks;

   ENTRY;

   g_return_if_fail(GB_IS_VIEW_LAYOUT_EDIT(edit));
   g_return_if_fail(GB_IS_VIEW(view));

   stacks = gb_view_layout_edit_get_stacks(edit);
   stacks = g_list_reverse(stacks);
   for (iter = stacks; iter; iter = iter->next) {
      if (gb_view_stack_contains_view(iter->data, view)) {
         gb_view_stack_focus_previous(iter->data);
         break;
      }
   }
   g_list_free(stacks);

   EXIT;
}

/**
 * gb_view_layout_edit_finalize:
 * @object: (in): A #GbViewLayoutEdit.
 *
 * Finalizer for a #GbViewLayoutEdit instance.
 */
static void
gb_view_layout_edit_finalize (GObject *object)
{
   ENTRY;
   G_OBJECT_CLASS(gb_view_layout_edit_parent_class)->finalize(object);
   EXIT;
}

/**
 * gb_view_layout_edit_class_init:
 * @klass: (in): A #GbViewLayoutEditClass.
 *
 * Initializes the #GbViewLayoutEditClass and prepares the vtable.
 */
static void
gb_view_layout_edit_class_init (GbViewLayoutEditClass *klass)
{
   GObjectClass *object_class;
   GtkContainerClass *container_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_view_layout_edit_finalize;
   g_type_class_add_private(object_class, sizeof(GbViewLayoutEditPrivate));

   container_class = GTK_CONTAINER_CLASS(klass);
   container_class->add = gb_view_layout_edit_add;
}

/**
 * gb_view_layout_edit_init:
 * @edit: (in): A #GbViewLayoutEdit.
 *
 * Initializes the newly created #GbViewLayoutEdit instance.
 */
static void
gb_view_layout_edit_init (GbViewLayoutEdit *edit)
{
   GtkWidget *paned;
   GtkWidget *stack;

   edit->priv = G_TYPE_INSTANCE_GET_PRIVATE(edit,
                                            GB_TYPE_VIEW_LAYOUT_EDIT,
                                            GbViewLayoutEditPrivate);

   g_object_set(edit, "position", 200, NULL);

   edit->priv->top_hpaned = g_object_new(GTK_TYPE_HPANED,
                                         "visible", TRUE,
                                         NULL);
   GTK_CONTAINER_CLASS(gb_view_layout_edit_parent_class)->
      add(GTK_CONTAINER(edit), edit->priv->top_hpaned);

   paned = g_object_new(GTK_TYPE_HPANED,
                        "visible", TRUE,
                        NULL);
   gtk_paned_add2(GTK_PANED(edit->priv->top_hpaned), paned);

   stack = g_object_new(GB_TYPE_VIEW_STACK,
                        "visible", TRUE,
                        NULL);
   g_signal_connect_swapped(stack, "changed",
                            G_CALLBACK(gb_view_layout_edit_remove_empty),
                            edit);
   gtk_paned_add1(GTK_PANED(paned), stack);
   gtk_container_child_set(GTK_CONTAINER(paned), stack,
                           "resize", TRUE,
                           "shrink", FALSE,
                           NULL);
}

static void
gb_view_layout_init (GbViewLayoutIface *iface)
{
   iface->get_views = gb_view_layout_edit_get_views;
   iface->move_right = gb_view_layout_edit_move_right;
   iface->next_view = gb_view_layout_edit_next_view;
   iface->previous_view = gb_view_layout_edit_previous_view;
}
