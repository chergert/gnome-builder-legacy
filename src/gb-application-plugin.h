/* gb-application-plugin.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_APPLICATION_PLUGIN_H
#define GB_APPLICATION_PLUGIN_H

#include <glib-object.h>

G_BEGIN_DECLS

#define GB_TYPE_APPLICATION_PLUGIN             (gb_application_plugin_get_type())
#define GB_APPLICATION_PLUGIN(o)               (G_TYPE_CHECK_INSTANCE_CAST((o),    GB_TYPE_APPLICATION_PLUGIN, GbApplicationPlugin))
#define GB_IS_APPLICATION_PLUGIN(o)            (G_TYPE_CHECK_INSTANCE_TYPE((o),    GB_TYPE_APPLICATION_PLUGIN))
#define GB_APPLICATION_PLUGIN_GET_INTERFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE((o), GB_TYPE_APPLICATION_PLUGIN, GbApplicationPluginIface))

typedef struct _GbApplicationPlugin      GbApplicationPlugin;
typedef struct _GbApplicationPluginIface GbApplicationPluginIface;

struct _GbApplicationPluginIface
{
   GTypeInterface parent;

   void (*activate)   (GbApplicationPlugin *plugin);
   void (*deactivate) (GbApplicationPlugin *plugin);
};

GType gb_application_plugin_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* GB_APPLICATION_PLUGIN_H */
