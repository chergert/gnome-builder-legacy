/* gb-view-docs.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <devhelp/dh-assistant-view.h>
#include <devhelp/dh-book-manager.h>
#include <devhelp/dh-search.h>
#include <glib/gi18n.h>
#include <webkit/webkit.h>

#include "gb-scrolled-window.h"
#include "gb-util.h"
#include "gb-view-docs.h"

G_DEFINE_TYPE(GbViewDocs, gb_view_docs, GB_TYPE_VIEW)

struct _GbViewDocsPrivate
{
   GtkWidget *dh_search;
   GtkWidget *entry;
   GtkWidget *webview;
};

enum
{
   PROP_0,
   LAST_PROP
};

//static GParamSpec *gParamSpecs[LAST_PROP];

static void
gb_view_docs_link_selected (GbViewDocs *docs,
                            DhLink     *link_,
                            GtkWidget  *widget)
{
   GbViewDocsPrivate *priv;

   g_return_if_fail(GB_IS_VIEW_DOCS(docs));
   g_return_if_fail(link_ != NULL);
   g_return_if_fail(GTK_IS_WIDGET(widget));

   priv = docs->priv;

   webkit_web_view_open(WEBKIT_WEB_VIEW(priv->webview),
                        dh_link_get_uri(link_));
}

static void
entry_changed (GtkWidget *entry,
               GtkWidget *window)
{
   GtkAllocation alloc;
   gint x;
   gint y;

   g_return_if_fail(GTK_IS_WIDGET(entry));
   g_return_if_fail(GTK_IS_WINDOW(window));

   gtk_widget_get_allocation(entry, &alloc);

   gdk_window_get_root_coords(gtk_widget_get_window(entry),
                              alloc.x, alloc.y,
                              &x, &y);

   gtk_window_resize(GTK_WINDOW(window), alloc.width, 200);
   gtk_window_move(GTK_WINDOW(window), x, y - 200);
   gtk_window_present(GTK_WINDOW(window));
}

static gboolean
entry_key_press (GtkWidget   *entry,
                 GdkEventKey *key,
                 GtkWidget   *window)
{
   GtkWidget *widget;

   if (key->keyval == GDK_KEY_Escape) {
      gtk_widget_hide(window);
      widget = entry;
      while (widget && !GB_IS_VIEW(widget)) {
         widget = gtk_widget_get_parent(widget);
      }
      if (GB_IS_VIEW_DOCS(widget)) {
         gtk_widget_grab_focus(GB_VIEW_DOCS(widget)->priv->webview);
      }
   }
   return FALSE;
}

static void
gb_view_docs_grab_focus (GtkWidget *widget)
{
   GbViewDocs *docs = (GbViewDocs *)widget;
   g_return_if_fail(GB_IS_VIEW_DOCS(docs));
   gtk_widget_grab_focus(docs->priv->entry);
}

static void
gb_view_docs_finalize (GObject *object)
{
   G_OBJECT_CLASS(gb_view_docs_parent_class)->finalize(object);
}

static void
gb_view_docs_class_init (GbViewDocsClass *klass)
{
   GObjectClass *object_class;
   GtkWidgetClass *widget_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_view_docs_finalize;
   g_type_class_add_private(object_class, sizeof(GbViewDocsPrivate));

   widget_class = GTK_WIDGET_CLASS(klass);
   widget_class->grab_focus = gb_view_docs_grab_focus;
}

static void
gb_view_docs_init (GbViewDocs *docs)
{
   GbViewDocsPrivate *priv;
   DhBookManager *mgr;
   GtkWidget *scroller;

   docs->priv = priv =
      G_TYPE_INSTANCE_GET_PRIVATE(docs,
                                  GB_TYPE_VIEW_DOCS,
                                  GbViewDocsPrivate);

   g_object_set(docs,
                "icon-name", GTK_STOCK_HELP,
                "name", _("Documentation"),
                NULL);

   mgr = dh_book_manager_new();
   dh_book_manager_populate(mgr);
   priv->dh_search = dh_search_new(mgr);
   g_signal_connect_swapped(priv->dh_search, "link-selected",
                            G_CALLBACK(gb_view_docs_link_selected),
                            docs);
   gtk_widget_set_vexpand(GTK_WIDGET(priv->dh_search), TRUE);
   gtk_container_add_with_properties(GTK_CONTAINER(docs), priv->dh_search,
                                     "left-attach", 0,
                                     "top-attach", 1,
                                     "width", 1,
                                     "height", 1,
                                     NULL);
   gtk_widget_hide(priv->dh_search);
   g_object_unref(mgr);

   {
      /*
       * XXX:
       *
       * In the following code, we rip devhelps shit part. This could
       * very well likely fuck us at some point. But it works for now!
       */

      GList *boxchildren;
      GList *children;
      GList *cells;
      GtkWidget *entry;
      GtkWidget *box;
      GtkWidget *combo;
      GtkWidget *win;
      GtkWidget *vp;

      children = gtk_container_get_children(GTK_CONTAINER(priv->dh_search));

      box = g_list_nth_data(children, 0);
      boxchildren = gtk_container_get_children(GTK_CONTAINER(box));
      combo = g_list_nth_data(boxchildren, 1);
      gb_widget_shrink_font(combo);
      cells = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT(combo));
      g_object_set(cells->data, "size-points", 9.0, NULL);
      g_list_free(cells);
      gb_widget_add_style_class(combo, GB_STYLE_CLASS_BOTTOM_BAR);
      gtk_widget_set_name(combo, "gb-view-search-combo");
      g_object_ref(combo);
      gtk_container_remove(GTK_CONTAINER(box), combo);
      gtk_container_add_with_properties(GTK_CONTAINER(docs), combo,
                                        "left-attach", 1,
                                        "top-attach", 3,
                                        "width", 1,
                                        "height", 1,
                                        NULL);
      g_list_free(boxchildren);
      g_object_unref(combo);

      entry = g_list_nth_data(children, 1);
      gb_widget_shrink_font(entry);
      g_object_ref(entry);
      gtk_container_remove(GTK_CONTAINER(priv->dh_search), entry);
      vp = g_object_new(GTK_TYPE_VIEWPORT,
                        "visible", TRUE,
                        "height-request", 30,
                        NULL);
      gb_widget_add_style_class(vp, GB_STYLE_CLASS_BOTTOM_BAR);
      gtk_container_add(GTK_CONTAINER(vp), entry);
      gtk_container_add_with_properties(GTK_CONTAINER(docs), vp,
                                        "left-attach", 0,
                                        "top-attach", 3,
                                        "width", 1,
                                        "height", 1,
                                        NULL);
      g_object_set(entry,
                   "primary-icon-name", GTK_STOCK_FIND,
                   "has-frame", FALSE,
                   "hexpand", TRUE,
                   "vexpand", FALSE,
                   NULL);
      priv->entry = entry;
      g_object_unref(entry);

      scroller = g_list_nth_data(children, 2);
      gb_widget_shrink_font(gtk_bin_get_child(GTK_BIN(scroller)));
      g_object_ref(scroller);
      gtk_container_remove(GTK_CONTAINER(priv->dh_search), scroller);
      win = g_object_new(GTK_TYPE_WINDOW,
                         "child", scroller,
                         "type", GTK_WINDOW_POPUP,
                         "type-hint", GDK_WINDOW_TYPE_HINT_COMBO,
                         NULL);
      g_signal_connect_swapped(entry, "focus-out-event",
                               G_CALLBACK(gtk_widget_hide),
                               win);
      gtk_container_add(GTK_CONTAINER(win), scroller);
      g_object_unref(scroller);

      g_signal_connect(entry, "changed",
                       G_CALLBACK(entry_changed),
                       win);
      g_signal_connect(entry, "key-press-event",
                       G_CALLBACK(entry_key_press),
                       win);

      g_list_free(children);
   }

   scroller = g_object_new(GB_TYPE_SCROLLED_WINDOW,
                           "visible", TRUE,
                           NULL);
   gtk_widget_set_hexpand(GTK_WIDGET(scroller), TRUE);
   gtk_container_add_with_properties(GTK_CONTAINER(docs), scroller,
                                     "left-attach", 0,
                                     "top-attach", 1,
                                     "height", 1,
                                     "width", 2,
                                     NULL);

   priv->webview = g_object_new(WEBKIT_TYPE_WEB_VIEW,
                                "vexpand", TRUE,
                                "visible", TRUE,
                                NULL);
   webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(priv->webview), 0.85);
   gtk_container_add(GTK_CONTAINER(scroller), priv->webview);
}
