/* main.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-application.h"
#include "gb-log.h"

gint
main (gint   argc,
      gchar *argv[])
{
   GbApplication *application;
   gint ret;

   g_set_prgname("Builder");
   g_set_application_name(_("Builder"));
   gb_log_init(TRUE, NULL);

   ENTRY;
   g_type_init();
   application = GB_APPLICATION_DEFAULT;
   ret = g_application_run(G_APPLICATION(application), argc, argv);
   g_object_unref(application);
   gb_log_shutdown();
   RETURN(ret);
}
