/* gb-application-plugin.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gb-log.h"
#include "gb-application.h"
#include "gb-application-plugin.h"

/**
 * gb_application_plugin_activate:
 * @plugin: (in): A #GbApplicationPlugin.
 *
 * Activates the #GbApplicationPlugin to allow it to prepare itself.
 */
void
gb_application_plugin_activate (GbApplicationPlugin *plugin)
{
   GbApplicationPluginIface *iface;

   g_return_if_fail(GB_IS_APPLICATION_PLUGIN(plugin));

   if ((iface = GB_APPLICATION_PLUGIN_GET_INTERFACE(plugin))->activate) {
      iface->activate(plugin);
   }
}

/**
 * gb_application_plugin_deactivate:
 * @plugin: (in): A #GbApplicationPlugin.
 *
 * Deactivates the #GbApplicationPlugin to allow it to shutdown.
 */
void
gb_application_plugin_deactivate (GbApplicationPlugin *plugin)
{
   GbApplicationPluginIface *iface;

   g_return_if_fail(GB_IS_APPLICATION_PLUGIN(plugin));

   if ((iface = GB_APPLICATION_PLUGIN_GET_INTERFACE(plugin))->deactivate) {
      iface->deactivate(plugin);
   }
}

static void
gb_application_plugin_init (gpointer iface)
{
   static gsize initialized = FALSE;

   ENTRY;

   if (g_once_init_enter(&initialized)) {
      g_object_interface_install_property(iface,
                                          g_param_spec_object("application",
                                                              "Application",
                                                              "The application instance.",
                                                              GB_TYPE_APPLICATION,
                                                              G_PARAM_WRITABLE));
      g_once_init_leave(&initialized, TRUE);
   }

   EXIT;
}

GType
gb_application_plugin_get_type (void)
{
   static gsize initialized = FALSE;
   static GType type_id = 0;
   static const GTypeInfo g_type_info = {
      sizeof(GbApplicationPluginIface),
      gb_application_plugin_init, /* base_init */
      NULL,                       /* base_finalize */
      NULL,                       /* class_init */
      NULL,                       /* class_finalize */
      NULL,                       /* class_data */
      0,                          /* instance_size */
      0,                          /* n_preallocs */
      NULL,                       /* instance_init */
      NULL                        /* value_vtable */
   };

   if (g_once_init_enter(&initialized)) {
      type_id = g_type_register_static(G_TYPE_INTERFACE,
                                       "GbApplicationPlugin",
                                       &g_type_info,
                                       0);
      g_type_interface_add_prerequisite(type_id, G_TYPE_OBJECT);
      g_once_init_leave(&initialized, TRUE);
   }

   return type_id;
}
