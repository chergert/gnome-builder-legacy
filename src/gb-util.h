/* gb-util.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_THEME_H
#define GB_THEME_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GB_STYLE_CLASS_BOTTOM_BAR   "gb-bottom-bar"
#define GB_STYLE_CLASS_TOP_BAR      "gb-top-bar"
#define GB_STYLE_CLASS_TOP_CONTROLS "gb-top-controls"

void gb_theme_init                (void);
void gb_widget_shrink_font        (GtkWidget   *widget);
void gb_widget_monospace_font     (GtkWidget   *widget);
void gb_widget_grab_focus_in_idle (GtkWidget   *widget);
void gb_widget_add_style_class    (GtkWidget   *widget,
                                   const gchar *style_class);

const gchar *gb_path_get_icon_name (const gchar *path);
gboolean gb_container_has_children (GtkContainer *container);
GType gb_get_scrolled_window_gtype (void);

G_END_DECLS

#endif /* GB_THEME_H */
