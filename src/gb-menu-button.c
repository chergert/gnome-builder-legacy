/* gb-menu-button.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-menu-button.h"

G_DEFINE_TYPE(GbMenuButton, gb_menu_button, GTK_TYPE_BUTTON)

struct _GbMenuButtonPrivate
{
   GtkWidget *label;
   GtkWidget *menu;
   guint menu_deactivate;
};

enum
{
   PROP_0,
   PROP_LABEL,
   PROP_MENU,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

GtkWidget *
gb_menu_button_new (const gchar *label,
                    GtkMenu     *menu)
{
   g_return_val_if_fail(GTK_IS_MENU(menu), NULL);
   return g_object_new(GB_TYPE_MENU_BUTTON,
                       "label", label,
                       "menu", menu,
                       NULL);
}

static void
gb_menu_button_menu_deactivate (GtkMenu      *menu,
                                GbMenuButton *button)
{
   GtkWindow *window;

   g_return_if_fail(GTK_IS_MENU(menu));
   g_return_if_fail(GB_IS_MENU_BUTTON(button));

   window = GTK_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(button)));
   gtk_window_set_focus(window, NULL);
}

/**
 * gb_menu_button_get_label_widget:
 * @button: (in): A #GbMenuButton.
 *
 * Get the widget containing the label.
 *
 * Returns: (transfer none): A #GtkWidget.
 */
GtkWidget *
gb_menu_button_get_label_widget (GbMenuButton *button)
{
   g_return_val_if_fail(GB_IS_MENU_BUTTON(button), NULL);
   return button->priv->label;
}

const gchar *
gb_menu_button_get_label (GbMenuButton *button)
{
   g_return_val_if_fail(GB_IS_MENU_BUTTON(button), NULL);
   return gtk_label_get_text(GTK_LABEL(button->priv->label));
}

void
gb_menu_button_set_label (GbMenuButton *button,
                          const gchar  *label)
{
   g_return_if_fail(GB_IS_MENU_BUTTON(button));
   gtk_label_set_text(GTK_LABEL(button->priv->label), label);
}

/**
 * gb_menu_button_get_menu:
 * @button: (in): A #GbMenuButton.
 *
 * Gets the menu associated with the widget, or %NULL.
 *
 * Returns: (transfer none): A #GtkWidget or %NULL.
 */
GtkWidget *
gb_menu_button_get_menu (GbMenuButton *button)
{
   g_return_val_if_fail(GB_IS_MENU_BUTTON(button), NULL);
   return button->priv->menu;
}

void
gb_menu_button_set_menu (GbMenuButton *button,
                         GtkMenu      *menu)
{
   GbMenuButtonPrivate *priv;

   g_return_if_fail(GB_IS_MENU_BUTTON(button));
   g_return_if_fail(!menu || GTK_IS_MENU(menu));

   priv = button->priv;

   if (priv->menu) {
      g_signal_handler_disconnect(priv->menu, priv->menu_deactivate);
      priv->menu_deactivate = 0;
      g_clear_object(&priv->menu);
   }

   if (menu) {
      priv->menu = g_object_ref(menu);
      priv->menu_deactivate =
         g_signal_connect(menu, "deactivate",
                          G_CALLBACK(gb_menu_button_menu_deactivate),
                          button);
   }
}

static void
gb_menu_button_menu_position_func (GtkMenu  *menu,
                                   gint     *x,
                                   gint     *y,
                                   gboolean *push_in,
                                   gpointer  user_data)
{
   GtkAllocation alloc;

   g_return_if_fail(GTK_IS_MENU(menu));
   g_return_if_fail(GTK_IS_WIDGET(user_data));

   gtk_widget_get_allocation(user_data, &alloc);
   gdk_window_get_root_coords(gtk_widget_get_window(user_data),
                              alloc.x, alloc.y + alloc.height, x, y);
   *push_in = TRUE;
}

static void
gb_menu_button_clicked (GtkWidget *widget,
                        gpointer   user_data)
{
   GbMenuButtonPrivate *priv;
   GbMenuButton *button = (GbMenuButton *)widget;
   GtkWindow *window;

   g_return_if_fail(GB_IS_MENU_BUTTON(button));

   priv = button->priv;

   if (priv->menu) {
      gtk_menu_popup(GTK_MENU(priv->menu), NULL, NULL,
                     gb_menu_button_menu_position_func,
                     button, 1, gtk_get_current_event_time());
   } else {
      window = GTK_WINDOW(gtk_widget_get_toplevel(widget));
      gtk_window_set_focus(window, NULL);
   }
}

static void
gb_menu_button_destroy (GtkWidget *widget)
{
   gb_menu_button_set_menu(GB_MENU_BUTTON(widget), NULL);
   GTK_WIDGET_CLASS(gb_menu_button_parent_class)->destroy(widget);
}

static void
gb_menu_button_finalize (GObject *object)
{
   G_OBJECT_CLASS(gb_menu_button_parent_class)->finalize(object);
}

static void
gb_menu_button_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
   GbMenuButton *button = GB_MENU_BUTTON(object);

   switch (prop_id) {
   case PROP_MENU:
      g_value_set_object(value, gb_menu_button_get_menu(button));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_menu_button_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
   GbMenuButton *button = GB_MENU_BUTTON(object);

   switch (prop_id) {
   case PROP_LABEL:
      gb_menu_button_set_label(button, g_value_get_string(value));
      break;
   case PROP_MENU:
      gb_menu_button_set_menu(button, g_value_get_object(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_menu_button_class_init (GbMenuButtonClass *klass)
{
   GObjectClass *object_class;
   GtkWidgetClass *widget_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_menu_button_finalize;
   object_class->get_property = gb_menu_button_get_property;
   object_class->set_property = gb_menu_button_set_property;
   g_type_class_add_private(object_class, sizeof(GbMenuButtonPrivate));

   widget_class = GTK_WIDGET_CLASS(klass);
   widget_class->destroy = gb_menu_button_destroy;

   gParamSpecs[PROP_LABEL] =
      g_param_spec_string("label",
                          _("Label"),
                          _("The label for the button."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_LABEL,
                                   gParamSpecs[PROP_LABEL]);

   gParamSpecs[PROP_MENU] =
      g_param_spec_object("menu",
                          _("Menu"),
                          _("The menu to show on click."),
                          GTK_TYPE_MENU, G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_MENU,
                                   gParamSpecs[PROP_MENU]);
}

static void
gb_menu_button_init (GbMenuButton *button)
{
   GbMenuButtonPrivate *priv;
   GtkWidget *arrow;
   GtkWidget *hbox;

   button->priv = priv =
      G_TYPE_INSTANCE_GET_PRIVATE(button,
                                  GB_TYPE_MENU_BUTTON,
                                  GbMenuButtonPrivate);

   hbox = g_object_new(GTK_TYPE_HBOX,
                       "spacing", 3,
                       "visible", TRUE,
                       NULL);
   gtk_container_add(GTK_CONTAINER(button), hbox);

   priv->label = g_object_new(GTK_TYPE_LABEL,
                              "ellipsize", PANGO_ELLIPSIZE_END,
                              "single-line-mode", TRUE,
                              "visible", TRUE,
                              "xalign", 0.0f,
                              NULL);
   gtk_container_add(GTK_CONTAINER(hbox), priv->label);

   arrow = g_object_new(GTK_TYPE_ARROW,
                        "arrow-type", GTK_ARROW_DOWN,
                        "visible", TRUE,
                        NULL);
   gtk_container_add_with_properties(GTK_CONTAINER(hbox), arrow,
                                     "expand", FALSE,
                                     NULL);

   g_signal_connect(button, "clicked",
                    G_CALLBACK(gb_menu_button_clicked),
                    NULL);
}
