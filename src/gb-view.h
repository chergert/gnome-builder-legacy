/* gb-view.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_VIEW_H
#define GB_VIEW_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GB_TYPE_VIEW            (gb_view_get_type())
#define GB_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_VIEW, GbView))
#define GB_VIEW_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_VIEW, GbView const))
#define GB_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_VIEW, GbViewClass))
#define GB_IS_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_VIEW))
#define GB_IS_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_VIEW))
#define GB_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_VIEW, GbViewClass))

typedef struct _GbView        GbView;
typedef struct _GbViewClass   GbViewClass;
typedef struct _GbViewPrivate GbViewPrivate;

struct _GbView
{
   GtkGrid parent;

   /*< private >*/
   GbViewPrivate *priv;
};

struct _GbViewClass
{
   GtkGridClass parent_class;

   void     (*close)   (GbView *view);
   void     (*save)    (GbView *view);
   gboolean (*is_file) (GbView *view,
                        GFile  *file);
};

void         gb_view_close          (GbView      *view);
void         gb_view_emit_closed    (GbView      *view);
GtkWidget   *gb_view_get_controls   (GbView      *view);
gboolean     gb_view_get_can_save   (GbView      *view);
gboolean     gb_view_get_can_search (GbView      *view);
const gchar *gb_view_get_icon_name  (GbView      *view);
const gchar *gb_view_get_name       (GbView      *view);
GType        gb_view_get_type       (void) G_GNUC_CONST;
gboolean     gb_view_is_file        (GbView      *view,
                                     GFile       *file);
void         gb_view_set_can_save   (GbView      *view,
                                     gboolean     can_save);
void         gb_view_set_can_search (GbView      *view,
                                     gboolean     can_search);
void         gb_view_save           (GbView      *view);
void         gb_view_set_icon_name  (GbView      *view,
                                     const gchar *icon_name);
void         gb_view_set_name       (GbView      *view,
                                     const gchar *name);

G_END_DECLS

#endif /* GB_VIEW_H */
