#!/usr/bin/env python

from gi.repository import GdkPixbuf
from gi.repository import GLib
from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import GtkSource
from gi.repository import Pango
from gi.repository import PangoCairo

BREAKPOINT = GdkPixbuf.Pixbuf.new_from_file('breakpoint-marker.png')
breakpoints = {}

css_data = """
GtkButton.gb_top {
    border-image: none;
    border-width: 0 0 1 0;
    border-style: solid;
    border-radius: 0;
    border-color: #c5c5c5;
    background-image: -gtk-gradient(linear,
                                    left top, left bottom,
                                    from (@wm_bg_b),
                                    to (@button_gradient_color_b));
}

GtkComboBox.gb_top .button {
    border-image: none;
    border-width: 0 1 1 0;
    border-style: solid;
    border-radius: 0;
    border-color: #c5c5c5;
    background-image: -gtk-gradient(linear,
                                    left top, left bottom,
                                    from (@wm_bg_b),
                                    to (@button_gradient_color_b));
}

GtkComboBox.gb_bottom .button {
    border-image: none;
    border-width: 1 0 0 0;
    border-style: solid;
    border-radius: 0;
    border-color: #c5c5c5;
}

* {
    -GtkPaned-handle-size: 1;
}

.pane-separator {
    color: darker (@theme_bg_color);
    background-color: darker (@theme_bg_color);
}
"""

aluminum1 = Gdk.RGBA(0xee/255., 0xee/255., 0xec/255.)

def patch_css(screen):
    css = Gtk.CssProvider()
    css.load_from_data(css_data)
    Gtk.StyleContext.add_provider_for_screen(screen, css, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

bp_notify = []

def set_breakpoint(line):
    breakpoints[line] = True
    for c in bp_notify: c()

def unset_breakpoint(line):
    if line in breakpoints:
        del breakpoints[line]
    for c in bp_notify: c()

class GutterRendererFolds(GtkSource.GutterRenderer):
    def __init__(self, **kwargs):
        kwargs['size'] = 8
        GtkSource.GutterRenderer.__init__(self, **kwargs)

    def do_draw(self, cr, bg_area, cell_area, start, end, state):
        GtkSource.GutterRenderer.do_draw(self, cr, bg_area, cell_area, start, end, state)

        b = start.get_buffer()
        text = b.get_text(start, end, False).strip()

        if text and text[-1] in ('{',):
            l = PangoCairo.create_layout(cr)
            c = '-' if text[-1] == '{' else '+'
            l.set_text(c, 1)
            cr.set_source_rgb(0, 0, 0)
            cr.move_to(cell_area.x, cell_area.y)
            fd = Pango.FontDescription()
            fd.set_family('Monospace')
            fd.set_size(10 * Pango.SCALE)
            l.set_font_description(fd)
            PangoCairo.show_layout(cr, l)

class GutterRendererBreakpoint(GtkSource.GutterRendererPixbuf):
    def __init__(self, **kwargs):
        kwargs['pixbuf'] = BREAKPOINT
        kwargs['size'] = 16
        GtkSource.GutterRendererPixbuf.__init__(self, **kwargs)
        bp_notify.append(lambda: self.queue_draw())

    def has_breakpoint(self, line):
        return breakpoints.get(line, False)

    def do_query_activatable(self, iter, area, event):
        return True

    def do_activate(self, iter, area, event):
        line = iter.get_line()
        if line not in breakpoints:
            set_breakpoint(line)
        else:
            unset_breakpoint(line)

    def do_draw(self, cr, bg_area, cell_area, start, end, state):
        GtkSource.GutterRendererPixbuf.do_draw(self, cr, bg_area, cell_area, start, end, state)
        if self.has_breakpoint(start.get_line()):
            width = height = self.get_size()
            x = cell_area.x + ((cell_area.width - width) / 2.0)
            y = cell_area.y + ((cell_area.height - height) / 2.0)
            cr.rectangle(x, y, width, height)
            Gdk.cairo_set_source_pixbuf(cr, BREAKPOINT, x, y + 2)
            cr.paint()

class GutterRendererDiff(GtkSource.GutterRenderer):
    def __init__(self, **kwargs):
        GtkSource.GutterRenderer.__init__(self, **kwargs)

    def do_draw(self, cr, bg_area, cell_area, start, end, state):
        GtkSource.GutterRenderer.do_draw(self, cr, bg_area, cell_area, start, end, state)
        cr.rectangle(cell_area.x + cell_area.width - 1, cell_area.y, 1, cell_area.height)
        if start.get_line() % 12 == 0:
            cr.set_source_rgb(1, 200/255., 8/255.)
            cr.fill()
        elif start.get_line() % 6 == 0:
            cr.set_source_rgb(1, 133/255., 133/255.)
            cr.fill()
        elif start.get_line() % 3:
            cr.set_source_rgb(114/255.0, 207/255.0, 118/255.0)
            cr.fill()

class Window(Gtk.Window):
    def __init__(self):
        super(Window, self).__init__()
        self.set_default_size(600, 700)
        self.set_title("Gnome Builder")
        self.props.has_resize_grip = False

        vbox = Gtk.VBox(visible=True)
        self.add(vbox)

        #tb = Gtk.Toolbar(visible=True)
        #tb.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)
        #vbox.pack_start(tb, False, True, 0)

        hbox = Gtk.HBox(visible=True)
        vbox.pack_start(hbox, False, True, 0)

        combo = Gtk.ComboBoxText(visible=True, has_frame=False)
        combo.get_style_context().add_class('gb_top')
        combo.append_text("gb-filename.c")
        combo.append_text("gb-filename.h")
        combo.set_active(0)
        hbox.add(combo)
        combo.get_cells()[0].props.size_points = 9.0

        combo = Gtk.ComboBoxText(visible=True, has_frame=False)
        combo.get_style_context().add_class('gb_top')
        combo.append_text("C")
        combo.append_text("Python")
        combo.set_active(0)
        hbox.pack_start(combo, False, True, 0)
        combo.get_cells()[0].props.size_points = 9.0

        vpaned = Gtk.VPaned(visible=True)
        def onPosition(paned, pspec):
            if paned.props.position < 24:
                button.props.active = False
        vpaned.connect('notify::position', onPosition)
        vbox.add(vpaned)

        img = Gtk.Image.new_from_file('split-16.png')
        img.show()

        def onToggled(button):
            if button.props.active:
                sw = Gtk.ScrolledWindow(visible=True)
                t = makeText(b)
                sw.add(t)
                vpaned.add(sw)
                vpaned.set_position(vpaned.get_allocation().height / 2.0)
                t.grab_focus()
            else:
                vpaned.remove(vpaned.get_children()[1])
        button = Gtk.ToggleButton(visible=True, child=img)
        button.get_style_context().add_class('gb_top')
        button.connect('toggled', onToggled)
        hbox.pack_start(button, False, True, 0)

        scroll = Gtk.ScrolledWindow(visible=True)
        vpaned.add(scroll)

        b = GtkSource.Buffer()
        sm = GtkSource.StyleSchemeManager.get_default()
        sm.append_search_path('./schemes')
        sl = GtkSource.LanguageManager.get_default()
        paths = sl.get_search_path()
        paths.insert(0, './langs')
        sl.set_search_path(paths)
        b.set_language(sl.get_language('c'))
        b.set_style_scheme(sm.get_scheme('tango2'))
        b.set_text("""#include <gtk/gtk.h>

gint
main (gint   argc,
      gchar *argv[])
{
	gtk_init(&argc, &argv);
	gtk_main();
	return 0;
}
""")
        b.place_cursor(b.get_start_iter())

        def makeText(b):
            def notify_cursor(b, pspec):
                i = b.props.cursor_position
                i = b.get_iter_at_offset(i)
                start = b.get_iter_at_line(i.get_line())
                end = b.get_iter_at_line(i.get_line())
                end.forward_to_line_end()
                line = b.get_text(start, end, False)[:i.get_line_offset()]
                width = text.get_tab_width()
                line = line.replace('\t', ' ' * width)
                l = 'Line %d' % (i.get_line() + 1)
                line_cell.props.text = l
                c = 'Column %d' % (len(line) + 1)
                column_cell.props.text = c
                bcombo.queue_draw()
            b.connect('notify::cursor-position', notify_cursor)
            text = GtkSource.View(visible=True, buffer=b)
            text.props.left_margin = 3
            font_desc = Pango.FontDescription()
            font_desc.set_family('Monospace')
            font_desc.set_size(10 * Pango.SCALE)
            text.override_font(font_desc)
            text.props.show_line_numbers = True
            text.props.show_right_margin = True
            text.props.right_margin_position = 80
            text.props.tab_width = 4

            gutter = text.get_gutter(Gtk.TextWindowType.LEFT)
            def callback(data):
                gp = GutterRendererBreakpoint()
                gp.set_padding(3, 0)
                gutter.insert(gp, -30)

                diff = GutterRendererDiff(size=4)
                gutter.insert(diff, -5)

                folds = GutterRendererFolds()
                gutter.insert(folds, -6)
            #GLib.timeout_add(100, callback, None)
            callback(None)

            return text

        text = makeText(b)
        scroll.add(text)

        combo = Gtk.ComboBox(visible=True, has_entry=False)
        combo.get_style_context().add_class('gb_bottom')
        combo.set_size_request(-1, 30)
        vbox.pack_start(combo, False, True, 0)

        combo.clear()

        cell = Gtk.CellRendererText(size_points=9)
        cell.props.text = 'gb_animation_start'
        combo.pack_start(cell, True)

        line_cell = cell = Gtk.CellRendererText(size_points=9)
        cell.props.text = 'Line 1'
        cell.props.width = 50
        combo.pack_start(cell, False)

        column_cell = cell = Gtk.CellRendererText(size_points=9)
        cell.props.text = 'Column 1'
        cell.props.width = 75
        combo.pack_start(cell, False)

        bcombo = combo

        text.grab_focus()

if __name__ == "__main__":
    import sys
    app = Gtk.Application(application_id="org.gnome.Builder")
    def on_activate(app):
        patch_css(Gdk.Screen.get_default())
        w = Window()
        w.present()
        w.connect("delete-event", lambda *_: Gtk.main_quit())
        app.add_window(w)
    app.connect('activate', on_activate)
    app.run(sys.argv)
