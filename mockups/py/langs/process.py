#!/usr/bin/env python

for line in file('/home/christian/.vim/syntax/gtk.vim'):
    if line.startswith('syn keyword gtkFunction'):
        funcs = (s.strip() for s in line.split('gtkFunction', 1)[1].split(' '))
        for func in funcs:
            print '<keyword>'+func+'</keyword>'
