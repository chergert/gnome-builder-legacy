#!/usr/bin/env python

#
# cautoindent.py
#
# Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from gi.repository import GLib
from gi.repository import GnomeBuilder
from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import GtkSource
from gi.repository import Pango

def getLastCharacter(iter, char, syncChar=None):
    """
    Looks for last occurance of @char. If @syncChar is found, an equal
    number of @char characters will be skipped.
    """
    toSkip = 0
    iter = iter.copy()
    while iter.backward_char():
        if iter.get_char() == syncChar:
            toSkip += 1
        if iter.get_char() == char:
            if toSkip:
                toSkip -= 1
            else:
                return iter.get_line(), iter.get_line_offset()
    return -1, -1

def getWord(iter):
    chars = '_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    word = iter.get_char()
    while True:
        ret = iter.backward_char()
        char = iter.get_char()
        if char in chars:
            word = char + word
        else:
            break
        if not ret:
            break
    return word

class AutoIndenter(GObject.Object, GnomeBuilder.ViewPlugin):
    __gtype_name__ = 'GbAutoIndenter'

    window = GObject.property(type=GnomeBuilder.Window)
    view = GObject.property(type=GnomeBuilder.View)

    lang_handler = 0
    insert_handler = 0

    def enable(self):
        if not self.insert_handler:
            sb = self.sourceBuffer
            self.insert_handler = sb.connect('insert-text', self.onInsertText)

    def disable(self):
        if self.insert_handler:
            sb = self.sourceBuffer
            sb.disconnect(self.insert_handler)
            self.insert_handler = 0

    def do_activate(self):
        if type(self.view) is not GnomeBuilder.ViewSource:
            return

        self.sourceView = self.view.props.source_view
        self.sourceBuffer = self.sourceView.props.buffer

        def onNotifyLanguage(buffer, pspec):
            lang = buffer.get_language()
            lang = lang.get_id() if lang else None
            if lang in ('c', 'chdr'):
                self.enable()
            else:
                self.disable()
        self.lang_handler = self.sourceBuffer.connect('notify::language',
                                                      onNotifyLanguage)
        onNotifyLanguage(self.sourceBuffer, None)

    def do_deactivate(self):
        self.disable()
        if self.lang_handler:
            self.sourceBuffer.disconnect(self.lang_handler)
            self.lang_handler = 0

    def onInsertText(self,
                   sourceBuffer,
                   insertLocation,
                   insertText,
                   insertTextLenght,
                   userData=None):
        """
        Handle insertion of text into the source buffer.

        If the inserted text is a newline, then we will try to determine
        where the next line of text should begin and properly space to
        that position.
        """
        tab = self.get_tab()
        line = insertLocation.get_line()
        if insertText == '\n':
            insertLocation.forward_chars(len(insertText) - 1)
            predent, insert, postdent = self.getIndentText(insertLocation.get_line(), insertLocation)
            sourceBuffer.insert(insertLocation, predent)
            origLine = insertLocation.get_line()
            origLineOffset = insertLocation.get_line_offset()
            sourceBuffer.insert(insertLocation, insert)
            sourceBuffer.insert(insertLocation, postdent)
            insertLocation.set_line(origLine)
            insertLocation.set_line_offset(origLineOffset)
            if insert and postdent:
                newInsertLocation = insertLocation.copy()
                newInsertLocation.forward_char()
                sourceBuffer.place_cursor(newInsertLocation)
        elif insertText == '}':
            line = insertLocation.get_line()
            if not self.getLineText(line).strip():
                line, offset = getLastCharacter(insertLocation, '{', '}')
                if line >= 0 and offset >= 0:
                    predent = ''
                    lineText = self.getLineText(line)
                    for i in lineText:
                        if i in (' ', tab):
                            predent += i
                            continue
                        break
                    startIter = insertLocation.copy()
                    startIter.set_line_offset(0)
                    sourceBuffer.delete(startIter, insertLocation)
                    sourceBuffer.insert(insertLocation, predent)
        elif insertText == '{':
            line = self.getLineText(insertLocation.get_line())
            if 'struct ' in line or 'enum ' in line or 'union ' in line:
                origLine = insertLocation.get_line()
                origLineOffset = insertLocation.get_line_offset()
                sourceBuffer.insert(insertLocation, '\n'+tab+'\n};')
                insertLocation.set_line(origLine)
                insertLocation.set_line_offset(origLineOffset)
                newInsertLocation = insertLocation.copy()
                newInsertLocation.forward_char()
                newInsertLocation.forward_char()
                sourceBuffer.place_cursor(newInsertLocation)
        elif insertText == '/':
            iter = insertLocation.copy()
            iter.backward_char()
            if iter.get_char() == ' ':
                iter.backward_char()
                if iter.get_char() == '*':
                    iter.forward_char()
                    sourceBuffer.delete(iter, insertLocation)

    def getLineText(self, line):
        if line < 0:
            return ""
        startIter = self.sourceBuffer.get_iter_at_line(line)
        endIter = startIter.copy()
        endIter.forward_to_line_end()
        return self.sourceBuffer.get_slice(startIter, endIter, True)

    def lineEndsDeclaration(self, line):
        tab = self.get_tab()
        lineText = self.getLineText(line)
        if lineText.strip().endswith(')'):
            # Get the line that started this paranthesis
            iter = self.sourceBuffer.get_iter_at_line(line)
            iter.forward_to_line_end()
            iter.backward_char()
            cLine, cOffset = getLastCharacter(iter, '(', ')')
            if cLine >= 0 and cOffset >= 0:
                # If this line has content at offset 0, it is a function
                cText = self.getLineText(cLine)
                if cText[0] not in (' ', tab):
                    return True

    def getIndentText(self, line, iter=None):
        tab = self.get_tab()

        if line == 0:
            return ('', '', '')

        if self.lineEndsDeclaration(line):
            return ('\n{', tab, '')

        if iter:
            startIter = self.sourceBuffer.get_iter_at_line(line)
            lineText = self.sourceBuffer.get_slice(startIter, iter, True)
        else:
            lineText = self.getLineText(line)

        if lineText.strip().endswith(');'):
            indent = ''
            cLine, cOffset = getLastCharacter(iter, '(', None)
            otherLine = self.getLineText(cLine)
            for i in otherLine:
                if i in (' ', tab):
                    indent += i
            return ('', indent, '')

        if lineText.strip() == '*/':
            return ('', lineText[:lineText.index('*') - 1], '')

        if lineText.endswith(','):
            if '(' in lineText:
                predent = ''
                iter = self.sourceBuffer.get_iter_at_line(line)
                iter.forward_to_line_end()
                cLine, cOffset = getLastCharacter(iter, '(', ')')
                cLineText = self.getLineText(cLine)
                for i in cLineText:
                    if i in (' ', tab):
                        predent += i
                    else:
                        break
                predent += ' ' * cOffset
                if cLine == line:
                    predent += ' '
                return ('', predent, '')

        if lineText.strip()[:2] in ('*', '* ', '/*'):
            return ('', lineText[:lineText.index('*')].replace('/', ' ') + '* ', '')

        o = 0
        for i in lineText:
            if i not in (' ', tab):
                add = ''
                if lineText.endswith('{') \
                and not lineText.strip().startswith('switch '):
                    add = tab
                elif lineText.endswith(':') \
                and lineText.strip().startswith('case'):
                    add = tab
                return ('', lineText[:o] + add, '')
            o += 1
        return ('', lineText[:o], '')

    def get_tab(self):
        sv = self.view.props.source_view
        if sv.props.insert_spaces_instead_of_tabs:
            w = sv.props.indent_width
            return w * ' '
        return '\t'
