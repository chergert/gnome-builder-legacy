#!/usr/bin/env python

#
# cgobject.py
#
# Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from codedom import Class, CCodeDom
from datetime import datetime
from gettext import gettext as _
from gi.repository import Gdk
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GnomeBuilder
from gi.repository import GObject
from gi.repository import Gtk
from mako.template import Template
import os

class ClassDialog(Gtk.Dialog):
    klass = None

    def __init__(self):
        Gtk.Dialog.__init__(self)

        self.props.title = _("New GObject")
        self.props.border_width = 12
        self.set_default_size(500, -1)

        self.klass = Class()
        self.klass.parent_identifier = 'GObject'
        self.klass.parent_type = 'G_TYPE_OBJECT'

        path = os.path.join(os.path.dirname(__file__), 'cgobject.ui')

        self.builder = Gtk.Builder()
        self.builder.add_from_file(path)

        win = self.builder.get_object('window1')
        c = win.get_child()
        c.reparent(self.get_content_area())
        c.props.border_width = 6
        win.destroy()

        tb = self.builder.get_object('props_toolbar')
        tb.get_style_context().add_class('inline-toolbar')
        tb.get_style_context().set_junction_sides(Gtk.JunctionSides.TOP)

        sw = self.builder.get_object('scrolledwindow1')
        sw.get_style_context().set_junction_sides(Gtk.JunctionSides.BOTTOM)

        tb = self.builder.get_object('methods_toolbar')
        tb.get_style_context().add_class('inline-toolbar')
        tb.get_style_context().set_junction_sides(Gtk.JunctionSides.TOP)

        sw = self.builder.get_object('scrolledwindow2')
        sw.get_style_context().set_junction_sides(Gtk.JunctionSides.BOTTOM)

        tb = self.builder.get_object('signals_toolbar')
        tb.get_style_context().add_class('inline-toolbar')
        tb.get_style_context().set_junction_sides(Gtk.JunctionSides.TOP)

        sw = self.builder.get_object('scrolledwindow3')
        sw.get_style_context().set_junction_sides(Gtk.JunctionSides.BOTTOM)

        cb = self.builder.get_object('combobox1')
        cb.connect('changed', self.license_changed)
        cb.set_active(0)

        cell = Gtk.CellRendererText()
        cb.pack_start(cell, True)
        cb.add_attribute(cell, 'text', 0)

        entry1 = self.builder.get_object('entry1')
        entry1.connect('changed', self.entry_changed, 'entry1')

        entry5 = self.builder.get_object('entry5')
        entry5.connect('changed', self.entry_changed, 'entry5')

        abstract = self.builder.get_object('abstract')
        abstract.connect('toggled', lambda c: self.set_abstract(c.get_active()))

        finalize = self.builder.get_object('finalize')
        finalize.connect('toggled', lambda c: self.set_finalize(c.get_active()))

        dispose = self.builder.get_object('dispose')
        dispose.connect('toggled', lambda c: self.set_dispose(c.get_active()))

        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        self.add_button(Gtk.STOCK_SAVE, Gtk.ResponseType.OK)
        self.set_default_response(Gtk.ResponseType.OK)

    def set_abstract(self, abstract):
        self.klass.abstract = abstract

    def set_finalize(self, finalize):
        self.klass.finalize = finalize

    def set_dispose(self, dispose):
        self.klass.dispose = dispose

    def split_full_name(self, text):
        parts = []
        part = ''
        for p in text:
            if part and p.isupper() and (not part.isupper() or len(part) == 1):
                parts.append(part)
                part = ''
            part += p
        parts.append(part)
        return parts

    def entry_changed(self, entry, prop):
        if prop == 'entry1':
            parts = self.split_full_name(entry.get_text())
            self.klass.ns_upper = parts[0].upper()
            self.klass.ns_identifier = parts[0]
            self.klass.identifier = ''.join(parts[1:])
            self.klass.type = '%s_TYPE_%s' % (self.klass.ns_upper,
                                              self.klass.identifier.upper())

            entry2 = self.builder.get_object('entry2')
            text = ''
            if self.klass.ns_identifier and self.klass.identifier:
                text = '%s_%s' % (self.klass.ns_identifier.lower(),
                                  '_'.join(parts[1:]).lower())
            entry2.set_text(text)

            entry3 = self.builder.get_object('entry3')
            if self.klass.ns_upper:
                entry3.set_text(self.klass.ns_upper)
            else:
                entry3.set_text('')

            entry4 = self.builder.get_object('entry4')
            if self.klass.identifier:
                entry4.set_text('_'.join(parts[1:]).upper())
            else:
                entry4.set_text('')

        elif prop == 'entry5':
            parts = self.split_full_name(entry.get_text())
            entry6 = self.builder.get_object('entry6')
            if ''.join(parts):
                entry6.set_text('%s_TYPE_%s' % (parts[0].upper(),
                                                '_'.join(parts[1:]).upper()))
            self.klass.parent_identifier = entry.get_text()

    def license_changed(self, combo):
        it = combo.get_active_iter()
        key, = combo.get_model().get(it, 0)
        self.klass.license = key

class CGObject(GObject.Object, GnomeBuilder.WindowPlugin):
    __gtype_name__ = 'GbCGObject'

    window = GObject.property(type=GnomeBuilder.Window)

    def do_activate(self):
        actions = Gtk.ActionGroup(name='CGObjectActions')
        uiData = """
                 <ui>
                   <menubar name="menubar">
                     <menu action="file">
                       <placeholder name="file-new-actions">
                        <menuitem action="new-gobject-c"/>
                       </placeholder>
                     </menu>
                   </menubar>
                   <popup name="project-popup">
                     <placeholder name="project-popup-new-actions">
                       <menuitem action="new-gobject-c"/>
                     </placeholder>
                   </popup>
                 </ui>
                 """
        action = Gtk.Action("new-gobject-c",
                            _("New GObject"),
                            "Create a new GObject",
                            Gtk.STOCK_NEW)
        action.props.visible = True
        Gtk.AccelMap.add_entry("<CGObject>/new-gobject-c",
                               Gdk.KEY_g,
                               Gdk.ModifierType.SHIFT_MASK |
                                   Gdk.ModifierType.CONTROL_MASK)
        action.set_accel_path("<CGObject>/new-gobject-c")
        action.connect('activate', lambda *_: self.show_dialog())
        actions.add_action(action)
        ui = self.props.window.get_ui_manager()
        ui.insert_action_group(actions, 0)
        ui.add_ui_from_string(uiData)

    def show_dialog(self):
        win = ClassDialog()
        win.props.transient_for = self.window
        win.props.window_position = Gtk.WindowPosition.CENTER_ON_PARENT
        if win.run() == Gtk.ResponseType.OK:
            path = self.get_directory()
            real_path = os.path.join(self.window.props.project.props.directory, path)
            dom = CCodeDom(win.klass, real_path)
            filenames = dom.write() # async?
            target = self.get_target()
            if target and filenames:
                for f in filenames:
                    p = os.path.join(path, f)
                    m = GnomeBuilder.ProjectFileMode.SOURCE
                    pf = GnomeBuilder.ProjectFile(path=p, mode=m)
                    target.props.files.append(pf)
            self.window.props.browser.rebuild()
            #def save_cb(project, result, data):
            #    project.save_finish(result)
            #self.window.props.project.save_async(None, save_cb, None)

        win.destroy()

    def get_target(self):
        targets = self.window.props.project.props.targets
        if targets.props.count == 1:
            return targets.get_item(0)

    def get_directory(self):
        target = self.get_target()
        if target and target.props.directory:
            return target.props.directory
        return '.'

    def do_update_state(self):
        pass
