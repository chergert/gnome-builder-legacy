#!/usr/bin/env python

from datetime import datetime
import os

LICENSES = {
    'GPL-3': ['This program is free software: you can redistribute it and/or modify',
              'it under the terms of the GNU General Public License as published by',
              'the Free Software Foundation, either version 3 of the License, or',
              '(at your option) any later version.',
              '',
              'This program is distributed in the hope that it will be useful,',
              'but WITHOUT ANY WARRANTY; without even the implied warranty of',
              'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the',
              'GNU General Public License for more details.',
              '',
              'You should have received a copy of the GNU General Public License',
              'along with this program.  If not, see <http://www.gnu.org/licenses/>.'],
    'LGPL-2.1': ['This file is free software; you can redistribute it and/or',
                 'modify it under the terms of the GNU Lesser General Public',
                 'License as published by the Free Software Foundation; either',
                 'version 2.1 of the License, or (at your option) any later version.',
                 '',
                 'This file is distributed in the hope that it will be useful,',
                 'but WITHOUT ANY WARRANTY; without even the implied warranty of',
                 'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU',
                 'Lesser General Public License for more details.',
                 '',
                 'You should have received a copy of the GNU General Public License',
                 'along with this program.  If not, see <http://www.gnu.org/licenses/>.'],
}

class Class:
    """
    Represents a GObject class to generate.
    """
    license = None            # `GPL-3'
    abstract = None           # Use G_DEFINE_ABSTRACT_TYPE()
    dispose = None            # Write _dispose func.
    finalize = True           # Write _finalize func.
    identifier = None         # `Object'
    type = None               # `MY_TYPE_OBJECT'
    parent_identifier = None  # `GObject'
    parent_type = None        # `G_TYPE_OBJECT'
    properties = None         # List of properties
    methods = None            # List of methods
    ns_identifier = None      # `My'
    ns_upper = None           # `MY'
    signals = None            # List of signals

class Property:
    """
    Represents a property of a GObject class.
    """
    name = None               # `my-property'
    nick = None               # `My Property'
    desc = None               # `My custom property.'
    gtype = None              # `G_TYPE_INT'
    pspec = None              # `g_param_spec_int'
    min = None                # Min for int types
    max = None                # Max for int types
    default = None            # Default for most param specs

class Method:
    """
    Represents a method of the GObject class.
    """
    name = None               # `do_something'
    type = None               # `gboolean' return type
    parameters = None         # Parameters for method
    async = None              # Generate _async() and _finish().
    
class Signal:
    """
    Represents a signal of the GObject class.
    """
    name = None               # `some-signal'
    parameters = None         # Parameters for signal
    type = None               # `gboolean' return type

class CodeDom:
    klass = None
    directory = None
    basename = None
    
    def __init__(self, klass, directory):
        self.directory = directory
        self.klass = klass
        
    def write(self):
        pass
        
    def getLicense(self, name):
        lines = []
        if self.klass.license in LICENSES:
            year = str(datetime.now().year)
            author = os.popen('git config user.name').read().strip()
            email = os.popen('git config user.email').read().strip()
            lines.append(os.path.basename(name))
            lines.append('')
            lines.append('Copyright (C) %s %s <%s>' % (year, author, email))
            lines.append('')
            lines += LICENSES[self.klass.license]
        return lines

class CCodeDom(CodeDom):
    tabchar = '   '
    
    def write(self):
        filenames = []

        basename = '%s-%s' % (self.klass.ns_upper.lower(),
                              self.klass.identifier.lower())
        
        headerPath = os.path.join(self.directory, basename + '.h')
        with file(headerPath, 'w') as stream:
            self.writeHeader(stream)
            filenames.append(basename + '.h')
        
        sourcePath = os.path.join(self.directory, basename + '.c')
        with file(sourcePath, 'w') as stream:
            self.writeSource(stream)
            filenames.append(basename + '.c')

        return filenames
        
    def writeHeader(self, stream):
        lines = self.getLicense(stream.name)
        if lines:
            self.writeComment(stream, lines)
            self.writeLine(stream)
        
        guard = os.path.basename(stream.name) \
                       .replace('-', '_') \
                       .replace('.', '_') \
                       .upper()
        self.writeGuardBegin(stream, guard)
        self.writeLine(stream)
        self.writeInclude(stream, 'glib-object.h')
        self.writeLine(stream)
        
        self.writeLine(stream, 'G_BEGIN_DECLS')
        self.writeLine(stream)
        
        line = '#define %s_TYPE_%s            (%s_%s_get_type())' % \
                (self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_upper.lower(),
                 self.klass.identifier.lower())
        self.writeLine(stream, line)
        
        line = '#define %s_%s(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), %s_TYPE_%s, %s%s))' % \
                (self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_identifier,
                 self.klass.identifier)
        self.writeLine(stream, line)
        
        line = '#define %s_%s_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), %s_TYPE_%s, %s%s const))' % \
                (self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_identifier,
                 self.klass.identifier)
        self.writeLine(stream, line)
        
        line = '#define %s_%s_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  %s_TYPE_%s, %s%sClass))' % \
                (self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_identifier,
                 self.klass.identifier)
        self.writeLine(stream, line)
        
        line = '#define %s_IS_%s(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), %s_TYPE_%s))' % \
                (self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_upper,
                 self.klass.identifier.upper())
        self.writeLine(stream, line)
        
        line = '#define %s_IS_%s_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  %s_TYPE_%s))' % \
                (self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_upper,
                 self.klass.identifier.upper())
        self.writeLine(stream, line)
        
        line = '#define %s_%s_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  %s_TYPE_%s, %s%sClass))' % \
                (self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_upper,
                 self.klass.identifier.upper(),
                 self.klass.ns_identifier,
                 self.klass.identifier)
        self.writeLine(stream, line)
        self.writeLine(stream)
        
        line = 'typedef struct _%s%s        %s%s;' % \
                (self.klass.ns_identifier,
                 self.klass.identifier,
                 self.klass.ns_identifier,
                 self.klass.identifier)
        self.writeLine(stream, line)
        
        line = 'typedef struct _%s%sClass   %s%sClass;' % \
                (self.klass.ns_identifier,
                 self.klass.identifier,
                 self.klass.ns_identifier,
                 self.klass.identifier)
        self.writeLine(stream, line)
                 
        line = 'typedef struct _%s%sPrivate %s%sPrivate;' % \
                (self.klass.ns_identifier,
                 self.klass.identifier,
                 self.klass.ns_identifier,
                 self.klass.identifier)
        self.writeLine(stream, line)

        self.writeLine(stream)
        
        lines = ['struct _%s%s' % (self.klass.ns_identifier,
                                   self.klass.identifier),
                 '{',
                 self.tabchar + 'GObject parent;',
                 '',
                 self.tabchar + '/*< private >*/',
                 self.tabchar + '%s%sPrivate *priv;' % (
                    self.klass.ns_identifier,
                    self.klass.identifier),
                 '};']
        self.writeLines(stream, lines)
        self.writeLine(stream)

        line = 'struct _%s%sClass' % (self.klass.ns_identifier,
                                      self.klass.identifier)
        self.writeLine(stream, line)
        self.writeLine(stream, '{')
        self.writeLine(stream, self.tabchar + '%sClass parent_class;' %
                       self.klass.parent_identifier)
        
        self.writeLine(stream, '};')

        self.writeLine(stream)
        
        l = len(self.klass.ns_identifier + self.klass.identifier) + 1
        t = ('%%-%ds' % l) % 'GType'
        line = '%s %s_%s_get_type (void) G_GNUC_CONST;' % (
                t,
                self.klass.ns_upper.lower(),
                self.klass.identifier.lower())
        self.writeLine(stream, line)
        
        line = '%s%s *%s_%s_new      (void);' % (
                self.klass.ns_identifier,
                self.klass.identifier,
                self.klass.ns_upper.lower(),
                self.klass.identifier.lower())
        self.writeLine(stream, line)
        
        self.writeLine(stream)
        
        self.writeLine(stream, 'G_END_DECLS')
        self.writeLine(stream)
        
        self.writeGuardEnd(stream, guard)
        
    def writeSource(self, stream):
        lines = self.getLicense(stream.name)
        if lines:
            self.writeComment(stream, lines)
            self.writeLine(stream)
        
    def writeComment(self, stream, lines):
        if not lines:
            return
        if not isinstance(lines, list):
            lines = [lines]
        if len(lines) == 1:
            data = '/* ' + lines[0] + ' */'
        else:
            data = '/* ' + '\n * '.join(lines) + '\n */'
        stream.write(data)
        stream.write('\n')
        
    def writeLine(self, stream, line=''):
        if line:
            stream.write(line)
        stream.write('\n')
        
    def writeLines(self, stream, lines):
        for line in lines:
            self.writeLine(stream, line)
        
    def writeGuardBegin(self, stream, guard):
        self.writeLine(stream, '#ifndef ' + guard)
        self.writeLine(stream, '#define ' + guard)
        
    def writeGuardEnd(self, stream, guard):
        self.writeLine(stream, '#endif /* ' + guard + ' */')
        
    def writeInclude(self, stream, filename, local=False):
        if local:
            self.writeLine(stream, '#include "%s"' % filename)
        else:
            self.writeLine(stream, '#include <%s>' % filename)
    
if __name__ == '__main__':
    klass = Class()
    klass.license = 'GPL-3'
    klass.identifier = 'Object'
    klass.type = 'MOCK_TYPE_OBJECT'
    klass.parent_identifier = 'GObject'
    klass.parent_type = 'G_TYPE_OBJECT'
    klass.properties = []
    klass.methods = []
    klass.ns_identifier = 'Mock'
    klass.ns_upper = 'MOCK'
    klass.signals = []
    
    codedom = CCodeDom(klass, '.')
    codedom.write()

