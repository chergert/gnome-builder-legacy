${license}

#include <glib/gi18n.h>

#include "${cprefix.replace('_','-')}.h"

G_DEFINE_TYPE(${CamelName}, ${cprefix}, G_TYPE_OBJECT)

struct _${CamelName}Private
{
   gpointer dummy;
};

enum
{
   PROP_0,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

static void
${cprefix}_finalize (GObject *object)
{
   G_OBJECT_CLASS(${cprefix}_parent_class)->finalize(object);
}

static void
${cprefix}_class_init (${CamelName}Class *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = ${cprefix}_finalize;
   g_type_class_add_private(object_class, sizeof(${CamelName}Private));
}

static void
${cprefix}_init (${CamelName} *self)
{
   self->priv = G_TYPE_INSTANCE_GET_PRIVATE(self,
                                            ${NS}_TYPE_${NAME},
                                            ${CamelName}Private);
}
