${license}

#ifndef ${NS}_${NAME}_H
#define ${NS}_${NAME}_H

#include <glib-object.h>

G_BEGIN_DECLS

#define ${NS}_TYPE_${NAME}            (${cprefix}_get_type())
#define ${NS}_${NAME}(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), ${NS}_TYPE_${NAME}, ${CamelName}))
#define ${NS}_${NAME}_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), ${NS}_TYPE_${NAME}, ${CamelName} const))
#define ${NS}_${NAME}_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  ${NS}_TYPE_${NAME}, ${CamelName}Class))
#define ${NS}_IS_${NAME}(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), ${NS}_TYPE_${NAME}))
#define ${NS}_IS_${NAME}_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  ${NS}_TYPE_${NAME}))
#define ${NS}_${NAME}_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  ${NS}_TYPE_${NAME}, ${CamelName}Class))

typedef struct _${CamelName}        ${CamelName};
typedef struct _${CamelName}Class   ${CamelName}Class;
typedef struct _${CamelName}Private ${CamelName}Private;

struct _${CamelName}
{
   ${ParentCamelName} parent;

   /*< private >*/
   ${CamelName}Private *priv;
};

struct _${CamelName}Class
{
   ${ParentCamelName}Class parent_class;
};

GType ${cprefix}_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* ${NS}_${NAME}_H */
