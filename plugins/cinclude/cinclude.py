#!/usr/bin/env python

#
# cinclude.py
#
# Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from gi.repository import GnomeBuilder
from gi.repository import GObject
from gi.repository import GtkSource
import os

HEADER_EXTENS = ('h', 'hh', 'hpp')
SYMBOL_CHARS  = 'abcdefghijklmnopqrstuvwxyz' \
                'ABCDEFGHIJKLMNOPQRSTUVWXYZ' \
                '_'

class CIncludeProvider(GObject.Object, GtkSource.CompletionProvider):
    buffer = None
    project = None
    sysheaders = None

    def _lineTextToCursor(self, context):
        endIter = context.get_iter().copy()
        startIter = self.buffer.get_iter_at_line(endIter.get_line())
        return startIter.get_text(endIter)

    def _getSysHeaders(self):
        if not self.sysheaders:
            self.sysheaders = [a for a in os.listdir('/usr/include')
                               if a.endswith('.h')]
            self.sysheaders.sort()
        return self.sysheaders

    def _getHeaders(self):
        targets = self.project.props.targets
        for i in range(targets.get_count()):
            t = targets.get_item(i)
            files = t.props.files
            for j in range(files.get_count()):
                f = files.get_item(j)
                path = f.props.path
                exten = path.rsplit('.', 1)[-1]
                if exten in HEADER_EXTENS:
                    yield f

    def do_get_name(self):
        return 'Includes'

    def do_get_icon(self):
        return None

    def is_directive(self, text):
        return text.startswith('#')

    def do_populate(self, context, add_to_context=True):
        self.proposals = []
        linetext = self._lineTextToCursor(context)

        def addp(label, text):
            p = GtkSource.CompletionItem(label=label, text=text)
            p.pos = context.get_iter().get_offset() - len(linetext)
            self.proposals.append(p)

        if self.is_directive(linetext):
            if '#define'.startswith(linetext):
                addp('#define', '#define ')
            if '#endif '.startswith(linetext):
                addp('#endif', '#endif ')
            if '#if '.startswith(linetext):
                addp('#if', '#if ')
            if '#ifdef '.startswith(linetext):
                addp('#ifdef', '#ifdef ')
            if '#include '.startswith(linetext):
                addp('#include', '#include ')
            if '#pragma '.startswith(linetext):
                addp('#pragma', '#pragma ')
            if '#undef'.startswith(linetext):
                addp('#undef', '#undef ')

            if linetext.startswith('#include <'):
                # Find includes from include directories.
                prefix = linetext.split('<')[1]
                for h in self._getSysHeaders():
                    if h.startswith(prefix):
                        i = '#include <%s>' % h
                        addp(h, i)
            elif linetext.startswith('#include "'):
                # Find includes from project targets.
                prefix = linetext.split('"')[1]
                for h in self._getHeaders():
                    f = os.path.basename(h.props.path)
                    if f.startswith(prefix):
                        i = '#include "%s"' % f
                        addp(f, i)

        if add_to_context:
            context.add_proposals(self, self.proposals, True)

    def do_get_activation(self):
        return GtkSource.CompletionActivation.INTERACTIVE | \
               GtkSource.CompletionActivation.USER_REQUESTED

    def do_match(self, context):
        self.do_populate(context, add_to_context=False)
        return (len(self.proposals) > 0)

    def do_get_info_widget(self, proposal):
        pass

    def do_update_info(self, proposal, info):
        pass

    def do_get_start_iter(self, context, proposal, _iter):
        _iter.set_offset(proposal.pos)
        return _iter

    def do_activate_proposal(self, proposal, _iter):
        pass

    def do_get_interactive_delay(self):
        return 0

    def do_get_priority(self):
        return 0

class CKeywordProvider(GObject.Object, GtkSource.CompletionProvider):
    buffer = None
    proposals = None
    all = None

    def _lineTextToCursor(self, context):
        endIter = context.get_iter().copy()
        startIter = self.buffer.get_iter_at_line(endIter.get_line())
        return startIter.get_text(endIter)

    def do_get_name(self):
        return 'Keywords'

    def do_get_icon(self):
        return None

    def do_populate(self, context, add_to_context=True):
        if not self.all:
            def addp(l):
                item = GtkSource.CompletionItem(label=l, text=l+' ')
                self.all.append(item)
            self.all = []
            keys = ['auto',
                    'break',
                    'case',
                    'continue',
                    'default',
                    'do',
                    'double',
                    'else',
                    'enum',
                    'extern',
                    'float',
                    'for',
                    'goto',
                    'if',
                    'int',
                    'long',
                    'register',
                    'return',
                    'short',
                    'signed',
                    'sizeof',
                    'static',
                    'struct',
                    'switch',
                    'typedef',
                    'union',
                    'unsigned',
                    'void',
                    'volatile',
                    'while']
            for k in keys: addp(k)
        self.proposals = []
        line = self._lineTextToCursor(context)
        for item in self.all:
            k = item.props.text
            words = line.split(' ')
            if words:
                word = words[-1]
                if word and k.startswith(word):
                    item.pos = context.get_iter().get_offset() - len(word)
                    self.proposals.append(item)
        if add_to_context:
            context.add_proposals(self, self.proposals, True)

    def do_get_activation(self):
        return GtkSource.CompletionActivation.INTERACTIVE | \
               GtkSource.CompletionActivation.USER_REQUESTED

    def do_match(self, context):
        self.do_populate(context, add_to_context=False)
        return (len(self.proposals) > 0)

    def do_get_info_widget(self, proposal):
        pass

    def do_update_info(self, proposal, info):
        pass

    def do_get_start_iter(self, context, proposal, _iter):
        _iter.set_offset(proposal.pos)
        return _iter

    def do_activate_proposal(self, proposal, _iter):
        pass

    def do_get_interactive_delay(self):
        return 0

    def do_get_priority(self):
        return 100

class CIncludePlugin(GObject.Object, GnomeBuilder.ViewPlugin):
    view = GObject.property(type=GnomeBuilder.View)
    window = GObject.property(type=GnomeBuilder.Window)
    provider = None
    keywords = None
    enabled = False
    lang_handler = 0

    def enable(self):
        if not self.enabled:
            sv = self.view.props.source_view
            sv.get_completion().add_provider(self.provider)
            sv.get_completion().add_provider(self.keywords)
            self.enabled = True

    def disable(self):
        if self.enabled:
            sv = self.view.props.source_view
            sv.get_completion().remove_provider(self.provider)
            sv.get_completion().remove_provider(self.keywords)
            self.enabled = False
        if self.lang_handler:
            sb = self.view.props.buffer
            sb.disconnect(self.lang_handler)
            self.lang_handler = 0

    def do_activate(self):
        if type(self.view) != GnomeBuilder.ViewSource:
            return

        sourceView = self.view.props.source_view
        sourceBuffer = self.view.props.buffer

        self.provider = CIncludeProvider()
        self.provider.buffer = self.view.props.buffer
        self.provider.project = self.window.props.project

        self.keywords = CKeywordProvider()
        self.keywords.buffer = self.view.props.buffer
        self.keywords.project = self.window.props.project

        def onNotifyLanguage(buffer, pspec):
            lang = buffer.get_language()
            lang = lang.get_id() if lang else None
            if lang in ('c', 'chdr'):
                self.enable()
            else:
                self.disable()
        self.lang_handler = sourceBuffer.connect('notify::language',
                                                 onNotifyLanguage)
        onNotifyLanguage(sourceBuffer, None)

    def do_deactivate(self):
        self.disable()
