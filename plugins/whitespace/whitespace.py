#!/usr/bin/env python

#
# whitespace.py
#
# Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from gi.repository import GnomeBuilder
from gi.repository import GObject
from gi.repository import Gtk

class CleanText(GObject.Object, GnomeBuilder.ViewPlugin):
    """
    This plugin removes whitespace at the end of lines in a text buffer.
    """
    view = GObject.property(type=GnomeBuilder.View)
    window = GObject.property(type=GnomeBuilder.Window)

    def do_activate(self):
        if type(self.view) is not GnomeBuilder.ViewSource:
            return
        buffer = self.view.props.buffer
        self.view.connect('pre-save', lambda *_: self.rstrip(buffer))

    def rstrip(self, buffer):
        n_lines = buffer.get_line_count()
        for line in xrange(n_lines):
            endIter = buffer.get_iter_at_line(line)
            n_chars = endIter.get_chars_in_line()
            endIter.forward_to_line_end()
            startIter = endIter.copy()
            hasSpace = False
            for _ in range(n_chars):
                startIter.backward_char()
                if startIter.get_char() in (' ', '\t'):
                    hasSpace = True
                    continue
                break
            if hasSpace:
                startIter.forward_char()
                buffer.delete(startIter, endIter)
