#!/usr/bin/env python

#
# cjunk.py
#
# Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from gettext import gettext as _
from gi.repository import GnomeBuilder
from gi.repository import GObject
from gi.repository import Gtk

def isSource(item):
    path = item.props.path
    suffix = path.rsplit('.', 1)[-1]
    return suffix in ('c', 'cc', 'cpp')

def isHeader(item):
    path = item.props.path
    suffix = path.rsplit('.', 1)[-1]
    return suffix in ('h', 'hh', 'hpp')

class HeadersNode(GnomeBuilder.TreeNode):
    __gtype_name__ = 'GbTreeNodeCHeaders'

class SourcesNode(GnomeBuilder.TreeNode):
    __gtype_name__ = 'GbTreeNodeCSources'

class CjunkBuilder(GnomeBuilder.TreeBuilder):
    def do_build_node(self, node):
        if isinstance(node.props.item, GnomeBuilder.ProjectTargetC):
            headers = HeadersNode(icon_name=Gtk.STOCK_DIRECTORY, text=_('Headers'))
            headers.project = node.props.item
            node.append(headers)
            sources = SourcesNode(icon_name=Gtk.STOCK_DIRECTORY, text=_('Sources'))
            sources.project = node.props.item
            node.append(sources)
        elif isinstance(node, (HeadersNode, SourcesNode)):
            for item in node.project.get_files().get_items():
                if isinstance(node, HeadersNode) and isHeader(item):
                    child = GnomeBuilder.TreeNode(item=item, icon_name='text-x-chdr')
                    child.props.text = item.props.path
                    node.append(child)
                elif isinstance(node, SourcesNode) and isSource(item):
                    child = GnomeBuilder.TreeNode(item=item, icon_name='text-x-csrc')
                    child.props.text = item.props.path
                    node.append(child)

    def do_node_activiated(self, node):
        pass

    def do_node_selected(self, node):
        pass

    def do_node_unselected(self, node):
        pass

class CjunkPlugin(GObject.Object, GnomeBuilder.WindowPlugin):
    window = GObject.property(type=GnomeBuilder.Window)
    enabled = False

    def do_activate(self):
        browser = self.props.window.props.browser
        self.builder = CjunkBuilder()
        browser.add_builder(self.builder)

    def do_deactivate(self):
        browser = self.props.window.props.browser
        browser.remove_builder(self.builder)
        self.builder = None
