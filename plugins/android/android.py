#!/usr/bin/env python

#
# android.py
#
# Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from gettext import gettext as _
from gi.repository import GnomeBuilder
from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import GUdev

class AndroidPlugin(GObject.Object, GnomeBuilder.WindowPlugin):
    window = GObject.property(type=GnomeBuilder.Window)
    sidebar = None
    client = None
    client_handler = None

    def do_activate(self):
        self.sidebar = self.props.window.props.sidebar
        self.client = GUdev.Client(subsystems=['usb', 'block'])
        self.client_handler = self.client.connect('uevent', self.on_uevent)

    def do_deactivate(self):
        self.sidebar = None
        GObject.signal_handler_disconnect(self.client, self.client_handler)
        self.client = None
        self.client_handler = None

    def on_uevent(self, client, action, device, *args):
        if action == 'add':
            if self.is_android(device):
                v = Gtk.VBox(visible=True, height_request=50)
                s = Gtk.HSeparator(visible=True)
                v.pack_start(s, False, False, 0)

                h = Gtk.HBox(visible=True, margin_top=3)
                v.pack_start(h, False, False, 0)
                h.props.border_width = 6

                i = Gtk.Image.new_from_file('/usr/share/icons/gnome/32x32/devices/phone-samsung-galaxy-s.png')
                h.pack_start(i, False, False, 0)
                i.show()

                m = device.get_property('ID_MODEL').replace('_', ' ')
                t = ('<span size="smaller">Android Device</span>\n'
                     '<span size="smaller"><span size="smaller">'
                     + m +
                     '</span></span>')
                l = Gtk.Label(label=t, use_markup=True, xalign=0.0, visible=True)
                h.pack_start(l, True, True, 0)

                self.sidebar.pack_start(v, False, True, 0)

    def is_android(self, device):
        model = device.get_property("ID_MODEL")
        if model:
            return 'android' in model.lower()
        return False
