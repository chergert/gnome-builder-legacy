#!/usr/bin/env python

#
# pyjunk.py
#
# Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from gettext import gettext as _
from gi.repository import GnomeBuilder
from gi.repository import GObject

class PackagesNode(GnomeBuilder.TreeNode):
    pass

class PyjunkBuilder(GnomeBuilder.TreeBuilder):
    def do_build_node(self, node):
        item = node.props.item
        if isinstance(item, GnomeBuilder.ProjectTargetPython):
            packages = PackagesNode(icon_name='gtk-execute', text=_('Packages'))
            node.append(packages)

    def do_node_activiated(self, node):
        pass

    def do_node_selected(self, node):
        pass

    def do_node_unselected(self, node):
        pass

class PyjunkPlugin(GObject.Object, GnomeBuilder.WindowPlugin):
    window = GObject.property(type=GnomeBuilder.Window)
    enabled = False

    def do_activate(self):
        browser = self.props.window.props.browser
        self.builder = PyjunkBuilder()
        browser.add_builder(self.builder)

    def do_deactivate(self):
        browser = self.props.window.props.browser
        browser.remove_builder(self.builder)
        self.builder = None
