#!/usr/bin/env python

#
# cfuncstyler.py
#
# Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from gi.repository import GnomeBuilder
from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import GtkSource

def getLastCharacter(iter, char, syncChar=None):
    """
    Looks for last occurance of @char. If @syncChar is found, an equal
    number of @char characters will be skipped.
    """
    toSkip = 0
    iter = iter.copy()
    while iter.backward_char():
        if iter.get_char() == syncChar:
            toSkip += 1
        if iter.get_char() == char:
            if toSkip:
                toSkip -= 1
            else:
                return iter.get_line(), iter.get_line_offset()
    return -1, -1

def getWord(iter):
    chars = '_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    word = iter.get_char()
    while True:
        ret = iter.backward_char()
        char = iter.get_char()
        if char in chars:
            word = char + word
        else:
            break
        if not ret:
            break
    return word

class FunctionStyler(GObject.Object, GnomeBuilder.ViewPlugin):
    __gtype_name__ = 'GbFunctionStyler'

    window = GObject.property(type=GnomeBuilder.Window)
    view = GObject.property(type=GnomeBuilder.View)

    lang_handler = 0
    insert_handler = 0

    def enable(self):
        if not self.insert_handler:
            sb = self.sourceBuffer
            self.insert_handler = sb.connect('insert-text', self.onInsertText)

    def disable(self):
        if self.insert_handler:
            sb = self.sourceBuffer
            sb.disconnect(self.insert_handler)
            self.insert_handler = 0

    def do_activate(self):
        if type(self.view) is not GnomeBuilder.ViewSource:
            return

        self.sourceView = self.view.props.source_view
        self.sourceBuffer = self.sourceView.props.buffer

        def onNotifyLanguage(buffer, pspec):
            lang = buffer.get_language()
            lang = lang.get_id() if lang else None
            if lang in ('c', 'chdr'):
                self.enable()
            else:
                self.disable()
        self.lang_handler = self.sourceBuffer.connect('notify::language',
                                                      onNotifyLanguage)
        onNotifyLanguage(self.sourceBuffer, None)

    def do_deactivate(self):
        self.disable()
        if self.lang_handler:
            self.sourceBuffer.disconnect(self.lang_handler)
            self.lang_handler = 0

    def onInsertText(self,
                   sourceBuffer,
                   insertLocation,
                   insertText,
                   insertTextLenght,
                   userData=None):
        if insertText == ')':
            line, offset = getLastCharacter(insertLocation, '(', ')')
            if line >= 0 and offset >= 0:
                startIter = sourceBuffer.get_iter_at_line_offset(line, offset)
                startIter.forward_char()
                text = sourceBuffer.get_slice(startIter, insertLocation, True)
                args = self.parseFuncDeclArgs(text)
                if args:
                    self.reformatFuncDeclArgs(startIter, insertLocation, args)

    def reformatFuncDeclArgs(self, startIter, endIter, args):
        longestType = max([len(a) for a,b,c in args])
        longestDeref = max([len(b) for a,b,c in args])
        parts = []
        if longestDeref > 0:
            formatString = '%%-%ds %%%ds%%s' % (longestType, longestDeref)
            for arg in args:
                parts.append(formatString % arg)
        else:
            formatString = '%%-%ds %%s' % longestType
            for a,b,c in args:
                parts.append(formatString % (a, c))
        offset = startIter.get_line_offset()
        self.sourceBuffer.delete(startIter, endIter)
        space = ' ' * offset
        text = (',\n' + space).join(parts)
        self.sourceBuffer.insert(endIter, text)

    def parseFuncDeclArgs(self, text):
        if not text.strip():
            return None
        try:
            args = []
            parts = [a.strip() for a in text.split(',')]
            for part in parts:
                tn = ''
                dr = ''
                nm = ''
                if '*' in part:
                    tn_and_dr = part[:part.rindex('*') + 1]
                    nm = part[part.rindex('*') + 1:]
                else:
                    t = part.replace('\t', ' ')
                    tn_and_dr, nm = [a.strip() for a in t.split(' ', 2)]
                dr = '*' * tn_and_dr.count('*')
                tn = tn_and_dr.replace('*','')
                args.append((tn.strip(), dr.strip(), nm.strip()))
            if len(args):
                return args
        except Exception, err:
            print repr(err)
